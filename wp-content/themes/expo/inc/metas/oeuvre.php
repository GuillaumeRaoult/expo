<?php
/**
 * Metas pour la page "la visite en musique"
 * User: guillaume
 * Date: 25/08/20
 * Time: 10:02
 */

add_action('cmb2_admin_init', 'cmb2_expo_metaboxes');
function cmb2_expo_metaboxes()
{

    $cmb2 = new_cmb2_box(array(
        'id'           => 'oeuvre_metabox',
        'title'        => __('Document IIIF', 'cmb2'),
        'object_types' => array('oeuvre'),
        'context'      => 'side',
        'priority'     => 'low',
        'show_names'   => true,
    ));

    $cmb2->add_field(array(
        'name' => 'Adresse de la page IIIF',
        'desc' => '',
        'id'   => 'iiif_page',
        'type' => 'text',
    ));

    $cmb2->add_field(array(
        'name' => 'Adresse du manifest',
        'desc' => '',
        'id'   => 'iiif_manifest',
        'type' => 'text_url',
    ));

    $cmb2->add_field(array(
        'name' => 'Page de démarrage',
        'desc' => '',
        'id'   => 'iiif_start',
        'type' => 'text_small',
    ));

    $cmblink = new_cmb2_box(array(
        'id'           => 'oeuvre_metabox_link',
        'title'        => __('Lien externe', 'cmb2'),
        'object_types' => array('oeuvre'),
        'context'      => 'side',
        'priority'     => 'low',
        'show_names'   => true,
    ));

    $cmblink->add_field(array(
        'name' => 'URL',
        'desc' => 'En remplacement du document IIIF. S\'affiche si "Adresse du manifest" est vide',
        'id'   => 'side_link',
        'type' => 'text_url',
    ));

}