/**
 * Registers a new block provided a unique name and an object defining its behavior.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/#registering-a-block
 */
import {registerBlockType} from '@wordpress/blocks';

/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import {__} from '@wordpress/i18n';

import {Fragment} from '@wordpress/element';
import {Button} from '@wordpress/components';
import {MediaUpload, MediaPlaceholder, MediaUploadCheck, RichText} from '@wordpress/block-editor';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * All files containing `style` keyword are bundled together. The code used
 * gets applied both to the front of your site and to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './style.scss';
import './editor.scss';

/**
 * Every block starts by registering a new block type definition.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/#registering-a-block
 */
registerBlockType('expo/carrousel', {
    /**
     * This is the display title for your block, which can be translated with `i18n` functions.
     * The block inserter will show this name.
     */
    title: __('Expo Carrousel', 'create-block'),

    /**
     * This is a short description for your block, can be translated with `i18n` functions.
     * It will be shown in the Block Tab in the Settings Sidebar.
     */
    description: __(
        'Carrousel d\'images',
        'create-block'
    ),

    /**
     * Blocks are grouped into categories to help users browse and discover them.
     * The categories provided by core are `common`, `embed`, `formatting`, `layout` and `widgets`.
     */
    category: 'expo',

    /**
     * An icon property should be specified to make it easier to identify a block.
     * These can be any of WordPress’ Dashicons, or a custom svg element.
     */
    icon: 'images-alt2',

    /**
     * Optional block extended support features.
     */
    supports: {
        // Removes support for an HTML mode.
        html: false,
    },

    attributes: {
        images  : {
            type   : "array",
            default: []
        },
        caption : {
            type    : "string",
            default : '',
            selector: 'figcaption',
        }
    },

    example: {
        attributes: {
            images: [],
            caption: 'Légende',
        },
    },

    /**
     * @see ./edit.js
     */
    edit: function (props) {
        const {images, caption} = props.attributes;

        function removeImage(removeImg, currentImages) {
            // Filter out the image we're deleting
            const filterImages  = currentImages.filter(img => img.id != removeImg.id);
            // Reset the ID's to the new index
            const updatedImages = filterImages.map((img, index) => {
                if (img.id != removeImg.id) {
                    return {
                        id          : index,
                        imgid       : img.imgid,
                        url         : img.url,
                        thumbnailUrl: img.thumbnailUrl,
                        alt         : img.alt,
                        caption     : img.caption
                    };
                }
            });
            props.setAttributes({
                images: updatedImages
            });
        }

        function addImage(selectedImage, selectedImages, selectedImageIndex) {
            let url = selectedImage.sizes.full.url;
            if (selectedImage.sizes.large) {
                url = selectedImage.sizes.large.url;
            }

            const updatedImage = {
                id          : selectedImageIndex,
                imgid       : selectedImage.id,
                url         : url,
                thumbnailUrl: selectedImage.sizes.thumbnail.url,
                alt         : selectedImage.alt,
                caption     : selectedImage.caption
            };
            // Insert our new image into the array after the current index.
            selectedImages.splice(selectedImageIndex + 1, 0, updatedImage);
            const updatedImages = selectedImages.map((img, index) => {
                return {
                    id          : index,
                    imgid       : img.id,
                    url         : img.url,
                    thumbnailUrl: img.thumbnailUrl,
                    alt         : img.alt,
                    caption     : img.caption
                };
            });

            props.setAttributes({
                images: updatedImages
            });
        }

        // Replace the image with the new selected one
        // need to update the specific attribute image with this image
        const onSelectImage = function (selectedImage,
                                        selectedImages,
                                        selectedImageIndex) {
            const updatedImages = selectedImages.map(img => {
                if (img.id === selectedImageIndex) {
                    let url = selectedImage.sizes.full.url;
                    if (selectedImage.sizes.large) {
                        url = selectedImage.sizes.large.url;
                    }

                    return {
                        id          : selectedImageIndex,
                        imgid       : selectedImage.id,
                        url         : url,
                        thumbnailUrl: selectedImage.sizes.thumbnail.url,
                        alt         : selectedImage.alt,
                        caption     : selectedImage.caption
                    };
                } else {
                    return img;
                }
            });
            props.setAttributes({
                images: updatedImages
            });
        };

        // Add an id to the array of selected images and update the img attribute
        const onSelectImages = function (selectedImages) {
            const updatedImages = selectedImages.map((img, index) => {
                let url = img.sizes.full.url;
                if (img.sizes.large) {
                    url = img.sizes.large.url;
                }

                return {
                    id          : index,
                    imgid       : img.id,
                    url         : url,
                    thumbnailUrl: img.sizes.thumbnail.url,
                    alt         : img.alt,
                    caption     : img.caption
                };
            });
            props.setAttributes({
                images: updatedImages
            });
        };

        if (images.length > 0) {
            return [
                <Fragment>
                    {images.map((img, imgMapIndex) => {
                        return [
                            <div className="media-row mcr-media-row">
                                <MediaUploadCheck>
                                    <MediaUpload
                                        onSelect={selectedImg =>
                                            onSelectImage(selectedImg, images, imgMapIndex)
                                        }
                                        type="image"
                                        value={img.imgid}
                                        accept="image/*"
                                        className=""
                                        render={({open}) => (
                                            <Button className={"image-button"} onClick={open}>
                                                <img src={img.url}/>
                                            </Button>
                                        )}
                                    />
                                    <div className="mcr-media-row--delete-button">
                                        <Button
                                            className={"button button-large"}
                                            onClick={() => {
                                                removeImage(img, images);
                                            }}
                                        >
                                            X
                                        </Button>
                                    </div>
                                </MediaUploadCheck>
                                <div className="mcr-media-row--add-button">
                                    <MediaUpload
                                        onSelect={selectedImage =>
                                            addImage(selectedImage, images, imgMapIndex)
                                        }
                                        accept="image/*"
                                        type="image"
                                        render={({open}) => (
                                            <Button
                                                className={"button button-large"}
                                                onClick={open}
                                            >
                                                Ajouter une image
                                            </Button>
                                        )}
                                    />
                                </div>
                            </div>
                        ];
                    })}
                    <RichText
                        tagName="figcaption"
                        className="caption"
                        label="Légende : "
                        placeholder="Légende"
                        value={caption}
                        onChange={(val) => props.setAttributes({caption: val})}
                    />
                </Fragment>
            ];
        } else {
            return (
                <Fragment>
                    <div className={props.className}>
                        <MediaPlaceholder
                            icon="format-gallery"
                            className={props.className}
                            labels={{
                                title: __("Carrousel"),
                                name : __("images")
                            }}
                            onSelect={onSelectImages}
                            accept="image/*"
                            type="image"
                            multiple
                        />
                    </div>
                </Fragment>
            );
        }
    },

    /**
     * @see ./save.js
     */
    save: function (props) {
        const {images, caption} = props.attributes;
        return (
            <div className="swiper_element_cont">
                <div className={`swiper-container mcr-swiper-container js-mcr-swiper-container`}>
                    <div className="swiper-wrapper mcr-swiper-wrapper">
                        {images.map((image, index) => {
                            return (
                                <div className="swiper-slide mcr-swiper-slide">
                                    <img src={image.url} alt={image.alt}/>
                                </div>
                            );
                        })}
                    </div>
                </div>
                <div
                    className="js-mcr-swiper-button-prev swiper-button-prev mcr-swiper-button-prev border_colored colored">
                    <span>Précédent</span>
                </div>
                <div className="figcaption_cont">
                    <figcaption>{caption}</figcaption>
                </div>
                <div
                    className="js-mcr-swiper-button-next swiper-button-next mcr-swiper-button-next border_colored colored">
                    <span>Suivant</span>
                </div>
            </div>
        );
    }
});
