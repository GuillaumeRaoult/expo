/**
 * Registers a new block provided a unique name and an object defining its behavior.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/#registering-a-block
 */
import { registerBlockType } from '@wordpress/blocks';
import {RichText} from '@wordpress/block-editor';

/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * All files containing `style` keyword are bundled together. The code used
 * gets applied both to the front of your site and to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './style.scss';
import './editor.scss';

/**
 * Every block starts by registering a new block type definition.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/#registering-a-block
 */
registerBlockType( 'expo/extract', {
	/**
	 * This is the display title for your block, which can be translated with `i18n` functions.
	 * The block inserter will show this name.
	 */
	title: __( 'Expo Résumé', 'expo-extract' ),

	/**
	 * This is a short description for your block, can be translated with `i18n` functions.
	 * It will be shown in the Block Tab in the Settings Sidebar.
	 */
	description: __(
		'Bloc résumé',
		'expo-extract'
	),

	/**
	 * Blocks are grouped into categories to help users browse and discover them.
	 * The categories provided by core are `common`, `embed`, `formatting`, `layout` and `widgets`.
	 */
	category: 'expo',

	/**
	 * An icon property should be specified to make it easier to identify a block.
	 * These can be any of WordPress’ Dashicons, or a custom svg element.
	 */
	icon: 'format-aside',

	/**
	 * Optional block extended support features.
	 */
	supports: {
		// Removes support for an HTML mode.
		html: false,
	},

    attributes: {
        title  : {
            type   : "string",
			default: "En résumé",
            selector: ".title"
        },
        content : {
            type    : "string",
            default: "Extrait",
            selector: ".content"
        }
    },

    example: {
        attributes: {
            title: 'En résumé',
            content: 'Extrait',
        },
    },

	/**
	 * @see ./edit.js
	 */
    edit: function ({attributes, className, setAttributes, isSelected}) {
        return (
        	<div className={className}>
                <RichText
                    tagName="p"
                    className="extract_title colored"
                    label=""
                    placeholder="En résumé"
                    allowedFormats={[]}
                    withoutInteractiveFormatting
                    value={attributes.title}
                    onChange={(val) => setAttributes({title: val})}
                />
                <RichText
                    tagName="p"
                    className="extract_content"
                    label=""
                    placeholder=""
                    allowedFormats={[]}
                    withoutInteractiveFormatting
                    value={attributes.content}
                    onChange={(val) => setAttributes({content: val})}
                />
			</div>
		)
    },

    /**
     * @see ./save.js
     */
    save: ({attributes, className}) => {
        return (
        	<div className={className}>
				<p className="extract_title colored">
					{attributes.title}
				</p>
				<p className="extract_content" dangerouslySetInnerHTML={{__html: attributes.content}}>
				</p>
			</div>
        );
    }
} );
