<footer class="content-info" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="widget-area clearfix">
                <?php dynamic_sidebar('sidebar-footer'); ?>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
