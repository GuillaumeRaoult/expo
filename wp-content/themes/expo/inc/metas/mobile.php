<?php
add_action('cmb2_admin_init', 'cmb2_expo_metaboxes_mobile_image');
function cmb2_expo_metaboxes_mobile_image()
{
    $cmbMobileImage = new_cmb2_box(array(
        'id'           => 'metabox_mobile_image',
        'title'        => __('Image affichage mobile', 'cmb2'),
        'object_types' => array('page', 'chapitre', 'temoignage', 'oeuvre'),
        'context'      => 'side',
        'priority'     => 'low',
        'show_names'   => true,
    ));

    $cmbMobileImage->add_field(array(
        'name'         => '',
        'id'           => 'mobile_image_thumbnail',
        'type'         => 'file',
        'options'      => array(
            'url' => false
        ),
        'query_args'   => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png',
            ),
        ),
        'preview_size' => 'medium',
    ));
}
