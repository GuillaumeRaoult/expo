<?php /* Template Name: Lecteur vidéo */ ?>


<?php
set_query_var('header-color', 'white sticky-white open-white sticky-open-white');
set_query_var('footer', 'no-footer');
get_header();
get_template_part('template-parts/breadcrumbs', '');

$videosList = [];
if (has_blocks(get_the_content())) {
    $blocks = parse_blocks(get_the_content());
    foreach ($blocks as $block) {
        if ($block['blockName'] === 'core/video') {
            preg_match('/<video.*src="(.*)".*><\/video>(?:<figcaption>(.*)<\/figcaption>)*?<\/figure>/u', $block['innerHTML'], $matches);
            $video = array();
            if ($matches && isset($matches[1])) {
                $video['url']     = $matches[1];
                $video['caption'] = '';
            }
            if ($matches && isset($matches[2])) {
                $video['caption'] = $matches[2];
            }
            if ($video) {
                $videosList[] = $video;
            }
        }
    }
}

?>
    <div class="container-fluid">
        <div class="row">
            <div class="video_cont">
                <?php
                if ($videosList) {
                    ?>
                    <video width="100%" id="video_expo" class="video-js">
                        <source src="<?php echo $videosList[0]['url'] ?>"
                                type="video/mp4">
                        Votre navigateur ne supporte pas les vidéos.
                    </video>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="video_info">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-sm-7 col-xl-6 pres">
                            <h1>
                                <span class="colored titre">Vidéo en cours</span>
                                <span class="caption">
                                <?php
                                if ($videosList) {
                                    echo $videosList[0]['caption'];
                                }
                                ?>
                                </span>
                            </h1>
                        </div>
                        <div class="col-12 col-sm-5 col-xl-6 no-mobile-padding">
                            <div class="playlist">
                                <div class="row justify-content-end no-gutters">
                                    <?php
                                    if ($videosList) {
                                        ?>
                                        <div class="prev d-lg-block d-none">
                                            <div class="swiper-nav swiper-prev swiper-prev-video">
                                                <span class="dashicons dashicons-arrow-left-alt"></span>
                                            </div>
                                        </div>
                                        <div class="swipecont">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <?php
                                                    foreach ($videosList as $i => $video) {
                                                        ?>
                                                        <div class="swiper-slide video">
                                                            <a href="javscript:void(0)" title="<?php echo $video['caption'] ?>">
                                                                <video data-caption="<?php echo $video['caption'] ?>" preload="metadata">
                                                                    <source src="<?php echo $video['url'] ?>#t=10,20"
                                                                            type="video/mp4">
                                                                    Votre navigateur ne supporte pas les vidéos.
                                                                </video>
                                                                <div class="play_icon">
                                                                    <?php echo $i + 1 ?>
                                                                </div>
                                                                <div class="progress"></div>
                                                            </a>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="next d-lg-block d-none">
                                            <div class="swiper-nav swiper-next swiper-next-video">
                                                <span class="dashicons dashicons-arrow-right-alt"></span>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
