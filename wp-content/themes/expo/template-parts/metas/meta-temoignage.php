<?php
/**
 * Template part for displaying meta in temoignage page
 *
 * @package Expo
 */

$temoignages = get_posts(array(
    'post_type'        => 'temoignage',
    'posts_per_page'   => '-1',
    'suppress_filters' => false
));

if ($temoignages) {
    ?>
    <div class="meta_cont transparent">
        <ul>
            <?php
            global $post;
            $currentId = $post->ID;
            foreach ($temoignages as $post) {
                setup_postdata($post);
                $current = $currentId == $post->ID;
                ?>
                <li>
                    <?php
                    if ($current) {
                        ?>
                        <span class="colored"><?php the_title() ?></span>
                        <?php
                    } else {
                        ?>
                        <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                        <?php
                    }
                    ?>
                </li>
                <?php
            }
            wp_reset_postdata();
            ?>
        </ul>
    </div>
    <?php
}


