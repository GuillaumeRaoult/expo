<?php
/**
 * Template part for displaying breadcrumbs
 *
 * @package Expo
 */

global $post;

if (!get_query_var('borne', '')) {
    ?>
    <div class="container-fluid">
        <div class="row" id="breadcrumbs_header">
            <div class="col-12">
                <nav id="breadcrumbs">
                    <ul>
                        <li>
                            <a class="hover_colored" href="<?php echo get_home_url() ?>">Accueil</a>
                        </li>
                        <?php
                        if (is_singular('oeuvre')) {
                            $parcours = getPageByTemplate('template-parcours.php');
                            if ($parcours) {
                                ?>
                                <li>
                                    <a class="hover_colored"
                                       href="<?php echo get_permalink($parcours[0]); ?>"><?php echo get_the_title($parcours[0]); ?></a>
                                </li>
                                <?php
                            }
                        }

                        if (is_singular('temoignage')) {
                            $subtitle = get_post_meta(get_the_ID(), 'temoignage_subtitle', true);
                            if ($subtitle) {
                                ?>
                                <li>
                                    <span><?php echo $subtitle ?></span>
                                </li>
                                <?php
                            }
                        }

                        if (is_singular('post')) {
                            $pageForPosts = get_option('page_for_posts');
                            if ($pageForPosts) {
                                ?>
                                <li>
                                    <a class="hover_colored"
                                       href="<?php echo get_permalink($pageForPosts); ?>">Agenda</a>
                                </li>
                                <?php
                            }
                        }

                        if (is_page() && !is_404()) {
                            $parentID = wp_get_post_parent_id(get_the_ID());
                            if ($parentID) {
                                $parent = get_post($parentID);
                                if ($parent) {
                                    ?>
                                    <li>
                                        <a class="hover_colored" href="<?php echo get_permalink($parent); ?>"><?php echo get_the_title($parent); ?></a>
                                    </li>
                                    <?php
                                }
                            }
                        }

                        if (is_home()) {
                            ?>
                            <li>
                                <span>Agenda</span>
                            </li>
                            <?php
                        } else {
                            if (is_404()) {
                                $title = 'Page non trouvée';
                            } else {
                                $title = $post->post_title;
                            }
                            ?>
                            <li>
                                <span><?php echo $title ?></span>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <?php
}