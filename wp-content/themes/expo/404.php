<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Expo
 */

set_query_var('header-color', 'transparent sticky-white open-white sticky-open-white');
get_header();

set_query_var('subtitle', '');

get_template_part('template-parts/title', '');
get_template_part('template-parts/breadcrumbs', '');
?>
    <div id="main_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-10 offset- sm-1 col-md-8 offset-md-2 text-center">
                    <p>La page que vous recherchez n'a pas pu être trouvée, mais vous pouvez revenir à l'<a href="<?php echo esc_url(home_url('/')); ?>">accueil</a>.</p>
                </div>
            </div>
        </div>
        <?php
        if (!get_query_var('borne', '')) {
            ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-6 offset-3">
                        <div class="single_footer">
                            <p class="author">Partager</p>
                            <p class="share">
                                <?php
                                shareButtons();
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php
get_footer();
