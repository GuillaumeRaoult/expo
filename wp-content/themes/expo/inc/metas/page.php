<?php
/**
 * Metas pour les chapitres
 * User: guillaume
 * Date: 25/08/20
 * Time: 10:02
 */

add_action('cmb2_admin_init', 'cmb2_expo_metaboxes_page');
function cmb2_expo_metaboxes_page()
{
	$cmbChapitre = new_cmb2_box(array(
		'id'           => 'page_metabox',
		'title'        => __('Informations', 'cmb2'),
		'object_types' => array('page'),
		'show_on'      => array(
			'key'   => 'page',
			'value' => array(
				'template' => 'page'
			)
		),
		'context'      => 'side',
		'priority'     => 'low',
		'show_names'   => true,
	));

	$cmbChapitre->add_field(array(
		'name'       => 'Sous-titre',
		'id'         => 'page_subtitle',
		'type'       => 'text',
		'attributes' => array(
			'placeholder' => '',
		),
	));

	$cmbChapitre->add_field(array(
		'name'    => 'Grand format',
		'id'      => 'page_format',
		'type'    => 'radio_inline',
		'options' => array(
			'yes' => __('Oui', 'cmb2'),
			'no'  => __('Non', 'cmb2')
		),
		'default' => 'no',
	));
}

function showCmbRegularPage($display, $meta_box)
{
	if (!isset($meta_box['show_on']['key'])) {
		return $display;
	}

	if ('page' !== $meta_box['show_on']['key']) {
		return $display;
	}

	$post_id = 0;

	// If we're showing it based on ID, get the current ID
	if (isset($_GET['post'])) {
		$post_id = $_GET['post'];
	} elseif (isset($_POST['post_ID'])) {
		$post_id = $_POST['post_ID'];
	}

	if (!$post_id) {
		return false;
	}

	$post = get_post($post_id);
	if (!$post) {
		return false;
	}

	$template = get_page_template_slug($post);

	return $template === '';
}

add_filter('cmb2_show_on', 'showCmbRegularPage', 10, 2);

add_action('cmb2_admin_init', 'cmb2_expo_metaboxes_frontpage');
function cmb2_expo_metaboxes_frontpage()
{
	$cmbFrontpage = new_cmb2_box(array(
		'id'           => 'page_metabox',
		'title'        => __('Informations', 'cmb2'),
		'object_types' => array('page'),
		'show_on_cb'   => 'showCmbFrontpage',
		'context'      => 'side',
		'priority'     => 'low',
		'show_names'   => true,
	));

	$cmbFrontpage->add_field(array(
		'name' => 'Vidéo',
		'id'   => 'page_video',
		'desc' => 'Afficher la vidéo ?',
		'type' => 'checkbox',
	));

	$cmbFrontpage->add_field(array(
		'name' => 'Actu',
		'id'   => 'page_news',
		'desc' => 'Afficher l\'actu ?',
		'type' => 'checkbox',
	));
}

function showCmbFrontpage($cmb)
{
	$front_page = get_option('page_on_front');

	return $cmb->object_id() == $front_page;
}

function cmb2_expo_metaboxes_borne_agenda()
{
	$cmbFrontpage = new_cmb2_box(array(
		'id'           => 'borne_page_metabox',
		'title'        => __('Informations', 'cmb2'),
		'object_types' => array('page'),
		'show_on'      => array('key' => 'page-template', 'value' => 'template-agenda-borne.php'),
		'context'      => 'normal',
		'priority'     => 'low',
		'show_names'   => true,
	));

	$cmbFrontpage->add_field(array(
		'name' => 'Titre partie gauche',
		'id'   => 'agenda_borne_title',
		'desc' => get_bloginfo('name'),
		'type' => 'text',
	));

	$cmbFrontpage->add_field(array(
		'name' => 'Sous-titre partie gauche',
		'id'   => 'agenda_borne_subtitle',
		'desc' => 'Les prochains évènements',
		'type' => 'text',
	));
}

add_action('cmb2_admin_init', 'cmb2_expo_metaboxes_borne_agenda');
