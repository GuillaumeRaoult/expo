<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Expo
 */

set_query_var('header-color', 'transparent sticky-white open-white sticky-open-white');
get_header();

get_template_part('template-parts/title', '');
get_template_part('template-parts/breadcrumbs', '');
?>
    <div id="main_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-8 offset-sm-2">
                    <div class="posts_cont row">
                        <?php
                        if (have_posts()) {
                            while (have_posts()) :
                                the_post();
                                ?>
                                <div class="col-12">
                                    <a href="<?php the_permalink() ?>" class="oeuvre">
                                        <?php the_post_thumbnail() ?>
                                        <?php
                                        $categories = get_the_category();
                                        $category   = '';
                                        if ($categories) {
                                            $cat      = $categories[0];
                                            $category = $cat->name;
                                        }

                                        $place = get_post_meta(get_the_ID(), 'post_place', true);
                                        $start = get_post_meta(get_the_ID(), 'post_start', true);
                                        $end   = get_post_meta(get_the_ID(), 'post_end', true);
                                        ?>
                                        <h2>
                                            <?php
                                            if ($start || $end || $category) {
                                                ?>
                                                <p class="meta_cont">
                                                    <span class="meta bg_colored">
                                                        <?php
                                                        if ($start) {
                                                            echo date('d/m/Y', $start);
                                                        }
                                                        if ($end) {
                                                            echo ' > ' . date('d/m/Y', $end);
                                                        }
                                                        if ($category) {
                                                            if ($start || $end) {
                                                                echo ' - ';
                                                            }
                                                            echo $category;
                                                        }
                                                        ?>
                                                    </span>
                                                </p>
                                                <?php
                                            }
                                            ?>
                                            <?php the_title() ?>
                                            <?php
                                            if ($place) {
                                                ?>
                                                <span class="places"><?php echo $place ?></span>
                                                <?php
                                            }
                                            ?>
                                            <span class="link colored">EN SAVOIR +</span>
                                        </h2>
                                    </a>
                                </div>
                            <?php
                            endwhile;
                            the_posts_navigation();
                        } else {
                            ?>
                            <div class="no_posts">
                                <h2>Aucun évènement à venir</h2>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
