<?php
/*
Plugin Name: Expo - Bornes
Description: Gestion des pages bornes
Version: 1.0.0
Author: Guillaume Raoult
License: GPL2
Text Domain: borne
*/

// don't call the file directly
if (!defined('ABSPATH')) {
    exit;
}

/**
 * WeDocs class.
 *
 * @class WeDocs The class that holds the entire Borne plugin
 */
final class Borne
{

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->activate();
    }

    /**
     * The plugin activation function.
     *
     * @since 1.3
     *
     * @return void
     */
    public function activate()
    {
        add_action('cmb2_admin_init', [
            $this,
            'createOptionPage'
        ], 100);

        add_action('cmb2_admin_init', [
            $this,
            'createPostTypesMetas'
        ], 100);

        add_action('admin_enqueue_scripts', [
            $this,
            'addOptionsScripts'
        ]);

        add_filter('cmb2_save_options-page_fields_borne_options_metabox', [
            $this,
            'saveOptionsPage'
        ]);

        add_action('save_post', [
            $this,
            'saveOptionsPage'
        ]);

        add_filter('init', [
            $this,
            'createNewRewriteRules'
        ]);

        add_filter('query_vars', [
            $this,
            'addQueryVar'
        ]);

        add_filter('template_redirect', [
            $this,
            'editContentUrls'
        ]);

        add_filter('get_pages', [
            $this,
            'filter_posts'
        ], 10, 2);

        add_filter('the_posts', [
            $this,
            'filter_posts'
        ], 10, 1);

        add_filter('the_content', [
            $this,
            'filter_content_links'
        ], 1);

        add_filter('body_class', [
            $this,
            'add_borne_body_class'
        ]);

        add_action( 'wp_ajax_validate_urls', [$this, 'validate_urls'] );
        add_action( 'wp_ajax_nopriv_validate_urls', [$this, 'validate_urls'] );
    }

    public function createOptionPage()
    {
        $cmb_options = new_cmb2_box(array(
            'id'           => 'borne_options_metabox',
            'title'        => esc_html__('Bornes', 'borne'),
            'object_types' => array('options-page'),
            'option_key'   => 'borne_options',
            'menu_title'   => esc_html__('Borne', 'borne'),
            'parent_slug'  => 'partenaires_options',
            'tab_group'    => 'partenaires_options',
            'tab_title'    => 'Borne'
        ));

        $group_field_id = $cmb_options->add_field(array(
            'id'          => 'options_url_group',
            'type'        => 'group',
            'description' => __('Groupe d\'adresses', 'borne'),
            'options'     => array(
                'group_title'   => __('Groupe {#}', 'borne'),
                'add_button'    => __('Ajouter un autre groupe', 'borne'),
                'remove_button' => __('Retirer le groupe', 'borne'),
                'sortable'      => true,
            ),
        ));

        $cmb_options->add_group_field($group_field_id, array(
            'name'            => 'Suffixe URL',
            'desc'            => 'Le suffixe de l\'URL du groupe. Ex. : <i>borne-oeuvres</i>. Le suffixe sera transformé à la sauvegarde si invalide.',
            'id'              => 'suffix',
            'type'            => 'text_small',
            'attributes'      => array(
                'class' => 'borne_suffix_input',
            ),
            'sanitization_cb' => [
                $this,
                'sanitize_suffix'
            ]
        ));

        $desc = 'Utiliser * comme joker pour ajouter tous les contenus identiques.<br/>';
        $desc .= 'Ex : <strong>' . get_site_url() . '/oeuvre/*/</strong> ajoutera toutes les oeuvres au même groupe ou <strong>' . get_site_url() . '/chapitre/*/</strong> pour les chapitres.<br/>';
        $desc .= 'Fonctionne également avec les pages : <strong>' . get_site_url() . '/mapage/*/</strong> ajoutera mapage et toutes les pages descendantes.<br/><br/>';
        $desc .= '<span style="color:red"><strong>Ne pas oublier de sauvegarder les changements pour créer les pages.</strong></span>';

        $cmb_options->add_group_field($group_field_id, array(
            'id'          => 'urls',
            'name'        => __('Adresses', 'borne'),
            'description' => $desc,
            'attributes'  => array(
                'class' => 'borne_url_input',
            ),
            'type'        => 'text_url',
            'repeatable'  => true,
            'options'     => array(
                'group_title'             => __('Adresse {#}', 'borne'),
                'add_row_text'            => __('Ajouter une adresse', 'borne'),
                'remove_row_button_title' => __('Retirer une adresse', 'borne'),
                'sortable'                => false,
            )
        ));
    }

    public function sanitize_suffix($value, $field_args, $field)
    {
        return sanitize_title($value);
    }

    public function createPostTypesMetas()
    {
        $cmb = new_cmb2_box(array(
            'id'           => 'borne_metabox',
            'title'        => __('Options Borne', 'cmb2'),
            'object_types' => array(
                'oeuvre',
                'chapitre',
                'temoignage',
                'page'
            ),
            'context'      => 'side',
            'priority'     => 'low',
            'show_names'   => true,
        ));

        $cmb->add_field(array(
            'name'       => '',
            'desc'       => 'Masquer le <i>header</i>',
            'id'         => 'hide_borne_header',
            'type'       => 'checkbox',
            'column'     => array(
                'position' => 4,
                'name'     => 'Groupe borne',
            ),
            'display_cb' => [
                $this,
                'column_display_borne'
            ]
        ));
    }

    public function column_display_borne($field_args, $field)
    {
        $borneUrls = get_option('borne_options');
        $groups    = $borneUrls['options_url_group'];
        $url       = get_the_permalink($field->object_id);
        $borneUrls = [];
        if ($groups) {
            foreach ($groups as $group) {
                if (isset($group['suffix']) && $group['suffix']) {
                    $suffix = $group['suffix'];
                    if (isset($group['urls']) && $group['urls']) {
                        set_query_var('borne', $suffix);
                        $siblings = $this->getGroupUrls($url);
                        if ($siblings) {
                            $borneUrls[$suffix] = $url . $suffix . '/';
                        }
                        set_query_var('borne', '');
                    }
                }
            }
        }


        ?>
        <div class="custom-column-display <?php echo $field->row_classes(); ?>">
            <?php
            if ($field->escaped_value() == 'on') {
                ?>
                <p class="description">Header masqué</p>
                <?php
            }
            foreach ($borneUrls as $suffix => $borneUrl) {
                $color = $this->generateColor($suffix);
                preg_match('/^#?([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i', $color, $parts);
                $darker = ""; // Prepare to fill with the results
                for ($i = 1; $i <= 3; $i++) {
                    $parts[$i] = hexdec($parts[$i]);
                    $parts[$i] = round($parts[$i] * 40 / 100); // 80/100 = 80%, i.e. 20% darker
                    $darker    .= str_pad(dechex($parts[$i]), 2, '0', STR_PAD_LEFT);
                }
                ?>
                <style type="text/css">
                    a.button-borne-<?php echo $suffix ?> {
                        background-color: <?php echo $color ?> !important;
                        color: # <?php echo $darker ?> !important;
                        border-color: # <?php echo $darker ?> !important;
                    }
                </style>
                <a class="button button-secondary button-link-original button-borne-<?php echo $suffix ?>"
                   target="_blank" href="<?php echo $borneUrl ?>">Borne <?php echo $suffix ?></a>
                <?php
            }
            ?>
        </div>
        <?php
    }

    private function generateColor($name)
    {
        $hash = md5($name);

        $color1 = hexdec(substr($hash, 8, 2));
        $color2 = hexdec(substr($hash, 4, 2));
        $color3 = hexdec(substr($hash, 0, 2));
        if ($color1 < 128)
            $color1 += 128;
        if ($color2 < 128)
            $color2 += 128;
        if ($color3 < 128)
            $color3 += 128;

        return "#" . dechex($color1) . dechex($color2) . dechex($color3);
    }

    public function addOptionsScripts($page)
    {
        if ($page != 'options-de-lexpo_page_borne_options') {
            return;
        }
        wp_enqueue_script('borne-admin-script', plugin_dir_url(__FILE__) . '/borne.js', array('jquery'), filemtime(plugin_dir_path(__FILE__) . 'borne.js'), true);
        wp_enqueue_style('borne-admin-style', plugin_dir_url(__FILE__) . '/borne.css');
        wp_localize_script('borne-admin-script', 'bornejs', array(
            'ajaxurl' => admin_url('admin-ajax.php')
        ));
    }

    public function addQueryVar($query_vars)
    {
        $query_vars[] = 'borne';

        return $query_vars;
    }

    public function saveOptionsPage()
    {
        update_option('flush_borne_rules', true);
    }

    public function createNewRewriteRules()
    {
        global $wp_rewrite;
        $bornePosts = [];
        $urls       = get_option('borne_options');
        if ($urls) {
            $groups = $urls['options_url_group'];
            if ($groups) {
                foreach ($groups as $group) {
                    if (isset($group['suffix']) && $group['suffix']) {
                        $suffix = $group['suffix'];
                        if (isset($group['urls']) && $group['urls']) {
                            foreach ($group['urls'] as $url) {
                                if (strpos($url, '*')) {
                                    $url     = rtrim($url, '/') . '/';
                                    $postUrl = str_replace('*/', '', $url);
                                    $postID  = url_to_postid($postUrl);
                                    $post    = get_post($postID);
                                    if ($post && $post->post_type === 'page') {
                                        $bornePosts[$suffix][] = get_post($postID);
                                        $pages                 = get_pages([
                                            'child_of' => $postID,
                                        ]);
                                        foreach ($pages as $childPage) {
                                            $bornePosts[$suffix][] = $childPage;
                                        }
                                    } else {
                                        foreach ($wp_rewrite->extra_permastructs as $postType => $struct) {
                                            $postTypeUrl = str_replace('%' . $postType . '%', '', $struct['struct']);
                                            if (get_site_url() . $postTypeUrl == $postUrl) {
                                                $posts = get_posts(array(
                                                    'post_type'      => $postType,
                                                    'posts_per_page' => '-1',
                                                ));
                                                foreach ($posts as $post) {
                                                    if ($post) {
                                                        $bornePosts[$suffix][] = $post;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $postID = url_to_postid($url);
                                    $post   = get_post($postID);
                                    if ($post) {
                                        $bornePosts[$suffix][] = $post;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($bornePosts as $suffix => $posts) {
            $cleanSuffix = rtrim($suffix, '/');
            $suffix      = $cleanSuffix . '/';
            foreach ($posts as $post) {
                if ($post->post_type == 'page') {
                    $pageUrl = get_the_permalink($post);
                    $pageUrl = str_replace(get_site_url(), '', $pageUrl);
                    $pageUrl = ltrim($pageUrl, '/');
                    $rewrite = 'index.php?page_id=' . $post->ID . '&borne=' . $cleanSuffix;
                } else {
                    $pageUrl = $post->post_type . '/' . $post->post_name . '/';
                    $rewrite = 'index.php?' . $post->post_type . '=' . $post->post_name . '&borne=' . $cleanSuffix;
                }
                add_rewrite_rule($pageUrl . $suffix . '?$', $rewrite, 'top');
            }
        }

        $shouldFlush = get_option('flush_borne_rules', false);
        if ($shouldFlush) {
            flush_rewrite_rules();
            update_option('flush_borne_rules', false);
        }
    }

    /**
     * Parse all links function (get_permalink, the_permalink) to alter url if borne group
     */
    public function editContentUrls()
    {
        if (get_query_var('borne', '')) {
            foreach ([
                         'post',
                         'page',
                         'post_type'
                     ] as $type
            ) {
                add_filter($type . '_link', [
                    $this,
                    'check_borne_urls'
                ], 10, 1);
            }
        }
    }

    /**
     * Filter all posts / pages
     *
     * @param $posts
     *
     * @return mixed
     */
    public function filter_posts($posts)
    {
        $currentVar = get_query_var('borne', '');
        if ($currentVar) {
            foreach ($posts as $k => $post) {
                set_query_var('borne', '');
                $postUrl = get_permalink($post);
                set_query_var('borne', $currentVar);
                $siblings = $this->getGroupUrls($postUrl);
                if (!in_array($postUrl, $siblings)) {
                    unset($posts[$k]);
                }
            }
        }

        return $posts;
    }

    /**
     * Rewrite all links to add /suffix/ if in same group
     *
     * @param $url
     *
     * @return string
     */
    public function check_borne_urls($url)
    {
        $currentVar = get_query_var('borne', '');
        $siblings   = $this->getGroupUrls($url);
        $isInternal = $this->isInternalUrl($url);
        if ($isInternal && in_array($url, $siblings)) {
            $url .= $currentVar . '/';
        }

        return $url;
    }

    /**
     * Remove unauthorized links in content
     *
     * @param $content
     *
     * @return null|string|string[]
     */
    public function filter_content_links($content)
    {
        $currentVar = get_query_var('borne', '');
        if ($currentVar) {
            $currentUrl = get_permalink(get_the_ID());
            $currentUrl = str_replace($currentVar . '/', '', $currentUrl);
            $siblings   = $this->getGroupUrls($currentUrl);

            $dom = new DOMDocument;
            libxml_use_internal_errors(true);
            $dom->loadHTML('<?xml encoding="utf-8" ?>' . $content);
            $links            = $dom->getElementsByTagName('a');
            $elementsToRemove = [];
            foreach ($links as $link) {
                //Do not remove definitions
                if ($link->getAttribute('data-type') === 'definition') {
                    continue;
                }

                $url        = $link->getAttribute('href');
                $isInternal = $this->isInternalUrl($url);
                if (!in_array($url, $siblings)) {
                    $elementsToRemove[] = $link;
                } else if ($isInternal) {
                    $link->removeAttribute('href');
                    $link->setAttribute('href', $url . $currentVar . '/');
                }
            }

            foreach ($elementsToRemove as $element) {
                if ($element->nodeValue) {
                    $text = new DomText($element->nodeValue);
                    $element->parentNode->insertBefore($text, $element);
                } else if ($element->childNodes) {
                    $children = $element->childNodes[0];
                    $element->parentNode->insertBefore($children, $element);
                }
                $element->parentNode->removeChild($element);
            }

            $content = $dom->saveHTML();
        }

        return $content;
    }

    /**
     * Add borne body class
     *
     * @param $classes
     *
     * @return mixed
     */
    public function add_borne_body_class($classes)
    {
        if (get_query_var('borne', false)) {
            $classes = array_merge($classes, array('borne'));
        }

        return $classes;
    }

    /**
     * Find all associated urls
     *
     * @param $url
     *
     * @return array
     */
    public function getGroupUrls($url)
    {
        $siblings   = [];
        $currentVar = get_query_var('borne', '');
        if ($currentVar) {
            $borneUrls = get_option('borne_options');
            $groups    = $borneUrls['options_url_group'];
            if ($groups) {
                set_query_var('borne', '');
                $currentPostId = url_to_postid($url);
                set_query_var('borne', $currentVar);
                $currentPost = get_post($currentPostId);
                $wildcards   = [];
                if ($currentPost->post_type == 'page') {
                    set_query_var('borne', '');
                    $wildcards[] = get_permalink($currentPostId) . '*/';
                    $ancestors   = get_post_ancestors($currentPostId);
                    set_query_var('borne', $currentVar);
                    foreach ($ancestors as $ancestor) {
                        set_query_var('borne', '');
                        $wildcards[] = get_permalink($ancestor) . '*/';
                        set_query_var('borne', $currentVar);
                    }
                } else {
                    $wildcards[] = get_site_url() . '/' . $currentPost->post_type . '/*/';
                }

                foreach ($groups as $group) {
                    if (!isset($group['suffix']) || $group['suffix'] != $currentVar) {
                        continue;
                    }
                    if (isset($group['urls']) && $group['urls']) {
                        if (in_array($url, $group['urls'])) {
                            foreach ($group['urls'] as $groupUrl) {
                                $siblings[] = $groupUrl;
                            }
                        }
                        foreach ($wildcards as $wildcard) {
                            if (in_array($wildcard, $group['urls'])) {
                                $url         = rtrim($url, '/') . '/';
                                $wildcardUrl = str_replace('*/', '', $wildcard);
                                $wildcardId  = url_to_postid($wildcardUrl);
                                $siblings[]  = $wildcardUrl;
                                set_query_var('borne', '');
                                if ($currentPost->post_type == 'page') {
                                    $posts = get_pages([
                                        'child_of' => $wildcardId,
                                    ]);
                                } else {
                                    $posts = get_posts(array(
                                        'post_type'      => $currentPost->post_type,
                                        'posts_per_page' => '-1',
                                    ));
                                }
                                set_query_var('borne', $currentVar);
                                foreach ($posts as $post) {
                                    set_query_var('borne', '');
                                    $siblings[] = get_the_permalink($post);
                                    set_query_var('borne', $currentVar);
                                }
                            }
                        }
                    }
                }
            }
        }

        return $siblings;
    }

    public function validate_urls()
    {
        $urls = $_POST['urls'];
        $invalids = [];
        foreach ($urls as $url) {
            if ($this->isInternalUrl($url)) {
                $baseUrl = $url;
                if (strpos($url, '*')) {
                    $baseUrl = str_replace('*/', '', $url);
                }
                if ($baseUrl != get_site_url().'/oeuvre/' && $baseUrl != get_site_url().'/chapitre/' && $baseUrl != get_site_url().'/temoignage/') {
                    $postID = url_to_postid($baseUrl);
                    if (!$postID) {
                        $invalids[] = $url;
                    }
                }
            }
        }

        echo wp_json_encode($invalids);
        wp_die();
    }

    private function isInternalUrl($url)
    {
        $parseUrl     = parse_url($url);
        $parseBlogUrl = parse_url(get_site_url());

        if (!isset($parseUrl['host'])) {
            return true;
        }

        return $parseUrl['host'] == $parseBlogUrl['host'];
    }
}

/**
 * Initialize the plugin.
 *
 * @return \Borne
 */
function borne()
{
    return new Borne();
}

// kick it off
global $borneController;
$borneController = borne();
