<?php
/**
 * The front page template file
 *
 * @package Tribe
 */

set_query_var('header-color', 'transparent sticky-black open-white sticky-open-black');

get_header();

?>

<div class="container-fluid overlay_cont">
    <div class="row overlay">
        <div class="col-sm-8 offset-sm-2 my-auto titles">
            <h1 class="site-title"><?php bloginfo('name'); ?></h1>
            <?php
            $expo_description = get_bloginfo('description', 'display');
            if ($expo_description || is_customize_preview()) {
                ?>
                <p class="site-description"><?php echo $expo_description ?></p>
                <?php
            }

            $showVideo      = get_post_meta(get_the_ID(), 'page_video', 1);
            $visitePagesIds = getPageByTemplate('template-lecteur-video.php');
            if ($visitePagesIds && $showVideo) {
                ?>
                <div class="accroche_video col-8 col-xl-5">
                    <?php
                    $visitePageId = $visitePagesIds[0];
                    $page         = get_post($visitePageId);
                    $content      = apply_filters('the_content', $page->post_content);
                    preg_match('/<video.*?src\s*=\s*[\'\"](.*?)[\'\"].*?/', $content, $matches);
                    $videoUrl = '';
                    if ($matches && isset($matches[1])) {
                        $videoUrl = $matches[1];
                        ?>
                        <a href="<?php echo get_permalink($page) ?>">
                            <div class="row align-items-center no-gutters">
                                <div class="col-sm-5 video_cont">
                                    <video width="100%" muted preload loop autoplay>
                                        <source src="<?php echo $videoUrl ?>#t=0,20" type="video/mp4">
                                        Votre navigateur ne supporte pas les vidéos.
                                    </video>
                                    <div class="play_icon">
                                        <span class="dashicons dashicons-controls-play"></span>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <p class="title colored">VOIR ET ÉCOUTER</p>
                                    <p class="subtitle"><?php echo get_the_title($page) ?></p>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="col-2 anchor bottom hover_bg_colored">
            <span class="dashicons dashicons-arrow-down-alt"></span>
        </div>
        <?php
        $stickies = get_option('sticky_posts');
        $posts    = get_posts([
            'post_type'   => 'post',
            'numberposts' => 1,
        ]);
		$showNews      = get_post_meta(get_the_ID(), 'page_news', true);
		if (($stickies || $posts) && $showNews) {
            $link = $stickies ? get_the_permalink($stickies[0]) : get_the_permalink(get_option('page_for_posts'));
            ?>
            <div class="col-10 col-sm-4 home_news_block bg_colored">
                <a href="<?php echo $link ?>">
                    <div class="row align-items-center">
                        <div class="col-9 news">
                            <?php
                            if ($stickies) {
                                $sticky     = $stickies[0];
                                $start      = get_post_meta($sticky, 'post_start', true);
                                $end        = get_post_meta($sticky, 'post_end', true);
                                $categories = get_the_category($sticky);
                                $category   = null;
                                if ($categories) {
                                    $category = $categories[0];
                                }

                                if ($start || $end || $category) {
                                    ?>
                                    <p>
                                        <?php
                                        if ($start) {
                                            echo wp_date('d M Y', $start);
                                        }
                                        if ($end) {
                                            echo ' > ' . wp_date('d M Y', $end);
                                        }
                                        if ($category) {
                                            echo ' - ' . $category->name;
                                        }
                                        ?>
                                    </p>
                                    <?php
                                }
                                ?>
                                <p><?php echo get_the_title($sticky) ?></p>
                                <?php
                            } else {
                                ?>
                                <p>Agenda</p>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="col-3 plus">
                            EN SAVOIR +
                        </div>
                    </div>
                </a>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<div id="main_content">
    <div class="container-fluid">
        <?php
        the_content();
        ?>
    </div>
</div>
<?php get_footer(); ?>

