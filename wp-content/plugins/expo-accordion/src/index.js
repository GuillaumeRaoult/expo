/**
 * Registers a new block provided a unique name and an object defining its behavior.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/#registering-a-block
 */
import {registerBlockType} from '@wordpress/blocks';

/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import {__} from '@wordpress/i18n';
import {BlockControls, InnerBlocks, RichText} from '@wordpress/block-editor';
import {Toolbar} from '@wordpress/components';
import React, {useEffect} from 'react';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * All files containing `style` keyword are bundled together. The code used
 * gets applied both to the front of your site and to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './style.scss';
import './editor.scss';

/**
 * Every block starts by registering a new block type definition.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/#registering-a-block
 */
registerBlockType('expo/accordion', {
    /**
     * This is the display title for your block, which can be translated with `i18n` functions.
     * The block inserter will show this name.
     */
    title: __('Expo Accordéon', 'expo-accordion'),

    /**
     * This is a short description for your block, can be translated with `i18n` functions.
     * It will be shown in the Block Tab in the Settings Sidebar.
     */
    description: __(
        'Un accordéon déroulable avec du contenu',
        'expo-accordion'
    ),

    /**
     * Blocks are grouped into categories to help users browse and discover them.
     * The categories provided by core are `common`, `embed`, `formatting`, `layout` and `widgets`.
     */
    category: 'expo',

    /**
     * An icon property should be specified to make it easier to identify a block.
     * These can be any of WordPress’ Dashicons, or a custom svg element.
     */
    icon: <svg version="1.0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
        <path
            d="M16.93,8.93a1,1,0,0,1-.7-.29L12,4.41,7.9,8.51A1,1,0,0,1,6.49,7.1L12,1.59l5.64,5.64a1,1,0,0,1,0,1.41A1,1,0,0,1,16.93,8.93Z"/>
        <path d="M12.07,22.35,6.42,16.71a1,1,0,0,1,1.42-1.42l4.23,4.23,4.09-4.1a1,1,0,0,1,1.42,1.42Z"/>
        <path d="M17.93,13H5.82a1,1,0,0,1,0-2H17.93a1,1,0,0,1,0,2Z"/>
    </svg>,

    /**
     * Optional block extended support features.
     */
    supports: {
        html: false,
    },

    attributes: {
        title   : {
            type    : 'string',
            selector: '.accordeon_title',
            source  : 'html',
            default : 'Titre',
        },
        titleTag: {
            type   : 'string',
            default: 'h2',
        },
        uuid    : {
            type: 'number',
        },
    },

    example: {
        attributes: {
            title   : 'Titre accordéon',
            titleTag: 'h3',
            uuid    : '10',
        },
    },

    edit: function (props) {
        const {title, titleTag, uuid} = props.attributes;
        const id                      = Math.floor(Math.random() * (10000 - 1) + 1);

        function getHTMLTagIcon(tag) {
            let icon;
            switch (tag) {
                case 'h2':
                    icon = <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12.09,15.88h-2v-4H6.45v4h-2V6.6h2v3.64h3.68V6.6h2Z"/>
                        <path
                            d="M20.27,15.88H13.78V14.52l2.33-2.36c.69-.71,1.14-1.2,1.36-1.47a3,3,0,0,0,.45-.76,1.78,1.78,0,0,0,.14-.72,1,1,0,0,0-.31-.83,1.13,1.13,0,0,0-.82-.28,2.45,2.45,0,0,0-1,.25,5.4,5.4,0,0,0-1.05.71L13.76,7.79A6.92,6.92,0,0,1,14.89,7a4,4,0,0,1,1-.37,4.82,4.82,0,0,1,1.19-.13,3.59,3.59,0,0,1,1.54.31,2.5,2.5,0,0,1,1,.89A2.33,2.33,0,0,1,20,9a3.16,3.16,0,0,1-.22,1.2,4.61,4.61,0,0,1-.7,1.15A16.65,16.65,0,0,1,17.42,13l-1.19,1.12v.09h4Z"/>
                    </svg>;
                    break;
                case 'h3':
                    icon = <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12.09,15.88h-2v-4H6.45v4h-2V6.6h2v3.64h3.68V6.6h2Z"/>
                        <path
                            d="M19.91,8.68a2.18,2.18,0,0,1-.53,1.47A2.68,2.68,0,0,1,17.9,11v0a3,3,0,0,1,1.71.68,1.93,1.93,0,0,1,.57,1.47,2.47,2.47,0,0,1-1,2.08,4.45,4.45,0,0,1-2.77.75,6.81,6.81,0,0,1-2.68-.5V13.84a5.76,5.76,0,0,0,1.19.44,5.12,5.12,0,0,0,1.28.17,2.47,2.47,0,0,0,1.43-.33,1.21,1.21,0,0,0,.47-1.06,1,1,0,0,0-.54-.93,4,4,0,0,0-1.7-.27h-.7v-1.5h.71a3.45,3.45,0,0,0,1.58-.28,1,1,0,0,0,.5-1c0-.71-.44-1.06-1.32-1.06a2.8,2.8,0,0,0-.93.16,4.24,4.24,0,0,0-1.05.52l-.91-1.35a5.06,5.06,0,0,1,3-.91,3.93,3.93,0,0,1,2.28.58A1.89,1.89,0,0,1,19.91,8.68Z"/>
                    </svg>;
                    break;
            }
            return icon;
        }

        useEffect(() => {
            props.setAttributes({
                uuid: id
            });
        }, []);

        return (
            <div className={props.className}>
                <BlockControls>
                    <Toolbar
                        controls={
                            ['h2', 'h3'].map((tag) => ({
                                icon    : getHTMLTagIcon(tag),
                                title   : tag.toUpperCase(),
                                isActive: titleTag === tag,
                                onClick : () => props.setAttributes({'titleTag': tag}),
                            }))
                        }
                    />
                </BlockControls>
                <RichText
                    tagName={titleTag}
                    className="accordeon_title"
                    allowedFormats={[]}
                    withoutInteractiveFormatting
                    value={title}
                    aria-expanded="true"
                    aria-controls={"collapse_" + uuid}
                    data-toggle="collapse"
                    data-target={"#collapse_" + uuid}
                    onChange={(val) => props.setAttributes({title: val})}
                />
                <div id={"collapse_" + uuid} className="collapse show" aria-labelledby={"heading_" + uuid}>
                    <InnerBlocks/>
                </div>
            </div>
        )
    },

    save: function (props) {
        const {title, titleTag, uuid} = props.attributes;
        return (
            <div className={props.className}>
                <RichText.Content tagName={titleTag}
                                  className="accordeon_title"
                                  value={title}
                                  aria-expanded="false"
                                  aria-controls={"collapse_" + uuid}
                                  data-toggle="collapse"
                                  data-target={"#collapse_" + uuid}/>
                <div id={"collapse_" + uuid} className="collapse" aria-labelledby={"heading_" + uuid}>
                    <InnerBlocks.Content/>
                </div>
            </div>
        )
    }
});