<?php
/**
 * Définition custom post type
 *
 * @package Expo
 */

$labels = array(
    'name'               => _x('Definitions', 'expo'),
    'singular_name'      => _x('Definition', 'expo'),
    'menu_name'          => _x('Glossaire', 'expo'),
    'name_admin_bar'     => _x('Glossaire', 'expo'),
    'add_new'            => _x('Ajouter une définition', 'expo'),
    'add_new_item'       => __('Ajouter une nouvelle définition', 'expo'),
    'new_item'           => __('Nouvelle définition', 'expo'),
    'edit_item'          => __('Modifier la définition', 'expo'),
    'view_item'          => __('Voir la définition', 'expo'),
    'all_items'          => __('Toutes les définitions', 'expo'),
    'search_items'       => __('Chercher', 'expo'),
    'parent_item_colon'  => __('Chapitre parent:', 'expo'),
    'not_found'          => __('Aucune définition trouvée.', 'expo'),
    'not_found_in_trash' => __('Aucune définition dans la corbeille.', 'expo'),
);

$args = array(
    'labels'              => $labels,
    'public'              => true,
    'show_ui'             => true,
    'hierarchical'        => false,
    'exclude_from_search' => true,
    'has_archive'         => false,
    'query_var'           => true,
    'can_export'          => true,
    'show_in_rest'        => true,
    'rewrite'             => true,
    'capability_type'     => 'post',
    'label'               => 'expo Définitions',
    'menu_icon'           => 'dashicons-editor-textcolor',
    'supports'            => array(
        'title',
        'editor'
    )
);

register_post_type('definition', $args);