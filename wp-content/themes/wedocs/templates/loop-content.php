<li class="doc">
    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

    <div class="inside">
        <?php the_excerpt(); ?>
    </div>
</li>