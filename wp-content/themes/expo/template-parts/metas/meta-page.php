<?php
/**
 * Template part for displaying meta in post/agenda page
 *
 * @package Expo
 */

?>
    <div class="meta_cont transparent">
        <?php
        $parentID = wp_get_post_parent_id(get_the_ID());
        if ($parentID) {
            ?>
            <ul>
                <?php
                $siblings = wp_list_pages(array(
                    'child_of' => $parentID,
                    'parent'   => $parentID,
                    'title_li' => ''
                ));
                ?>
            </ul>
            <?php
        }
        ?>
    </div>
<?php
