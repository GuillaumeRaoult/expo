/**
 * Registers a new block provided a unique name and an object defining its behavior.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/#registering-a-block
 */
import {registerBlockType} from '@wordpress/blocks';

/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import {__} from '@wordpress/i18n';

import {
    Button,
    __experimentalNumberControl as NumberControl,
    TextControl,
    TextareaControl,
    CheckboxControl,
    ResponsiveWrapper,
    PanelBody,
    PanelRow
} from '@wordpress/components'

import {useState} from '@wordpress/element';

import {MediaUpload, MediaUploadCheck, RichText, InspectorControls} from '@wordpress/block-editor';

const {withSelect} = wp.data;

const axios = require('axios').default;

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * All files containing `style` keyword are bundled together. The code used
 * gets applied both to the front of your site and to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './style.scss';

/**t
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

export const edit = ({attributes, className, setAttributes, isSelected}) => {

    const [isCopy, setCopy]           = useState(true);
    const [isError, setError]         = useState(false);
    const [isValidated, setValidated] = useState(false);

    const ImageComponentDisplay = withSelect((select) => {
        return {media: attributes.mediaId ? select('core').getMedia(attributes.mediaId) : undefined};
    })(ImageComponent);

    function ImageComponent({media}) {
        const onSelectMedia = (media) => {
            let url = media.url;
            if (media.sizes.medium.url) {
                url = media.sizes.medium.url;
            } else if (media.sizes.large.url) {
                url = media.sizes.large.url;
            }
            setAttributes({
                mediaId : media.id,
                mediaUrl: url
            });
        };

        return (
            <div className="placeholder_image">
                <MediaUploadCheck>
                    <MediaUpload
                        allowedTypes={['image']}
                        onSelect={onSelectMedia}
                        value={attributes.mediaId}
                        render={({open}) => (
                            <Button
                                className={attributes.mediaId === 0 ? 'editor-post-featured-image__toggle' : 'editor-post-featured-image__preview'}
                                onClick={open}
                            >
                                {attributes.mediaId === 0 && __('Choisir une image', 'awp')}
                                {media != undefined &&
                                <img src={media.source_url}/>
                                }
                            </Button>
                        )}
                    />
                </MediaUploadCheck>
            </div>
        )
    }

    const validateURL = () => {
        axios.get(attributes.manifest)
             .then(function (response) {
                 if (response.data["@type"] == !"sc:Manifest") {
                     setError(true);
                     setValidated(false);
                     return;
                 }

                 if (isCopy) {
                     let title = response.data.description;
                     if (!title) {
                         title = response.data.label;
                     }

                     setError(false);
                     setValidated(true);

                     setAttributes({
                         title: title
                     });
                 }
             })
             .catch(function (error) {
                 setError(true);
                 setValidated(false);
             });
    };

    const isBnfGallicaDocUrl = (val) => {
        if (!val) {
            return false;
        }
        if (val.indexOf('ark:') != -1) {
            return true;
        }
        return false;
    };

    const setDocUrl = (val) => {
        val = val.replace(/(\?rk=.+)/g, '');
        setAttributes({docUrl: val});
        if (isBnfGallicaDocUrl(val)) {
            let manifest = val.replace(/ark:/g, 'iiif/ark:');

            let lastChar = val.substr(val.length - 1);
            if (lastChar !== '/') {
                manifest += '/';
            }
            manifest += 'manifest.json';
            setAttributes({manifest: manifest})
        }
    };

    return (
        <div className={className}>
            {isSelected &&
            <div className="controls">
                <InspectorControls>
                    <PanelBody title={__("Paramètres IIIF")}>
                        <PanelRow>
                            <NumberControl
                                label="Page de démarrage"
                                value={attributes.start}
                                onChange={(val) => setAttributes({start: val})}
                            />
                        </PanelRow>
                    </PanelBody>
                </InspectorControls>
                <div className="manifest_url">
                    <div className="inputs">
                        <TextControl
                            label="URL du document :"
                            placeholder="Exemple : https://gallica.bnf.fr/ark:/12148/btv1b8423833p/"
                            value={attributes.docUrl}
                            onChange={setDocUrl}
                        />
                        <TextControl
                            label="URL du manifest :"
                            placeholder="Exemple : https://gallica.bnf.fr/iiif/ark:/12148/btv1b8423833p/manifest.json"
                            value={attributes.manifest}
                            onChange={(val) => setAttributes({manifest: val})}
                        />
                    </div>
                    <div className="checks">
                        <CheckboxControl
                            label="Copier le titre"
                            checked={isCopy}
                            onChange={setCopy}
                        />
                        <Button
                            isSmall
                            isPrimary
                            onClick={validateURL}
                            type="submit">
                            {__('Valider')}
                        </Button>
                    </div>
                </div>
                {isError &&
                <p className="alert alert-error">{__('L\'adresse ou le fichier manifest est incorrect')}</p>}

                {!isError && isValidated &&
                <p className="alert alert-success">{__('Le manifest semble correct, pensez à sauvegarder')}</p>}
            </div>
            }

            <div className="display">
                <div className="col-4">
                    <ImageComponentDisplay/>
                </div>
                <div className="col-6">
                    <div className="description">
                        <p className="pres"><span class="dashicons dashicons-text-page"></span>Documents interactifs
                        </p>
                        {attributes.title && !isSelected ? (
                            <div className="title">{attributes.title}</div>
                        ) : (
                            <RichText
                                tagName="div"
                                className="title"
                                label=""
                                placeholder="Titre du document"
                                allowedFormats={[]}
                                withoutInteractiveFormatting
                                value={attributes.title}
                                onChange={(val) => setAttributes({title: val})}
                            />
                        )}
                    </div>
                </div>
                <div className="col-2">
                    <div className="icon border_colored">
                        <span class="dashicons dashicons-plus-alt2 colored"></span>
                    </div>
                </div>
                <div className="clear"/>
            </div>
        </div>
    );
};

/**
 * Every block starts by registering a new block type definition.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/#registering-a-block
 */
registerBlockType('expo/iiif', {
    /**
     * This is the display title for your block, which can be translated with `i18n` functions.
     * The block inserter will show this name.
     */
    title: __('Expo Liseuse IIIF', 'create-block'),

    /**
     * This is a short description for your block, can be translated with `i18n` functions.
     * It will be shown in the Block Tab in the Settings Sidebar.
     */
    description: __(
        'Liseuse IIIF depuis un document manifest',
        'create-block'
    ),

    /**
     * Blocks are grouped into categories to help users browse and discover them.
     * The categories provided by core are `common`, `embed`, `formatting`, `layout` and `widgets`.
     */
    category: 'expo',

    /**
     * An icon property should be specified to make it easier to identify a block.
     * These can be any of WordPress’ Dashicons, or a custom svg element.
     */
    icon: <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
               width="24.000000pt" height="24.000000pt" viewBox="0 0 24.000000 24.000000"
               preserveAspectRatio="xMidYMid meet">

        <g transform="translate(0.000000,24.000000) scale(0.100000,-0.100000)"
           fill="#000000" stroke="none">
            <path d="M182 188 c-7 -7 -12 -40 -12 -80 0 -54 3 -68 15 -68 10 0 15 10 15
30 0 17 6 39 13 50 10 17 9 22 -3 30 -12 7 -12 12 -2 22 6 6 12 16 12 20 0 12
-24 10 -38 -4z"/>
            <path d="M32 173 c5 -25 28 -28 28 -4 0 12 -6 21 -16 21 -9 0 -14 -7 -12 -17z"/>
            <path d="M73 175 c-3 -9 -3 -18 0 -22 10 -10 37 6 37 22 0 20 -29 20 -37 0z"/>
            <path d="M122 174 c2 -11 11 -20 22 -22 13 -3 17 1 14 14 -2 11 -11 20 -22 22
-13 3 -17 -1 -14 -14z"/>
            <path d="M30 90 c0 -38 4 -50 15 -50 11 0 15 12 15 50 0 38 -4 50 -15 50 -11
0 -15 -12 -15 -50z"/>
            <path d="M77 133 c-4 -3 -7 -26 -7 -50 0 -37 3 -44 18 -41 22 4 32 98 9 98 -7
0 -17 -3 -20 -7z"/>
            <path d="M122 93 c2 -37 7 -49 21 -51 16 -3 18 3 15 45 -2 37 -7 49 -21 51
-16 3 -18 -3 -15 -45z"/>
        </g>
    </svg>,

    /**
     * Optional block extended support features.
     */
    supports: {
        html: false,
    },

    attributes: {
        title   : {
            type    : 'string',
            source  : 'text',
            selector: '.title',
        },
        docUrl  : {
            type: 'string'
        },
        manifest: {
            type: 'string'
        },
        start   : {
            type   : 'string',
            default: '1'
        },
        mediaId : {
            type   : 'number',
            default: 0
        },
        mediaUrl: {
            type   : 'string',
            default: ''
        }
    },

    example: {
        attributes: {
            title   : 'Titre du document IIIF',
            docUrl  : 'https://gallica.bnf.fr/ark:/12148/btv1b8423833p/',
            manifest: 'https://gallica.bnf.fr/iiif/ark:/12148/btv1b8423833p/manifest.json',
            mediaUrl: 'http://via.placeholder.com/300x200'
        },
    },

    edit: edit,
    save: ({attributes, className}) => {
        return (
            <div className={className + " col-12"}
                 data-iiif-viewer-manifest={attributes.manifest}
                 data-iiif-viewer-docUrl={attributes.docUrl}
                 data-iiif-viewer-start={attributes.start}
            >
                <div className="row align-items-center">
                    {attributes.mediaUrl &&
                    <div className="col-sm-4">
                        <div className="placeholder_image" style={'background-image:url(' + attributes.mediaUrl + ')'}/>
                    </div>
                    }
                    <div className="col-10 col-sm-6">
                        <div className="description">
                            <p className="pres colored"><span class="dashicons dashicons-text-page"></span>Documents
                                interactifs</p>
                            <div className="title">{attributes.title}</div>
                        </div>
                    </div>
                    <div className="col-2">
                        <div className="icon border_colored"><span class="dashicons dashicons-plus-alt2 colored"></span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});
