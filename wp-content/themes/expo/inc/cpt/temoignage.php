<?php
/**
 * Chapitre custom post type
 *
 * @package Expo
 */

$labels = array(
    'name'               => _x('Témoignages', 'expo'),
    'singular_name'      => _x('Témoignage', 'expo'),
    'menu_name'          => _x('Témoignages', 'expo'),
    'name_admin_bar'     => _x('Chapitre', 'expo'),
    'add_new'            => _x('Ajouter un témoignage', 'expo'),
    'add_new_item'       => __('Ajouter un nouveau témoignage', 'expo'),
    'new_item'           => __('Nouveau témoignage', 'expo'),
    'edit_item'          => __('Modifier le témoignage', 'expo'),
    'view_item'          => __('Voir le témoignage', 'expo'),
    'all_items'          => __('Tous les témoignages', 'expo'),
    'search_items'       => __('Chercher', 'expo'),
    'parent_item_colon'  => __('Témoignage parent:', 'expo'),
    'not_found'          => __('Aucun témoignage trouvé.', 'expo'),
    'not_found_in_trash' => __('Aucun témoignage dans la corbeille.', 'expo'),
);

$args = array(
    'labels'              => $labels,
    'public'              => true,
    'show_ui'             => true,
    'hierarchical'        => false,
    'exclude_from_search' => false,
    'has_archive'         => false,
    'query_var'           => true,
    'can_export'          => true,
    'show_in_rest'        => true,
    'rewrite'             => true,
    'capability_type'     => 'post',
    'label'               => 'expo Témoignages',
    'menu_icon'           => 'dashicons-format-quote',
    'supports'            => array(
        'author',
        'title',
        'thumbnail',
        'editor',
        'excerpt'
    )
);

register_post_type('temoignage', $args);