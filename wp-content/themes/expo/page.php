<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Expo
 */

set_query_var('header-color', 'transparent sticky-white open-white sticky-open-white');
get_header();

$subtitle = '';
$parentID = wp_get_post_parent_id(get_the_ID());
if ($parentID) {
    $parent = get_post($parentID);
    if ($parent) {
        $subtitle = get_the_title($parent);
    }
}

if (get_post_meta(get_the_ID(), 'page_subtitle', true)) {
    $subtitle = get_post_meta(get_the_ID(), 'page_subtitle', true);
}

set_query_var('subtitle', $subtitle);

get_template_part('template-parts/title', '');
get_template_part('template-parts/breadcrumbs', '');
?>
    <div id="main_content">
        <div class="container-fluid">
            <div class="row">
                <?php
                $grandFormat = get_post_meta(get_the_ID(), 'page_format', true);
                $class       = 'col-sm-10 offset-sm-1 col-md-8 offset-md-2';
                $parentID = wp_get_post_parent_id(get_the_ID());
                if ($parentID && (!$grandFormat || $grandFormat == 'no')) {
                    $class = 'col-sm-8 col-md-6';
                    ?>
                    <div class="col-sm-4 col-md-3 col-xl-2 offset-xl-1">
                        <?php
                        get_template_part('template-parts/metas/meta', get_post_type());
                        ?>
                    </div>
                    <?php
                }
                ?>
                <div class="<?php echo $class ?>">
                    <?php
                    the_content();
                    if (wp_get_post_parent_id(get_the_ID()) == 0) {
                        $pages = get_pages([
                            'child_of' => get_the_ID(),
                            'parent' => get_the_ID()
                        ]);
                        if ($pages) {
                            ?>
                            <div class="row oeuvres_cont">
                                <?php
                                foreach ($pages as $page) {
                                    ?>
                                    <div class="col-12">
                                        <a href="<?php echo get_the_permalink($page) ?>" class="oeuvre">
                                            <?php
                                            $thumbnail = get_the_post_thumbnail($page, 'large', [
                                                'class' => 'card-img-top'
                                            ]);
                                            if (!$thumbnail) {
                                                $homeBackgroundImageUrl = get_theme_mod('expo_home_background_image');
                                                $homeBackgroundImageUrl = attachment_url_to_postid($homeBackgroundImageUrl);
                                                $thumbnail              = wp_get_attachment_image($homeBackgroundImageUrl, 'large');
                                            }
                                            echo $thumbnail;
                                            ?>
                                            <h2>
                                                <?php echo get_the_title($page) ?>
                                                <span class="link colored">- Voir</span>
                                            </h2>
                                        </a>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
        if (!get_query_var('borne', '')) {
            ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-6 offset-3">
                        <div class="single_footer">
                            <p class="author">Partager</p>
                            <p class="share">
                                <?php
                                shareButtons();
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php
get_sidebar();
get_footer();
