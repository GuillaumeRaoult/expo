(function ($) {
    $(document).ready(function () {
        if ($('.wp-block-expo-iiif').length || $('.open_manifest').length) {
            let requests    = [];
            let url         = '';
            let currentOpen = null;

            //Get the manifests
            $('.wp-block-expo-iiif, .open_manifest').each(function () {
                const manifest = $(this).data('iiif-viewer-manifest');
                if (manifest) {
                    requests.push($.getJSON(manifest));
                }
            });

            $.when.apply(null, requests).then(function () {
                let images     = [];
                let infos      = [];
                let _arguments = Array.prototype.slice.call(arguments);
                if (Array.isArray(requests) && requests.length === 1)
                    _arguments = [arguments];

                $.each(_arguments, function (i, row) {
                    const status = row[1], data = row[0];
                    if (status === 'success') {
                        let temps = [];

                        let metas = [];
                        if (data.metadata) {
                            metas = data.metadata;
                        }
                        infos[i] = metas;

                        $.each(data.sequences[0].canvases, function (index, canvas) {
                            let full = canvas.images[0].resource["@id"];
                            full     = full.replace(/full\/full/g, 'full/1000');

                            const thumb = canvas.thumbnail["@id"];
                            temps.push({
                                src  : full,
                                thumb: thumb
                            });
                        });

                        images.push(temps);
                    }
                });

                let wasDefined = false;
                if (window.lightbox === undefined) {
                    window.lightbox = $.ModuloBox({
                        scrollToZoom  : true,
                        controls      : ['zoom', 'fullScreen', 'close'],
                        timeToIdle    : 0,
                        tapToClose    : false,
                        counterMessage: '',
                        loadError     : 'Désolé, une erreur s\'est produite...',
                        noContent     : 'Désolé, aucun contenu n\'a été trouvé'
                    });
                } else {
                    wasDefined = true;
                }

                window.lightbox.on('updateGalleries.modulobox', function (galleries) {
                    $.each(images, function (index, image) {
                        if (galleries['iiif_' + index] === undefined) {
                            window.lightbox.addMedia('iiif_' + index, image);
                        }
                    });

                    /**
                     * V2, we can have size like 500,/full ?????
                     * If we do it before, the comma breaks the lightbox, but not with thumb ????
                     */
                    $.each(galleries, function (index, gallery) {
                        $.each(gallery, function (j, image) {
                            if (image.thumb.indexOf(',') != -1) {
                                var newFull             = image.src.replace(/1000/g, ',1000');
                                galleries[index][j].src = newFull;
                            }
                        });
                    });

                    if (images.length > 0) {
                        $('.wp-block-expo-iiif, .open_manifest').addClass('loaded');
                    } else {
                        $('.wp-block-expo-iiif, .open_manifest').addClass('error');
                    }
                });

                window.lightbox.on( 'afterClose.modulobox', function( gallery, index ) {
                    $('.mobx-holder .mobx-top-bar .open_infobox').remove();
                    $('.mobx-holder .mobx-top-bar .infobox').remove();
                });

                window.lightbox.on('beforeOpen.modulobox', function (gallery, index) {
                    if (gallery.match(/iiif/g)) {
                        $('.mobx-holder .mobx-top-bar .open_infobox').remove();
                        $('.mobx-holder .mobx-top-bar .infobox').remove();

                        $('.mobx-holder .mobx-prev').html('<span>Précédent</span>').addClass('border_colored colored');
                        $('.mobx-holder .mobx-next').html('<span>Suivant</span>').addClass('border_colored colored');

                        if (infos[currentOpen].length || url) {
                            let infobox = '<div class="infobox"><button class="close_infobox"><span class="dashicons dashicons-no-alt"></span></button><ul>';

                            if (infos[currentOpen].length) {
                                const valids = [
                                    {'key': 'Title', 'label': 'Titre'},
                                    {'key': 'Creator', 'label': 'Auteur'},
                                    {'key': 'Date', 'label': 'Date'},
                                    {'key': 'Shelfmark', 'label': 'Provenance'}
                                ];

                                $.each(valids, function (key, valid) {
                                    $.each(infos[currentOpen], function (key, value) {
                                        if (valid.key === value.label) {
                                            if (valid.key === 'Creator') {
                                                if (typeof value.value === 'object') {
                                                    let authors = '';
                                                    $.each(value.value, function (index, author) {
                                                        authors += author["@value"];
                                                        if (index + 1 !== value.value.length) {
                                                            authors += '<br/>';
                                                        }
                                                    });
                                                    value.value = authors;
                                                }
                                            }

                                            if (valid.key === 'Source Images') {
                                                value.value = '<a href="' + value.value + '" target="_blank">' + value.value + '</a>';
                                            }

                                            infobox += '' +
                                                '<li>' +
                                                '<span>' +
                                                valid.label +
                                                '</span>' +
                                                '<span>' +
                                                value.value +
                                                '</span>' +
                                                '</li>';
                                        }
                                    });
                                });
                            }

                            if (url) {
                                infobox += '<li><span>En savoir +</span><span><a target="_blank" href="' + url + '">' + url + '</a></span></li>'
                            }

                            infobox += '</ul></div>';

                            $('.mobx-holder .mobx-top-bar').prepend('' +
                                infobox +
                                '<button class="open_infobox bg_colored">' +
                                '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20pt" height="20pt" viewBox="0 0 20 20" version="1.1">\n' +
                                '<g id="surface1">\n' +
                                '<path style=" stroke:none;fill-rule:nonzero;fill:rgb(0%,0%,0%);fill-opacity:1;" d="M 12.441406 0 C 13.082031 0 13.574219 0.179688 13.921875 0.539062 C 14.269531 0.898438 14.441406 1.359375 14.441406 1.917969 C 14.441406 2.585938 14.179688 3.171875 13.660156 3.679688 C 13.140625 4.1875 12.507812 4.441406 11.761719 4.441406 C 11.121094 4.441406 10.628906 4.269531 10.28125 3.921875 C 9.933594 3.574219 9.773438 3.09375 9.800781 2.480469 C 9.800781 1.867188 10.035156 1.300781 10.5 0.78125 C 10.964844 0.261719 11.613281 0 12.441406 0 M 8.320312 20 C 6.988281 20 6.628906 18.8125 7.242188 16.441406 L 8.441406 11.363281 C 8.628906 10.617188 8.628906 10.242188 8.441406 10.242188 C 8.28125 10.242188 7.921875 10.363281 7.363281 10.601562 C 6.804688 10.839844 6.324219 11.09375 5.921875 11.363281 L 5.402344 10.484375 C 6.601562 9.445312 7.863281 8.605469 9.183594 7.964844 C 10.503906 7.324219 11.511719 7.003906 12.203125 7.003906 C 13.242188 7.003906 13.484375 8.082031 12.921875 10.242188 L 11.523438 15.5625 C 11.308594 16.414062 11.351562 16.84375 11.644531 16.84375 C 12.230469 16.84375 13.019531 16.445312 14.003906 15.644531 L 14.605469 16.445312 C 13.484375 17.59375 12.320312 18.472656 11.105469 19.085938 C 9.890625 19.699219 8.960938 20 8.320312 20 "/>\n' +
                                '</g>\n' +
                                '</svg>\n' +
                                '</button>');

                            $('.open_infobox').on('click tap touchstart', function () {
                                $(this).toggleClass('opened');
                                if ($(this).hasClass('opened')) {
                                    $('.mobx-holder .mobx-top-bar .infobox').addClass('opened');
                                } else {
                                    $('.close_infobox').click();
                                }
                            });

                            $('.close_infobox').on('click tap touchstart', function () {
                                $('.open_infobox').removeClass('opened');
                                $('.mobx-holder .mobx-top-bar .infobox').removeClass('opened');
                            });
                        }
                    }
                });

                if (wasDefined) {
                    window.lightbox.updateGalleries();
                } else {
                    window.lightbox.init();
                }

                $('.wp-block-expo-iiif, .open_manifest').each(function (index) {
                    $(this).click(function () {
                        url       = $(this).data('iiif-viewer-docurl');
                        var start = $(this).data('iiif-viewer-start');
                        if (!start) {
                            start = 1;
                        }
                        currentOpen = index;
                        window.lightbox.open('iiif_' + index, start - 1);
                    });
                });
            });
        }
    });
})(jQuery);