(function ($) {
    $(document).ready(function () {
        var swiper = (function () {
            var swiperSliders = document.getElementsByClassName(
                "js-mcr-swiper-container"
            );
            for (i = 0; i < swiperSliders.length; i++) {
                var prevEl = swiperSliders[i].parentElement.querySelector('.js-mcr-swiper-button-prev');
                var nextEl = swiperSliders[i].parentElement.querySelector('.js-mcr-swiper-button-next');

                var swiperOptions = {
                    navigation    : {
                        nextEl: nextEl,
                        prevEl: prevEl
                    },
                    slidesPerView : 'auto',
                    centeredSlides: true,
                    autoHeight    : false,
                    spaceBetween  : 20,
                    loop          : true,
                };

                new Swiper(swiperSliders[i], swiperOptions);
            }
        })();
    });

})(jQuery);
