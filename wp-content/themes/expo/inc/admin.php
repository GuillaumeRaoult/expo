<?php

/**
 * ADMIN
 */

function changeAdminLoginUrl($url)
{
    return get_home_url();
}

add_filter('login_headerurl', 'changeAdminLoginUrl', 10, 1);

function changeAdminLoginTitle($title)
{
    return 'Le site des expositions';
}

add_filter('login_headertext', 'changeAdminLoginTitle', 10, 1);

function adminLoginStyle()
{
    ?>
    <style>
        body {
            background: url(<?php echo get_theme_file_uri('/assets/images/Toulouse_Bibliotheque_Municipale.jpg') ?>) no-repeat center;
            background-size: cover;
        }

        .login h1 a {
            background-image: url(<?php echo get_theme_file_uri('/assets/images/Logo-Rosalis-Menu.png') ?>);
            background-size: 80%;
            width: 100%;
        }
    </style>
    <?php
}

add_action('login_header', 'adminLoginStyle');


function adminRemoveMenus()
{
    remove_menu_page('edit-comments.php');
}

add_action('admin_menu', 'adminRemoveMenus');


function adminRemoveDashboardWidgets()
{
    remove_meta_box('dashboard_primary', 'dashboard', 'side');
    remove_meta_box('dashboard_secondary', 'dashboard', 'side');
}

add_action('wp_dashboard_setup', 'adminRemoveDashboardWidgets');

function adminRemoveLogo()
{
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('comments');
}

add_action('wp_before_admin_bar_render', 'adminRemoveLogo', 0);


function removeAllowedBlocks($allowed_blocks, $block_editor_context)
{
    $frontId = (int)get_option('page_on_front');
    if ($block_editor_context->post->ID == $frontId) {
        return [
            'expo/home'
        ];
    }

    return array(
        'core/image',
        'core/paragraph',
        'core/heading',
        'core/list',
        'core/quote',
        'core/table',
        'core/file',
        'core/gallery',
        'core/audio',
        'core/video',
        'core/pullquote',
        'core/buttons',
        'core/text-columns',
        'core/media-text',
        'core/separator',
        'core/spacer',
        'core/columns',
        'core/column',
        'core/html',
        'core/embed',
        'expo/accordion',
        'expo/carousel',
        'expo/extract',
        'expo/iiif',
        'expo/carrousel',
        'expo/home'
    );

}

add_filter('allowed_block_types_all', 'removeAllowedBlocks', 10, 2);


/**
 * Edit menus
 */
add_action('wp_nav_menu_item_custom_fields', 'addCustomMenuOption', 10, 5);
function addCustomMenuOption($item_id, $item, $depth, $args, $id)
{
    $linkType                = get_post_meta($item_id, '_menu_item_menu_item_linktype', true);
    $selectedNormal          = $linkType == 'normal' ? 'selected' : '';
    $selectedOvertitleLink   = $linkType == 'overtitle-link' ? 'selected' : '';
    $selectedOvertitleNoLink = $linkType == 'overtitle-nolink' ? 'selected' : '';
    ?>
    <p class="field-linktype linktype linktype-wide">
        <label for="edit-menu-item-linktype-<?php echo $item_id; ?>">
            <?php _e('Type de lien expo'); ?><br/>
            <select id="edit-menu-item-linktype-<?php echo $item_id; ?>" class="widefat edit-menu-item-type"
                    name="menu-item-linktype[<?php echo $item_id; ?>]">
                <option value="normal" <?php echo $selectedNormal ?>>Normal</option>
                <option value="overtitle-link" <?php echo $selectedOvertitleLink ?>>Surtitre avec lien</option>
                <option value="overtitle-nolink" <?php echo $selectedOvertitleNoLink ?>>Surtitre sans lien</option>
            </select>
            <span class="description"><?php _e('Le type de lien. Normal affiche un lien dans la couleur de l\'expo. Surtitre un titre au dessus avec ou sans lien.'); ?></span>
        </label>
    </p>
    <style type="text/css">
        .menu-item-settings .linktype-wide {
            margin-right: 10px;
            float: left;
            margin: 2px 0 5px;
            color: #646970;
        }
    </style>
    <?php
}

add_action('wp_update_nav_menu_item', 'saveCustomMenuOption', 10, 3);
function saveCustomMenuOption($menu_id, $menu_item_db_id, $args)
{
    if (isset($_POST['menu-item-linktype'])) {
        foreach ((array)$_POST['menu-item-linktype'] as $menuItemId => $menu_item_linktype) {
            update_post_meta($menuItemId, '_menu_item_menu_item_linktype', $menu_item_linktype);
        }
    }
}

add_filter('nav_menu_link_attributes', 'filterMenuItemClasses', 10, 4);
function filterMenuItemClasses($atts, $item, $args, $depth)
{
    $linkType = get_post_meta($item->ID, '_menu_item_menu_item_linktype', true);
    if ($linkType == 'overtitle-link') {
        $atts['class'] .= ' overtitle-link';
    }
    return $atts;
}

