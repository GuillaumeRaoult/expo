/* global wp, jQuery */
/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

(function ($) {
    // Site title and description.
    wp.customize('blogname', function (value) {
        value.bind(function (to) {
            $('.site-title a').text(to);
        });
    });
    wp.customize('blogdescription', function (value) {
        value.bind(function (to) {
            $('.site-description').text(to);
        });
    });

    // Header text color.
    wp.customize('header_textcolor', function (value) {
        value.bind(function (to) {
            document.documentElement.style.setProperty('--main-expo-color', to);

            $('a, .colored').css({
                color: to,
            });
            $('a[data-type="definition"]').css({
                background: to,
            });
            $('.bg_colored').css({
                background: to,
            });
            $('.border_colored').css({
                'border-color': to,
            });
            $('.hover_colored:hover').css({
                color: to,
            });
            $('.hover_bg_colored:hover').css({
                background: to,
            });
        });
    });

    //Home background image
    wp.customize('expo_home_background_image', function (value) {
        value.bind(function (to) {
            //document.styleSheets[0].addRule('body.home .overlay:before','background-image: url('+to+')');
            $('<style>body.home .overlay:before{background-image:url('+to+')}</style>').appendTo('head');
        });
    });
}(jQuery));
