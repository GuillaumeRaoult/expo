<?php
/**
 * Plugin Name:     Expo - Home
 * Description:     Un block affichant les post-types sur la home
 * Version:         1.0.0
 * Author:          Guillaume Raoult
 * License:         GPL-2.0-or-later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     create-block
 *
 * @package         create-block
 */

/**
 * Registers all block assets so that they can be enqueued through the block editor
 * in the corresponding context.
 *
 * @see https://developer.wordpress.org/block-editor/tutorials/block-tutorial/applying-styles-with-stylesheets/
 */
function create_block_home_posts_block_block_init()
{
    $dir = dirname(__FILE__);

    $script_asset_path = "$dir/build/index.asset.php";
    if (!file_exists($script_asset_path)) {
        throw new Error('You need to run `npm start` or `npm run build` for the "create-block/home-posts-block" block first.');
    }
    $index_js     = 'build/index.js';
    $script_asset = require($script_asset_path);
    wp_register_script('create-block-home-posts-block-editor', plugins_url($index_js, __FILE__), $script_asset['dependencies'], $script_asset['version']);

    $editor_css = 'build/index.css';
    wp_register_style('create-block-home-posts-block-editor', plugins_url($editor_css, __FILE__), array(), filemtime("$dir/$editor_css"));

    $style_css = 'build/style-index.css';
    wp_register_style('create-block-home-posts-block', plugins_url($style_css, __FILE__), array(), filemtime("$dir/$style_css"));

    //Front
    wp_register_script('create-block-home-posts--block-front', plugins_url('build/front.js', __FILE__), array('jquery'), '', true);

    register_block_type('expo/home', array(
        'editor_script'   => 'create-block-home-posts-block-editor',
        'editor_style'    => 'create-block-home-posts-block-editor',
        'style'           => 'create-block-home-posts-block',
        'script'          => 'create-block-home-posts--block-front',
        'render_callback' => 'render_home_block_callback'
    ));
}

add_action('init', 'create_block_home_posts_block_block_init');

function render_home_block_callback($block_attributes, $content)
{
    ob_start();
    global $post;

    if (!isset($block_attributes['type'])) {
        $block_attributes['type'] = 'post_type';
    }

    if (!isset($block_attributes['theme'])) {
        $block_attributes['theme'] = 'clair';
    }

    $contentPosts = $contentButtons = $contentPhoto = [];
    if ($block_attributes['type'] == 'post_type') {
        if (!isset($block_attributes['post_type'])) {
            $block_attributes['post_type'] = 'chapitre';
        }
        $posts = get_posts(array(
            'post_type'      => $block_attributes['post_type'],
            'posts_per_page' => '-1',
        ));
        if ($posts) {
            $contentPosts = $posts;
        }
    }

    if ($block_attributes['type'] == 'button') {
        if (isset($block_attributes['buttons']) && $block_attributes['buttons']) {
            $contentButtons = $block_attributes['buttons'];
        }
    }

    if ($block_attributes['type'] == 'photo') {
        if (isset($block_attributes['photo']) && $block_attributes['photo']) {
            $contentPhoto = $block_attributes['photo'];
        }
    }

    if ($contentPosts || $contentButtons || $contentPhoto) {
        $class = '';
        if ($contentPosts) {
            $class = $block_attributes['post_type'] . 's';
        }
        if ($contentButtons) {
            $class = 'buttons';
        }
        if ($contentPhoto) {
            $class = 'photo';
        }
        ?>
        <section
                class="row section <?php echo $block_attributes['theme'] ?> <?php echo $class ?>">
            <div class="col-sm-8 offset-sm-2">
                <div class="row justify-content-center">
                    <div class="col-md-6 section_title">
                        <?php
                        if (isset($block_attributes['title'])) {
                            ?>
                            <h2 class="colored"><?php echo $block_attributes['title'] ?></h2>
                            <?php
                        }
                        if (isset($block_attributes['desc'])) {
                            ?>
                            <p><?php echo $block_attributes['desc'] ?></p>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
            if ($contentPosts) {
                switch ($block_attributes['post_type']) {
                    case 'chapitre':
                        ?>
                        <div class="col-sm-10 offset-sm-1">
                            <div class="row ">
                                <?php
                                $maxChapters = isset($block_attributes['max_elements']) ? $block_attributes['max_elements'] : 5;
                                foreach ($contentPosts as $k => $post) {
                                    setup_postdata($post);
                                    $i = $k + 1;
                                    if ($i < 10) {
                                        $i = '0' . $i;
                                    }

                                    if (count($contentPosts) > $maxChapters && $maxChapters == $k) {
                                        ?>
                                        <div class="expand_chapters_cont">
                                        <?php
                                    }
                                    ?>
                                    <div class="col-12 chapitre">
                                        <a href="<?php echo get_the_permalink() ?>">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-sm-6 chapitre_illus">
                                                    <?php the_post_thumbnail() ?>
                                                </div>
                                                <div class="col-sm-6 chapitre_title overlap">
                                                    <h3 class="row no-gutters align-items-center">
                                                        <span class="col-1 mr-1 number colored"><?php echo $i ?></span>
                                                        <span class="col"><?php echo the_title(); ?></span>
                                                    </h3>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <?php
                                    if (count($contentPosts) > $maxChapters && $k == count($contentPosts) - 1) {
                                        ?>
                                        </div>
                                        <div class="col-1 col-centered">
                                            <div class="custom_button more_chapters colored animated">
                                                <span class="dashicons dashicons-arrow-down-alt2 colored"></span>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                wp_reset_postdata();
                                ?>
                            </div>
                        </div>
                        <?php
                        break;
                    case 'oeuvre':
                        $clear = isset($block_attributes['hide_title']) && $block_attributes['hide_title'] ? ' clear' : '';
                        ?>
                        <div class="col-12">
                            <div class="row no-gutters">
                                <div class="col-6 col-md-1 prev">
                                    <div class="swiper-nav swiper-prev swiper-prev-oeuvre bg_colored">
                                        <span class="dashicons dashicons-arrow-left-alt"></span>
                                    </div>
                                </div>
                                <div class="col-12 col-md-10 swipecont">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">
                                            <?php
                                            foreach ($contentPosts as $post) {
                                                setup_postdata($post);
                                                ?>
                                                <div class="swiper-slide <?php echo $clear ?>">
                                                    <a href="<?php the_permalink() ?>">
                                                        <div class="oeuvre_illus">
                                                            <?php the_post_thumbnail() ?>

                                                        </div>
                                                        <?php
                                                        if (!$clear) {
                                                            ?>
                                                            <span><?php the_title() ?></span>
                                                            <?php
                                                        }
                                                        ?>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            wp_reset_postdata();
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 col-md-1 next">
                                    <div class="swiper-nav swiper-next swiper-next-oeuvre bg_colored">
                                        <span class="dashicons dashicons-arrow-right-alt"></span>
                                    </div>
                                </div>
                            </div>
                            <?php
                            if (isset($block_attributes['link']) && $block_attributes['link']) {
                                ?>
                                <div class="row">
                                    <div class="col-8 offset-2 col-sm-12 offset-sm-0 text-center">
                                        <div class="btn outline colored border_colored">
                                            <?php
                                            echo $block_attributes['link'];
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                        break;
                    case 'temoignage':
                        ?>
                        <div class="col-sm-10 offset-sm-1">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <?php
                                    foreach ($contentPosts as $post) {
                                        setup_postdata($post);
                                        ?>
                                        <div class="swiper-slide">
                                            <a href="<?php the_permalink() ?>">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-sm-6 temoignage_illus">
                                                        <?php
                                                        $tags = wp_get_post_terms($post->ID, 'temoignage_tag');
                                                        if ($tags) {
                                                            $tag = $tags[0];
                                                            ?>
                                                            <span class="bg_colored"><?php echo $tag->name ?></span>
                                                            <?php
                                                        }

                                                        $thumbnail = get_the_post_thumbnail();
                                                        if (get_post_meta(get_the_ID(), 'mobile_image_thumbnail', true)) {
                                                            $mobileUrl    = get_post_meta(get_the_ID(), 'mobile_image_thumbnail', true);
                                                            $attachmentId = attachment_url_to_postid($mobileUrl);
                                                            $thumbnail    = wp_get_attachment_image($attachmentId, 'large');
                                                        }
                                                        echo $thumbnail;
                                                        ?>
                                                    </div>
                                                    <div class="col-sm-6 temoignage_title overlap sombre">
                                                        <div class="row no-gutters align-items-center">
                                                            <h3>
                                                                <?php the_excerpt() ?>
                                                                <p class="title"><?php the_title() ?></p>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                    wp_reset_postdata();
                                    ?>
                                </div>
                                <div class="swiper-pagination-cont">
                                    <div class="swiper-nav swiper-prev swiper-prev-temoignage bg_colored float-left">
                                        <span class="dashicons dashicons-arrow-left-alt"></span>
                                    </div>
                                    <div class="swiper-nav swiper-next swiper-next-temoignage bg_colored float-left">
                                        <span class="dashicons dashicons-arrow-right-alt"></span>
                                    </div>
                                    <div class="swiper-pagination float-left"></div>
                                </div>
                            </div>
                        </div>
                        <?php
                        break;
                }
            }

            if ($contentButtons) {
                ?>
                <div class="col-md-8 offset-md-2">
                    <div class="row">
                        <?php
                        foreach ($contentButtons as $q => $button) {
                            $class = 'col-sm-4';
                            $class = count($contentButtons) == 1 ? $class . ' offset-sm-4' : $class;
                            $class = count($contentButtons) == 2 && $q == 0 ? $class . ' offset-sm-2' : $class;
                            if ($q == 3) {
                                $class = 'col-12 offset-md-0 col-sm-1 more_button';
                            }
                            ?>
                            <div class="<?php echo $class ?> mb-1 custom_button">
                                <div class="row no-gutters">
                                    <div class="col-12">
                                        <?php echo $button ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
            }

            if ($contentPhoto) {
                $href = '';
                if (isset($block_attributes['link'])) {
                    $href = $block_attributes['link'];
                }

                ?>
                <div class="col-md-10 offset-md-1">
                    <a href="<?php echo $href ?>" class="row no-gutters">
                        <div class="col-sm-4 offset-sm-1 photo_text_cont">
                            <h3 class="photo_text">
                                <?php
                                if (isset($block_attributes['photo_text'])) {
                                    echo $block_attributes['photo_text'];
                                }
                                ?>
                            </h3>
                        </div>
                        <div class="col-sm-6 photo_cont">
                            <?php
                            if (isset($contentPhoto['imgid'])) {
                                echo wp_get_attachment_image($contentPhoto['imgid'], 'large');
                            }
                            ?>
                        </div>
                    </a>
                </div>
                <?php
            }
            ?>
        </section>
        <?php
    }

    $page = ob_get_contents();
    ob_end_clean();

    return $page;
}

