<?php
/**
 * Expo Theme Customizer
 *
 * @package Expo
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function expo_customize_register($wp_customize)
{

    if (!isset($wp_customize)) {
        return;
    }

    /**
     * Add 'Home Background Image' Setting.
     */
    $wp_customize->add_setting('expo_home_background_image', array(
        'default'           => '',
        'sanitize_callback' => 'esc_url_raw',
        'transport'         => 'postMessage'
    ));

    /**
     * Add 'Home Background Image' image upload Control.
     */
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'expo_home_background_image', array(
        'settings'    => 'expo_home_background_image',
        'section'     => 'static_front_page',
        'label'       => __('Image de fond', 'expo'),
        'description' => __('Choisissez l\'image de fond', 'expo')
    )));

    $wp_customize->get_setting('blogname')->transport         = 'postMessage';
    $wp_customize->get_setting('blogdescription')->transport  = 'postMessage';
    $wp_customize->get_setting('header_textcolor')->transport = 'postMessage';

    if (isset($wp_customize->selective_refresh)) {
        $wp_customize->selective_refresh->add_partial('blogname', array(
            'selector'        => '.site-title a',
            'render_callback' => 'expo_customize_partial_blogname',
        ));
        $wp_customize->selective_refresh->add_partial('blogdescription', array(
            'selector'        => '.site-description',
            'render_callback' => 'expo_customize_partial_blogdescription',
        ));
    }
}

add_action('customize_register', 'expo_customize_register');

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function expo_customize_partial_blogname()
{
    bloginfo('name');
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function expo_customize_partial_blogdescription()
{
    bloginfo('description');
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function expo_customize_preview_js()
{
    wp_enqueue_script('expo-customizer', get_template_directory_uri() . '/assets/js/customizer.js', array('customize-preview'), '20151215', true);
}

add_action('customize_preview_init', 'expo_customize_preview_js');
