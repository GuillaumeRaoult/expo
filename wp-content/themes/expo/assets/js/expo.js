var expo;

(function ($) {

    expo = {
        init      : function () {
            var self = this;
            self.resize();
            self.animations();
            self.menus();
            self.home();
            self.footer();
            self.video();
            self.media();
            self.modal();
            self.agenda();
            self.masonry();
            self.gallery();

            if ($('body.single-chapitre').length) {
                self.chapitre();
            }
        },
        resize    : function () {
            function appHeight() {
                document.documentElement.style.setProperty('--app-height', $(window).innerHeight() + 'px');
            }

            window.addEventListener('resize', appHeight);
            appHeight();
        },
        animations: function () {
            $('.overlay_cont .overlay').addClass('animated');
        },
        menus     : function () {
            $('.nav-toggle').on('click', function (e) {
                e.preventDefault();
                if ($('body').hasClass('nav-active') && $('body').hasClass('chapitre-nav-active')) {
                    $('body').removeClass('nav-active chapitre-nav-active');
                }
                $('body').toggleClass('nav-active menu-nav-active');
            });
        },
        home      : function () {
            /**
             * Parallax
             */
            if ($('body').outerHeight() > $(window).height()) {
                function setScroll() {
                    var st = $(this).scrollTop();

                    var position = -(st / 10);
                    if (position > -200) {
                        $('.overlay').css('transform', 'translateY(' + position + 'px)');
                    }

                    var rosalisHeight = 0;
                    if ($('#rosalis_header').length) {
                        rosalisHeight = $('#rosalis_header').height();
                    }
                    if (position < -rosalisHeight) {
                        position = -rosalisHeight;
                    }
                    $('header').css('transform', 'translateY(' + position + 'px)');

                    //If we are at 70% of the overlay
                    if (st > $('.overlay_cont').height() * 0.7) {
                        $('header').addClass('sticky');
                    } else {
                        $('header').removeClass('sticky');
                    }
                }

                $(window).scroll(function () {
                    setScroll();
                });
                setScroll();
            }

            /**
             * Scroll bottom anchor
             */
            $('.anchor').click(function () {
                scroll = 0;
                if ($(this).hasClass('bottom')) {
                    var rosalisHeight = 0;
                    if ($('#rosalis_header').length) {
                        rosalisHeight = $('#rosalis_header').outerHeight();
                    }
                    var scroll = $("#main_content").offset().top - $('header').height() + rosalisHeight;
                }
                $([document.documentElement, document.body]).animate({
                    scrollTop: scroll
                }, 500);
            });
        },
        chapitre  : function () {
            /**
             * Open right title menu
             */
            $('.chapitre-nav-toggle').on('click', function (e) {
                e.preventDefault();
                if ($('body').hasClass('nav-active') && $('body').hasClass('menu-nav-active')) {
                    $('body').removeClass('nav-active menu-nav-active');
                }
                $('body').toggleClass('nav-active chapitre-nav-active');
            });

            /**
             * Sub nav chapitres sur h2 contenu
             */
            $('.nav_chapitre > ul > li a').click(function (e) {
                var index    = $(this).data('index');
                var position = getPosition(index);

                $([document.documentElement, document.body]).animate(
                    {
                        scrollTop: position
                    }, {
                        duration: 500,
                        step    : function (now, fx) {
                            position = getPosition(index);
                            if (fx.end !== position) {
                                fx.end = position;
                            }
                        }
                    }
                );

                $('.chapitre-nav-toggle').click();
            });

            /**
             * Get offset position
             *
             * @param index
             * @returns {number}
             */
            function getPosition(index) {
                var titleOffset = $('#main_content').offset().top;
                if (index > 0) {
                    titleOffset = $('#main_content').find('h2:eq(' + (index - 1) + ')').offset().top;
                }
                return titleOffset - $('header').height();
            }

            /**
             * Change current title
             *
             * @param index
             */
            function setCurrentTitle(index) {
                var titlePosition = 0;
                if (index > 0) {
                    if ($('body header #menu_header .chapitre_cont .chapitre-nav-toggle p.nav_text span:eq(' + index + ')')) {
                        titlePosition = $('body header #menu_header .chapitre_cont .chapitre-nav-toggle p.nav_text span:eq(' + index + ')').position().top;
                    }
                }
                $('body header #menu_header .chapitre_cont .chapitre-nav-toggle p.nav_text').css('top', -titlePosition);
            }

            if ($('body header #menu_header .chapitre_cont .chapitre-nav-toggle p.nav_text').is(':visible')) {
                if ('IntersectionObserver' in window) {
                    /**
                     * Intersection observer for titles div
                     *
                     * @type {NodeListOf<Element>}
                     */
                    var titleSections = document.querySelectorAll('#main_content .chapter_title_cont');
                    var currentTitle  = null;
                    var observer      = new IntersectionObserver(function (titles) {
                        var titleElement = null;
                        titles.forEach(function (title) {
                            if (title.isIntersecting || title.boundingClientRect.top < 0) {
                                titleElement = title.target;
                            } else {
                                //Passed below
                                title = title.target;
                                while (title = title.previousElementSibling) {
                                    if (title.querySelector('h2')) {
                                        titleElement = title;
                                        break;
                                    }
                                }
                            }
                        });

                        if (titleElement !== currentTitle) {
                            currentTitle = titleElement;
                            var index    = [].indexOf.call(titleSections, currentTitle);
                            setCurrentTitle(index + 1);
                        }
                    }, {
                        threshold : [1],
                        rootMargin: '0px 0px -' + ($(window).height() / 2 - $('header').height()) + 'px 0px'
                    });

                    titleSections.forEach(function (titleSection) {
                        observer.observe(titleSection);
                    });
                } else {
                    var chapitresPositions = [];
                    $('#main_content').find('h2').each(function (index) {
                        var chapitreTop = $(this).offset().top - $('header').height() - 30;
                        chapitresPositions.push(chapitreTop);
                    });

                    function setScrollMenu() {
                        var st                = $(this).scrollTop();
                        var currentTitleIndex = 0;
                        $.each(chapitresPositions, function (index, chapitreTop) {
                            if (st > chapitreTop) {
                                currentTitleIndex = index + 1;
                            }
                        });
                        setCurrentTitle(currentTitleIndex);
                    }

                    $(window).scroll(function () {
                        setScrollMenu();
                    });
                    setScrollMenu();
                }
            }
        },
        footer    : function () {
            $('.trigger_expand_partenaires').click(function () {
                $('.partenaires_list').toggleClass('expanded');
                $(this).toggleClass('open');
            });

            initTemoignageSlider();
            $(window).resize(function () {
                initTemoignageSlider();
            });

            var swiperPartenaires;

            function initTemoignageSlider() {
                if ($(window).width() > 768) {
                    swiperPartenaires = new Swiper('#partenaires-swiper-container', {
                        speed                   : 400,
                        slidesPerView           : 4,
                        spaceBetween            : 0,
                        centeredSlides          : false,
                        loop                    : false,
                        centerInsufficientSlides: true,
                        watchOverflow           : true,
                        centeredSlidesBounds    : true,
                        navigation              : {
                            nextEl: '.swiper-next-video',
                            prevEl: '.swiper-prev-video',
                        }
                    });
                } else {
                    if (swiperPartenaires != undefined) {
                        swiperPartenaires.destroy();
                    }
                }
            }

        },
        video     : function () {
            if ($('#video_expo').length) {
                var videoSwiper = new Swiper('.playlist .swiper-container', {
                    loop         : false,
                    spaceBetween : 0,
                    slidesPerView: 'auto',
                    watchOverflow: true,
                    navigation   : {
                        nextEl: '.swiper-next-video',
                        prevEl: '.swiper-prev-video',
                    }
                });

                var currentVideo = 0;
                var player       = videojs("video_expo", {
                    fill      : true,
                    controls  : true,
                    muted     : false,
                    autoplay  : true,
                    preload   : 'metadata',
                    controlBar: {
                        playToggle            : true,
                        remainingTimeDisplay  : true,
                        pictureInPictureToggle: false,
                        progressControl       : {
                            seekBar: true
                        }
                    },
                    language  : 'fr',
                    languages : {
                        fr: {
                            "Audio Player"                                                                                                               : "Lecteur audio",
                            "Video Player"                                                                                                               : "Lecteur vidéo",
                            "Play"                                                                                                                       : "Lecture",
                            "Pause"                                                                                                                      : "Pause",
                            "Replay"                                                                                                                     : "Revoir",
                            "Current Time"                                                                                                               : "Temps actuel",
                            "Duration"                                                                                                                   : "Durée",
                            "Remaining Time"                                                                                                             : "Temps restant",
                            "Stream Type"                                                                                                                : "Type de flux",
                            "LIVE"                                                                                                                       : "EN DIRECT",
                            "Loaded"                                                                                                                     : "Chargé",
                            "Progress"                                                                                                                   : "Progression",
                            "Progress Bar"                                                                                                               : "Barre de progression",
                            "progress bar timing: currentTime={1} duration={2}"                                                                          : "{1} de {2}",
                            "Fullscreen"                                                                                                                 : "Plein écran",
                            "Non-Fullscreen"                                                                                                             : "Fenêtré",
                            "Mute"                                                                                                                       : "Sourdine",
                            "Unmute"                                                                                                                     : "Son activé",
                            "Playback Rate"                                                                                                              : "Vitesse de lecture",
                            "Subtitles"                                                                                                                  : "Sous-titres",
                            "subtitles off"                                                                                                              : "Sous-titres désactivés",
                            "Captions"                                                                                                                   : "Sous-titres transcrits",
                            "captions off"                                                                                                               : "Sous-titres transcrits désactivés",
                            "Chapters"                                                                                                                   : "Chapitres",
                            "Descriptions"                                                                                                               : "Descriptions",
                            "descriptions off"                                                                                                           : "descriptions désactivées",
                            "Audio Track"                                                                                                                : "Piste audio",
                            "Volume Level"                                                                                                               : "Niveau de volume",
                            "You aborted the media playback"                                                                                             : "Vous avez interrompu la lecture de la vidéo.",
                            "A network error caused the media download to fail part-way."                                                                : "Une erreur de réseau a interrompu le téléchargement de la vidéo.",
                            "The media could not be loaded, either because the server or network failed or because the format is not supported."         : "Cette vidéo n'a pas pu être chargée, soit parce que le serveur ou le réseau a échoué ou parce que le format n'est pas reconnu.",
                            "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.": "La lecture de la vidéo a été interrompue à cause d'un problème de corruption ou parce que la vidéo utilise des fonctionnalités non prises en charge par votre navigateur.",
                            "No compatible source was found for this media."                                                                             : "Aucune source compatible n'a été trouvée pour cette vidéo.",
                            "The media is encrypted and we do not have the keys to decrypt it."                                                          : "Le média est chiffré et nous n'avons pas les clés pour le déchiffrer.",
                            "Play Video"                                                                                                                 : "Lire la vidéo",
                            "Close"                                                                                                                      : "Fermer",
                            "Close Modal Dialog"                                                                                                         : "Fermer la boîte de dialogue modale",
                            "Modal Window"                                                                                                               : "Fenêtre modale",
                            "This is a modal window"                                                                                                     : "Ceci est une fenêtre modale",
                            "This modal can be closed by pressing the Escape key or activating the close button."                                        : "Ce modal peut être fermé en appuyant sur la touche Échap ou activer le bouton de fermeture.",
                            ", opens captions settings dialog"                                                                                           : ", ouvrir les paramètres des sous-titres transcrits",
                            ", opens subtitles settings dialog"                                                                                          : ", ouvrir les paramètres des sous-titres",
                            ", opens descriptions settings dialog"                                                                                       : ", ouvrir les paramètres des descriptions",
                            ", selected"                                                                                                                 : ", sélectionné",
                            "captions settings"                                                                                                          : "Paramètres des sous-titres transcrits",
                            "subtitles settings"                                                                                                         : "Paramètres des sous-titres",
                            "descriptions settings"                                                                                                      : "Paramètres des descriptions",
                            "Text"                                                                                                                       : "Texte",
                            "White"                                                                                                                      : "Blanc",
                            "Black"                                                                                                                      : "Noir",
                            "Red"                                                                                                                        : "Rouge",
                            "Green"                                                                                                                      : "Vert",
                            "Blue"                                                                                                                       : "Bleu",
                            "Yellow"                                                                                                                     : "Jaune",
                            "Magenta"                                                                                                                    : "Magenta",
                            "Cyan"                                                                                                                       : "Cyan",
                            "Background"                                                                                                                 : "Arrière-plan",
                            "Window"                                                                                                                     : "Fenêtre",
                            "Transparent"                                                                                                                : "Transparent",
                            "Semi-Transparent"                                                                                                           : "Semi-transparent",
                            "Opaque"                                                                                                                     : "Opaque",
                            "Font Size"                                                                                                                  : "Taille des caractères",
                            "Text Edge Style"                                                                                                            : "Style des contours du texte",
                            "None"                                                                                                                       : "Aucun",
                            "Raised"                                                                                                                     : "Élevé",
                            "Depressed"                                                                                                                  : "Enfoncé",
                            "Uniform"                                                                                                                    : "Uniforme",
                            "Dropshadow"                                                                                                                 : "Ombre portée",
                            "Font Family"                                                                                                                : "Famille de polices",
                            "Proportional Sans-Serif"                                                                                                    : "Polices à chasse variable sans empattement (Proportional Sans-Serif)",
                            "Monospace Sans-Serif"                                                                                                       : "Polices à chasse fixe sans empattement (Monospace Sans-Serif)",
                            "Proportional Serif"                                                                                                         : "Polices à chasse variable avec empattement (Proportional Serif)",
                            "Monospace Serif"                                                                                                            : "Polices à chasse fixe avec empattement (Monospace Serif)",
                            "Casual"                                                                                                                     : "Manuscrite",
                            "Script"                                                                                                                     : "Scripte",
                            "Small Caps"                                                                                                                 : "Petites capitales",
                            "Reset"                                                                                                                      : "Réinitialiser",
                            "restore all settings to the default values"                                                                                 : "Restaurer tous les paramètres aux valeurs par défaut",
                            "Done"                                                                                                                       : "Terminé",
                            "Caption Settings Dialog"                                                                                                    : "Boîte de dialogue des paramètres des sous-titres transcrits",
                            "Beginning of dialog window. Escape will cancel and close the window."                                                       : "Début de la fenêtre de dialogue. La touche d'échappement annulera et fermera la fenêtre.",
                            "End of dialog window."                                                                                                      : "Fin de la fenêtre de dialogue."
                        }
                    }
                });

                /**
                 * Update playlist timer
                 */
                player.on('timeupdate', function () {
                    updateLibrarySeek(this);
                });
                updateLibrarySeek(player);

                function updateLibrarySeek(player) {
                    var $currentVideoPlaylist = $('.video_info .playlist .video').eq(currentVideo);
                    var progPercent           = player.currentTime() * 100 / player.duration();
                    $currentVideoPlaylist.find('.progress').css('width', 100 - progPercent + '%');
                }

                /**
                 * Play next video at end
                 */
                player.on('ended', function () {
                    playNextVideo(this);
                });

                function playNextVideo(player) {
                    var newIndex           = currentVideo + 1;
                    var $nextVideoPlaylist = $('.video_info .playlist .video').eq(newIndex);
                    if (!$nextVideoPlaylist.length) {
                        newIndex = 0;
                    }

                    changeVideo(newIndex);
                }

                /**
                 * Click playlist
                 */
                $('.video_info .playlist .video a').on('click', function (e) {
                    e.preventDefault();
                    var newIndex = $(this).parent().index();
                    changeVideo(newIndex);
                });

                /**
                 * Set a new video based on playlist index
                 *
                 * @param newIndex
                 */
                function changeVideo(newIndex) {
                    var newSource = $('.video_info .playlist .video').eq(newIndex).find('source').attr('src');
                    newSource     = newSource.replace('#t=10,20', '');

                    if (!player.paused) {
                        player.pause();
                    }

                    player.src({type: 'video/mp4', src: newSource});
                    player.load();
                    player.play();

                    //Replace caption
                    var newCaption = $('.video_info .playlist .video').eq(newIndex).find('video').data('caption');
                    $('.video_info .pres .caption').html(newCaption);

                    //Reset all progresses
                    $('.video_info .playlist .video .progress').css('width', 100 + '%');

                    currentVideo = newIndex;

                    videoSwiper.slideTo(newIndex);
                }
            }
        },
        media     : function () {
            if (window.location.href.indexOf('no-script') == -1) {
                var playerAudios = new Plyr.setup('audio', {
                    controls: ['play', 'progress', 'current-time', 'mute', 'volume']
                });
                var playerVideos = new Plyr.setup('body.single video', {
                    controls    : ['play', 'progress', 'current-time', 'mute', 'volume'],
                    hideControls: true,
                    clickToPlay : true
                });

                $.each(playerVideos, function (index, player) {
                    player.once('ready', function () {
                        var player  = this;
                        var wrapper = $(player.elements.wrapper);
                        player.toggleControls(false);
                        wrapper.parent().find('.plyr__controls').hide();
                        wrapper.after('<div class="video_overlay"><span class="dashicons dashicons-controls-play"></span></div>');
                        wrapper.parent().find('.video_overlay').click(function () {
                            player.play();
                            $(this).remove();
                            wrapper.parent().find('.plyr__controls').show();
                            player.toggleControls(true);
                        });
                    });
                });
            }
        },
        modal     : function () {
            $('a[data-type="definition"]').click(function () {
                var id                  = $(this).data('id');
                var $definition_content = $(document).find('.definition_placeholder[data-id="' + id + '"]');
                if ($definition_content.length) {
                    var title   = $definition_content.data('title');
                    var content = $definition_content.html();
                    $('#modal_definition .modal-title').html(title);
                    $('#modal_definition .modal-body').html(content);
                    $('#modal_definition').modal();
                }
            });
        },
        agenda    : function () {
            var swiperAgenda = new Swiper('body.page-template-template-agenda-borne .swiper-container', {
                loop         : true,
                direction    : 'vertical',
                spaceBetween : 20,
                slidesPerView: 3,
                autoplay     : {
                    delay: 5000,
                }
            });

            //Fix duplicated slides if not enough in loop
            if (swiperAgenda.loopedSlides < 3) {
                swiperAgenda.destroy(false, false);
                swiperAgenda.params.loop = false;
                swiperAgenda.init();
            }

        },
        masonry   : function () {
            if ($('body.page').length && $('.wp-block-gallery').length) {
                var columns = 4;
                var reg     = /columns-(.?)/g;
                var cols    = reg.exec($('.wp-block-gallery').attr('class'));
                if (cols.length > 1) {
                    columns = cols[1];
                }

                var macyInstance = Macy({
                    container    : '.blocks-gallery-grid',
                    trueOrder    : false,
                    waitForImages: false,
                    margin       : 15,
                    columns      : columns,
                    breakAt      : {
                        760: {
                            margin : 10,
                            columns: 2
                        }
                    }
                });

                macyInstance.runOnImageLoad(function (event) {
                    macyInstance.recalculate(true);
                    if (event.data.img) {
                        $(event.data.img).addClass('show');
                    }
                }, true);
            }
        },
        gallery   : function () {
            if (!$('body').hasClass('borne')) {
                if ($('.wp-block-gallery').length) {

                    var mediasGalleries = [];
                    $('.wp-block-gallery').each(function (galleryIndex, galleryElement) {
                        var medias = [];
                        $(this).find('.blocks-gallery-item img').each(function (imageIndex, imageElement) {
                            var caption     = '';
                            var $figcaption = $(this).parent().find('figcaption');
                            if ($figcaption.length) {
                                caption = $figcaption.html();
                            }

                            var src = $(this).attr('src');
                            if ($(this).attr('data-full-url')) {
                                src = $(this).attr('data-full-url');
                            }

                            medias.push({
                                src  : src,
                                thumb: $(this).attr('src'),
                                title: '',
                                desc : caption
                            });
                        });
                        mediasGalleries.push(medias);
                    });

                    var wasDefined = false;
                    if (window.lightbox === undefined) {
                        window.lightbox = $.ModuloBox({
                            scrollToZoom  : true,
                            controls      : ['zoom', 'fullScreen', 'close'],
                            timeToIdle    : 0,
                            counterMessage: '',
                            tapToClose    : false,
                            loadError     : 'Désolé, une erreur s\'est produite...',
                            noContent     : 'Désolé, aucun contenu n\'a été trouvé'
                        });
                    } else {
                        wasDefined = true;
                    }

                    window.lightbox.on('updateGalleries.modulobox', function (galleries) {
                        $.each(mediasGalleries, function (index, element) {
                            if (galleries['images_' + index] === undefined) {
                                window.lightbox.addMedia('images_' + index, element);
                            }
                        });
                    });

                    window.lightbox.on('beforeOpen.modulobox', function (gallery, index) {
                        if (gallery.match(/images/g)) {
                            $('.mobx-holder .mobx-top-bar .open_infobox').remove();
                            $('.mobx-holder .mobx-top-bar .infobox').remove();

                            $('.mobx-holder .mobx-prev').html('<span>Précédent</span>').addClass('border_colored colored');
                            $('.mobx-holder .mobx-next').html('<span>Suivant</span>').addClass('border_colored colored');
                        }
                    });

                    if (wasDefined) {
                        window.lightbox.updateGalleries();
                    } else {
                        window.lightbox.init();
                    }

                    $('.wp-block-gallery').each(function (galleryIndex, galleryElement) {
                        $(this).find('img').click(function () {
                            var imageIndex = $(this).parent().parent().index();
                            window.lightbox.open('images_' + galleryIndex, imageIndex);
                        });
                    });
                }
            }
        }
    };

    $(document).ready(function () {
        expo.init();
    });

})(jQuery);
