<?php
/**
 * Plugin Name:     Expo - Liseuse IIIF
 * Description:     Une liseuse IIIF en JS
 * Version:         1.0.0
 * Author:          Guillaume Raoult
 * License:         GPL-2.0-or-later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     create-block
 *
 * @package         create-block
 */

/**
 * Registers all block assets so that they can be enqueued through the block editor
 * in the corresponding context.
 *
 * @see https://developer.wordpress.org/block-editor/tutorials/block-tutorial/applying-styles-with-stylesheets/
 */
function create_block_iiif_block_block_init()
{
    $dir = dirname(__FILE__);

    $script_asset_path = "$dir/build/index.asset.php";
    if (!file_exists($script_asset_path)) {
        throw new Error('You need to run `npm start` or `npm run build` for the "create-block/iiif-block" block first.');
    }
    $index_js     = 'build/index.js';
    $script_asset = require($script_asset_path);
    wp_register_script('create-block-iiif-block-block-editor', plugins_url($index_js, __FILE__), $script_asset['dependencies'], $script_asset['version']);

    wp_enqueue_style('lightbox-css', plugins_url('assets/lightbox.css', __FILE__), array(), '1.5.0');
    wp_enqueue_script('lightbox-js', plugins_url('assets/lightbox.js', __FILE__), array('jquery'), '1.5.0', true);

    wp_register_script('create-block-iiif-block-block-front', plugins_url('build/front.js', __FILE__), array('lightbox-js'), '');

    $editor_css = 'build/index.css';
    wp_register_style('create-block-iiif-block-block-editor', plugins_url($editor_css, __FILE__), array(), filemtime("$dir/$editor_css"));

    $style_css = 'build/style-index.css';
    wp_register_style('create-block-iiif-block-block', plugins_url($style_css, __FILE__), array(), filemtime("$dir/$style_css"));

    register_block_type('create-block/iiif-block', array(
        'editor_script' => 'create-block-iiif-block-block-editor',
        'editor_style'  => 'create-block-iiif-block-block-editor',
        'script'        => 'create-block-iiif-block-block-front',
        'style'         => 'create-block-iiif-block-block',
    ));
}


add_action('init', 'create_block_iiif_block_block_init');
