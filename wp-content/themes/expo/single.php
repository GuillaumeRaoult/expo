<?php
/**
 * The template for displaying all single posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Expo
 */

set_query_var('header-color', 'transparent sticky-white open-white sticky-open-white');
get_header();

$subtitle = '';
if (is_singular('post')) {
    $subtitle = 'Agenda';
}
if (is_singular('temoignage')) {
    $subtitle = get_post_meta(get_the_ID(), 'temoignage_subtitle', true);
}
set_query_var('subtitle', $subtitle);

get_template_part('template-parts/title', '');
get_template_part('template-parts/breadcrumbs', '');
?>
    <div id="main_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4 col-md-3 col-xl-2 offset-xl-1">
                    <?php
                    get_template_part('template-parts/metas/meta', get_post_type());
                    ?>
                </div>
                <div class="col-sm-8 col-md-6">
                    <?php
                    the_content();
                    ?>
                </div>
            </div>
        </div>
        <?php
        if (!get_query_var('borne', '')) {
            ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6 offset-sm-3">
                        <div class="single_footer">
                            <p class="author">Partager</p>
                            <p class="share">
                                <?php
                                shareButtons();
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php
get_footer();
