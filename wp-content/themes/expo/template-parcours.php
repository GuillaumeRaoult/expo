<?php /* Template Name: Parcours */ ?>

<?php

set_query_var('header-color', 'transparent sticky-white open-white sticky-open-white');
get_header();

get_template_part('template-parts/title', '');
get_template_part('template-parts/breadcrumbs', '');
?>
<div id="main_content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8 offset-sm-2">
                <div class="content">
                    <?php
                    the_content();
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col col-sm-8 offset-sm-2">
                <div class="oeuvres_cont">
                    <?php
                    global $post;
                    $oeuvres = get_posts(array(
                        'post_type'        => 'oeuvre',
                        'posts_per_page'   => '-1',
                        'suppress_filters' => false
                    ));
                    if ($oeuvres) {
                        ?>
                        <div class="row">
                            <?php
                            foreach ($oeuvres as $k => $post) {
                                setup_postdata($post);
                                ?>
                                <div class="col-12">
                                    <a href="<?php the_permalink() ?>" class="oeuvre">
                                        <?php the_post_thumbnail() ?>
                                        <h2>
                                            <?php the_title() ?>
                                            <span class="extract"><?php the_excerpt() ?></span>
                                            <span class="link colored">- Découvrir</span>
                                        </h2>
                                    </a>
                                </div>
                                <?php
                                wp_reset_postdata();
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>
