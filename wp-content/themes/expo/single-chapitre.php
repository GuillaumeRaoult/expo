<?php

$isBorne = get_query_var('borne', '');

set_query_var('header-color', 'transparent sticky-white open-white sticky-open-white');

$headerTitle = get_the_title();
set_query_var('header-title', $headerTitle);

if (has_blocks(get_the_content())) {
    $chapitres = [];
    $blocks    = parse_blocks(get_the_content());
    foreach ($blocks as $block) {
        if ($block['blockName'] == 'core/heading' && !isset($block['attrs']['level'])) {
            $chapitres[] = trim(strip_tags($block['innerHTML']));
        }
    }
    set_query_var('header-chapitre', $chapitres);
}

get_header();

set_query_var('subtitle', get_post_meta(get_the_ID(), 'chapitre_subtitle', true));

get_template_part('template-parts/title', '');
get_template_part('template-parts/breadcrumbs', '');
?>
<div id="main_content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 offset-0 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
                <?php
                the_content();
                ?>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 offset-0 col-lg-8 offset-lg-2">
                <div class="single_footer">
                    <?php
                    $author = get_post_meta(get_the_ID(), 'chapitre_author', true);
                    $date   = get_post_meta(get_the_ID(), 'chapitre_date', true);
                    if ($author || $date) {
                        ?>
                        <p class="author">
                            <?php
                            if ($author) {
                                echo $author . ', ';
                            }
                            if ($date) {
                                echo strftime('%d/%m/%Y', $date);
                            }
                            ?>
                        </p>
                        <?php
                    }
                    ?>
                    <p class="share">
                        <?php
                        shareButtons();
                        ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="chapter_navigation">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <?php
                        $prevPost = get_previous_post();
                        $nextPost = get_next_post();
                        if ($isBorne) {
                            global $post;
                            $nextPrevious = getBorneNextPreviousLink($prevPost, $nextPost, $post);
                            $nextPost     = $nextPrevious[0];
                            $prevPost     = $nextPrevious[1];
                        }

                        if ($nextPost) {
                            ?>
                            <div class="col-6">
                                <a href="<?php echo get_permalink($nextPost) ?>" class="prev hover_colored">
                                    <?php echo get_the_title($nextPost) ?>
                                    <p class="colored"><span class="dashicons dashicons-arrow-left-alt"></span>Précédent
                                    </p>
                                </a>
                            </div>
                            <?php
                        }
                        if ($prevPost) {
                            $class = !$nextPost ? 'offset-6' : '';
                            ?>
                            <div class="col-6 <?php echo $class ?>">
                                <a href="<?php echo get_permalink($prevPost) ?>" class="next hover_colored">
                                    <?php echo get_the_title($prevPost) ?>
                                    <p class="colored">Suivant<span class="dashicons dashicons-arrow-right-alt"></span>
                                    </p>
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>
