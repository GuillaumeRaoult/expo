<?php

$isBorne = get_query_var('borne', '');

set_query_var('header-color', 'white sticky-white open-white sticky-open-white');

get_header();
get_template_part('template-parts/breadcrumbs', '');
?>
<div id="main_content">
    <div class="container-fluid nopadding">
        <div class="row cont no-gutters">
            <div class="col-sm-6 thumb">
                <?php
                $manifestPage  = get_post_meta(get_the_ID(), 'iiif_page', true);
                $manifestUrl   = get_post_meta(get_the_ID(), 'iiif_manifest', true);
                $manifestStart = get_post_meta(get_the_ID(), 'iiif_start', true);
                $sideLink      = get_post_meta(get_the_ID(), 'side_link', true);
                if ($manifestUrl) {
                    ?>
                    <a href="javascript:void(0)" class="open_manifest colored"
                       data-iiif-viewer-manifest="<?php echo $manifestUrl ?>"
                       data-iiif-viewer-docUrl="<?php echo $manifestPage ?>"
                       data-iiif-viewer-start="<?php echo $manifestStart ?>">
                        <span class="dashicons dashicons-visibility"></span>Voir ce document
                    </a>
                    <?php
                } else if ($sideLink) {
                    ?>
                    <a href="<?php echo $sideLink ?>" target="_blank" rel="external noopener noreferrer"
                       class="open_manifest colored">
                        <span class="dashicons dashicons-visibility"></span>Voir le document
                    </a>
                    <?php
                }
                the_post_thumbnail();
                ?>
            </div>
            <div class="col-sm-6 content">
                <?php
                $oeuvreSubtitle = cmb2_get_option('general_options', 'oeuvres_subtitle', 'Parcours musical');
                if ($isBorne) {
                    $oeuvreSubtitle       = getBorneParcoursLink($post, $oeuvreSubtitle);
                }
                ?>
                <p class="pres"><?php echo $oeuvreSubtitle ?></p>
                <h1><?php the_title(); ?></h1>
                <?php
                the_content();
                ?>
                <div class="oeuvre_navigation">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <?php
                                    $prevPost = get_previous_post();
                                    $nextPost = get_next_post();
                                    if ($isBorne) {
                                        global $post;
                                        $nextPrevious = getBorneNextPreviousLink($prevPost, $nextPost, $post);
                                        $nextPost     = $nextPrevious[0];
                                        $prevPost     = $nextPrevious[1];
                                    }

                                    if ($nextPost) {
                                        ?>
                                        <div class="col-6">
                                            <a href="<?php echo get_permalink($nextPost) ?>" class="prev">
                                                <?php echo get_the_title($nextPost) ?>
                                                <p class="colored"><span
                                                            class="dashicons dashicons-arrow-left-alt"></span>Précédent
                                                </p>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                    if ($prevPost) {
                                        $class = !$nextPost ? 'offset-6' : '';
                                        ?>
                                        <div class="col-6 <?php echo $class ?>">
                                            <a href="<?php echo get_permalink($prevPost) ?>" class="next">
                                                <?php echo get_the_title($prevPost) ?>
                                                <p class="colored">Suivant<span
                                                            class="dashicons dashicons-arrow-right-alt"></span>
                                                </p>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
set_query_var('footer', 'no-footer');
get_footer();
?>
