<?php
/**
 * Plugin Name:     Expo - Carousel
 * Description:     Carousel d'images
 * Version:         1.0.0
 * Author:          Guillaume Raoult
 * License:         GPL-2.0-or-later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     create-block
 *
 * @package         create-block
 */

/**
 * Registers all block assets so that they can be enqueued through the block editor
 * in the corresponding context.
 *
 * @see https://developer.wordpress.org/block-editor/tutorials/block-tutorial/applying-styles-with-stylesheets/
 */
function create_block_expo_carousel_block_init() {
	$dir = dirname( __FILE__ );

	$script_asset_path = "$dir/build/index.asset.php";
	if ( ! file_exists( $script_asset_path ) ) {
		throw new Error(
			'You need to run `npm start` or `npm run build` for the "create-block/expo-carousel" block first.'
		);
	}
	$index_js     = 'build/index.js';
	$script_asset = require( $script_asset_path );
	wp_register_script(
		'create-block-expo-carousel-block-editor',
		plugins_url( $index_js, __FILE__ ),
		$script_asset['dependencies'],
		$script_asset['version']
	);

	$editor_css = 'build/index.css';
	wp_register_style(
		'create-block-expo-carousel-block-editor',
		plugins_url( $editor_css, __FILE__ ),
		array(),
		filemtime( "$dir/$editor_css" )
	);

	$style_css = 'build/style-index.css';
	wp_register_style(
		'create-block-expo-carousel-block',
		plugins_url( $style_css, __FILE__ ),
		array(),
		filemtime( "$dir/$style_css" )
	);

    wp_enqueue_style('swiper-css', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.5/swiper-bundle.min.css', array(), '6.4.5');
    wp_enqueue_script('swiper-js', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.5/swiper-bundle.min.js', array('jquery'), '6.4.5', true);

    wp_register_script(
        'create-block-expo-carousel-block-block-front',
        plugins_url('build/front.js', __FILE__),
        array('swiper-js'),
        ''
    );

	register_block_type( 'create-block/expo-carousel', array(
		'editor_script' => 'create-block-expo-carousel-block-editor',
		'editor_style'  => 'create-block-expo-carousel-block-editor',
        'script'        => 'create-block-expo-carousel-block-block-front',
		'style'         => 'create-block-expo-carousel-block',
	) );
}
add_action( 'init', 'create_block_expo_carousel_block_init' );
