/**
 * Registers a new block provided a unique name and an object defining its behavior.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/#registering-a-block
 */
import {registerBlockType} from '@wordpress/blocks';

/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import {__} from '@wordpress/i18n';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * All files containing `style` keyword are bundled together. The code used
 * gets applied both to the front of your site and to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './style.scss';

import './editor.scss';

import {
    SelectControl,
    PanelBody,
    PanelRow,
    Button,
    Toolbar,
    ToolbarButton,
    ToolbarGroup,
    __experimentalNumberControl as NumberControl,
    ToggleControl
} from '@wordpress/components'

import {withSelect, useSelect} from '@wordpress/data';
import {
    MediaUpload,
    MediaPlaceholder,
    MediaUploadCheck,
    InspectorControls,
    RichText,
    BlockControls,
    AlignmentToolbar,
    __experimentalImageURLInputUI as ImageURLInputUI,
} from '@wordpress/block-editor';

import React, {useEffect} from 'react';

/**
 * Every block starts by registering a new block type definition.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/#registering-a-block
 */
registerBlockType('expo/home', {
    /**
     * This is the display title for your block, which can be translated with `i18n` functions.
     * The block inserter will show this name.
     */
    title: __('Expo Page d\'accueil', 'create-block'),

    /**
     * This is a short description for your block, can be translated with `i18n` functions.
     * It will be shown in the Block Tab in the Settings Sidebar.
     */
    description: __(
        'Blocs pour la page d\'accueil',
        'create-block'
    ),

    /**
     * Blocks are grouped into categories to help users browse and discover them.
     * The categories provided by core are `common`, `embed`, `formatting`, `layout` and `widgets`.
     */
    category: 'expo',

    /**
     * An icon property should be specified to make it easier to identify a block.
     * These can be any of WordPress’ Dashicons, or a custom svg element.
     */
    icon: 'screenoptions',

    /**
     * Optional block extended support features.
     */
    supports: {
        html: false,
    },

    attributes: {
        type      : {
            type   : 'string',
            default: 'post_type',
        },
        buttons   : {
            type   : 'array',
            default: ['<a href="/">Lien 1</a>', '<a href="/">Lien 2</a>', '<a href="/">Lien 3</a>'],
        },
        photo     : {
            type   : 'object',
            default: '',
        },
        photo_text: {
            type    : 'string',
            selector: '.photo_text',
        },
        post_type : {
            type   : 'string',
            default: 'chapitre',
        },
        theme     : {
            type   : 'string',
            default: 'clair',
        },
        title     : {
            type    : 'string',
            selector: '.title',
        },
        desc      : {
            type    : 'string',
            selector: '.desc',
        },
        link      : {
            type    : 'string',
            default : '',
            selector: '.link',
        },
        max_elements : {
            type    : 'number',
            default : 5
        },
        hide_title : {
            type    : 'boolean',
            default : false
        }
    },

    example: {
        attributes: {
            type     : 'post_type',
            post_type: 'chapitre',
            theme    : 'sombre',
            title    : 'Titre',
            desc     : 'Description',
            link     : '<a href="/">Tous les chapitres</a>'
        },
    },

    /**
     * @see ./edit.js
     */
    edit: function ({attributes, className, setAttributes, isSelected}) {
        const customEventOeuvres     = new Event('oeuvresSlider');
        const customEventTemoignages = new Event('temoignagesSlider');
        const expandChaptersButton = new Event('expandChapters');

        const ContentComponentDisplay = withSelect((select) => {
            return {
                posts: select('core').getEntityRecords('postType', attributes.post_type, {
                    order: 'asc'
                }),
                temoignages_tags: select('core').getEntityRecords('taxonomy', 'temoignage_tag', {per_page: 100})
        };
        })(displayContent);

        function displayContent({posts, temoignages_tags}) {
            if (!posts) {
                return 'Chargement...';
            }

            if (posts && posts.length === 0) {
                return 'Aucun contenu publié';
            }

            if (attributes.post_type === 'chapitre') {
                return (<DisplayChapitres posts={posts}/>)
            }

            if (attributes.post_type === 'oeuvre') {
                return (<DisplayOeuvres posts={posts}/>)
            }

            if (attributes.post_type === 'temoignage') {
                return (<DisplayTemoignages posts={posts} tags={temoignages_tags}/>)
            }
        }

        const Chapitre = ({post, index}) => {
            return (
                <div className="col-12 chapitre">
                    <a href={post.link} target="_blank">
                        <div className="row no-gutters align-items-center">
                            <div className="col-6 chapitre_illus animated"
                                 dangerouslySetInnerHTML={{__html: post.featuredTag}}/>
                            <div className="col-6 chapitre_title animated overlap">
                                <h3 className="row no-gutters align-items-center">
                                    <span className="col-1 number colored">{index + 1}</span>
                                    <span className="col-11">{post.title.raw}</span>
                                </h3>
                            </div>
                        </div>
                    </a>

                </div>
            )
        };


        const DisplayChapitres = ({posts}) => {
            useEffect(() => {
                document.dispatchEvent(expandChaptersButton);
            });

            const max_chapitres = attributes.max_elements;
            let firstPosts = [...posts];
            let restPosts = [...posts];
            if (posts.length > max_chapitres) {
                firstPosts = posts.slice(0, max_chapitres);
                restPosts = posts.slice(max_chapitres);
            }

            return (
                <div className="col-10 offset-1">
                    <div className="row ">
                        {firstPosts.map((post, index) => {
                            return (
                                <Chapitre post={post} index={index} />
                            );
                        })}

                        {posts.length > max_chapitres &&
                            <>
                                <div className="col-12 expand_chapters_cont">
                                    {restPosts.map((post, index) => {
                                        return (
                                            <Chapitre post={post} index={max_chapitres+index} />
                                        );
                                    })}
                                </div>
                                <div className="col col-centered">
                                    <div className="custom_button more_chapters colored">
                                        <span class="dashicons dashicons-arrow-down-alt2 colored"></span>
                                    </div>
                                </div>
                            </>
                        }
                    </div>
                </div>
            )
        };

        const DisplayOeuvres = ({posts}) => {
            useEffect(() => {
                document.dispatchEvent(customEventOeuvres);
            });

            const clear = attributes.hide_title ? ' clear' : '';

            return (
                <div className="col-12">
                    <div className="row no-gutters">
                        <div className="col-1">
                            <div className="swiper-nav swiper-prev swiper-prev-oeuvre bg_colored">
                                <span className="dashicons dashicons-arrow-left-alt"></span>
                            </div>
                        </div>
                        <div className="col-10">
                            <div className="swiper-container">
                                <div className="swiper-wrapper">
                                    {posts.map((post, index) => {
                                        return (
                                            <div className={"swiper-slide" + clear}>
                                                <a href={post.link} target="_blank">
                                                    <div className="oeuvre_illus"
                                                         dangerouslySetInnerHTML={{__html: post.featuredTag}}/>
                                                    {!attributes.hide_title && <span>{post.title.raw}</span>}
                                                </a>
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                        </div>
                        <div className="col-1">
                            <div className="swiper-nav swiper-next swiper-next-oeuvre bg_colored">
                                <span className="dashicons dashicons-arrow-right-alt"></span>
                            </div>
                        </div>
                    </div>
                </div>
            )
        };

        const DisplayTemoignages = ({posts, tags}) => {
            useEffect(() => {
                document.dispatchEvent(customEventTemoignages);
            });
            return (
                <div className="col-10 offset-1">
                    <div className="swiper-container">
                        <div className="swiper-wrapper">
                            {posts.map((post, index) => {
                                const postTags = post.temoignage_tag;
                                let selectedTag = null;
                                if (postTags.length > 0) {
                                    for (let j = 0; j < tags.length; j++) {
                                        if (postTags[0] === tags[j].id) {
                                            selectedTag = tags[j];
                                        }
                                    }
                                }

                                let tagHTML = '';
                                if (selectedTag) {
                                    tagHTML = '<span class="bg_colored">'+selectedTag.name+'</span>';
                                }

                                return (
                                    <div className="swiper-slide">
                                        <a href={post.link} target="_blank">
                                            <div className="row no-gutters align-items-center">
                                                <div className="col-6 temoignage_illus"
                                                     dangerouslySetInnerHTML={{__html: post.featuredTag + tagHTML}}/>
                                                <div className="col-6 temoignage_title overlap sombre">
                                                    <div className="row no-gutters align-items-center">
                                                        <h3>
                                                            {post.excerpt.raw}
                                                            <p className="title">{post.title.raw}</p>
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                );
                            })}
                        </div>
                        <div className="swiper-pagination-cont">
                            <div className="swiper-nav swiper-prev swiper-prev-temoignage bg_colored">
                                <span className="dashicons dashicons-arrow-left-alt"></span>
                            </div>
                            <div className="swiper-nav swiper-next swiper-next-temoignage bg_colored">
                                <span className="dashicons dashicons-arrow-right-alt"></span>
                            </div>
                            <div className="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            )
        };

        function removePhoto() {
            setAttributes({
                photo: {}
            });
        }

        const onSelectPhoto = function (selectedImage) {
            setAttributes({
                photo: {
                    imgid       : selectedImage.id,
                    url         : selectedImage.sizes.full.url,
                    thumbnailUrl: selectedImage.sizes.thumbnail.url,
                    alt         : selectedImage.alt,
                    caption     : selectedImage.caption
                }
            });
        };

        let sectionClass = attributes.type === 'post_type' ? attributes.post_type + 's' : '';
        sectionClass     = attributes.type === 'photo' ? 'photo' : sectionClass;
        sectionClass     = attributes.type === 'button' ? 'buttons' : sectionClass;

        const button_row_class = attributes.buttons.length === 4 ? 'col-11' : 'col-12';

        return [
            <InspectorControls>
                <PanelBody title={__("Paramètres")}>
                    <PanelRow>
                        <SelectControl
                            label={__('Type :')}
                            value={attributes.type}
                            onChange={(val) => setAttributes({type: val})}
                            options={[
                                {value: 'button', label: 'Boutons'},
                                {value: 'post_type', label: 'Contenus'},
                                {value: 'photo', label: 'Galerie photo'},
                            ]}
                        />
                    </PanelRow>
                    {attributes.type === 'post_type' && <PanelRow>
                        <SelectControl
                            label={__('Selectionner un contenu :')}
                            value={attributes.post_type}
                            onChange={(val) => setAttributes({post_type: val})}
                            options={[
                                {value: 'chapitre', label: 'Chapitres'},
                                {value: 'oeuvre', label: 'Oeuvres'},
                                {value: 'temoignage', label: 'Témoignages'}
                            ]}
                        />
                    </PanelRow>}
                    {attributes.type ==='post_type' && attributes.post_type === 'chapitre' && <NumberControl
                        label={__('Nombre de chapitres affichés')}
                        labelPosition='top'
                        isShiftStepEnabled={ true }
                        onChange={(val) => setAttributes({max_elements: val})}
                        shiftStep={ 1 }
                        value={ attributes.max_elements }
                    />}
                    {attributes.type === 'button' && <PanelRow>
                        <SelectControl
                            label={__('Nombre de boutons :')}
                            value={attributes.buttons.length}
                            onChange={(val) => {
                                let newButtons = [...attributes.buttons];
                                if (attributes.buttons.length > val) {
                                    newButtons = newButtons.slice(0, val);
                                }
                                if (attributes.buttons.length < val) {
                                    for (let i = newButtons.length; i < val; i++) {
                                        newButtons.push('<a href="/">Titre ' + (newButtons.length + 1) + '</a>');
                                    }

                                    if (newButtons.length == 4) {
                                        newButtons[3] = '<a href="/">+</a>';
                                    }
                                }
                                setAttributes({buttons: newButtons});
                            }}
                            options={[
                                {value: '1', label: '1'},
                                {value: '2', label: '2'},
                                {value: '3', label: '3'},
                                {value: '4', label: '3 +'}
                            ]}
                        />
                    </PanelRow>}
                    {attributes.type ==='post_type' && attributes.post_type === 'oeuvre' &&
                        <ToggleControl
                            label="Masquer les titres"
                            checked={ attributes.hide_title }
                            onChange={ (hidetitle) =>
                                setAttributes({hide_title: hidetitle})
                            }
                        />
                    }
                    <PanelRow>
                        <SelectControl
                            label={__('Thème :')}
                            value={attributes.theme}
                            onChange={(val) => setAttributes({theme: val})}
                            options={[
                                {value: 'clair', label: 'Clair'},
                                {value: 'sombre', label: 'Sombre'}
                            ]}
                        />
                    </PanelRow>
                </PanelBody>
            </InspectorControls>,
            <section className={className + " row section " + sectionClass + " " + attributes.theme}>
                <div className="col-12">
                    <div className="row">
                        <div className="col-8 offset-2 section_title">
                            <RichText
                                tagName="h2"
                                className="title colored"
                                label=""
                                placeholder="Titre"
                                allowedFormats={[]}
                                withoutInteractiveFormatting
                                value={attributes.title}
                                onChange={(val) => setAttributes({title: val})}
                            />
                            <RichText
                                tagName="p"
                                className="desc"
                                label=""
                                placeholder="Description"
                                allowedFormats={[]}
                                withoutInteractiveFormatting
                                value={attributes.desc}
                                onChange={(val) => setAttributes({desc: val})}
                            />
                        </div>
                    </div>
                </div>
                {(attributes.type === 'button') &&
                <div className={button_row_class}>
                    <div className="row">
                        {attributes.buttons.map((button, index) => {
                            let divClass = 'col-4';
                            divClass     = attributes.buttons.length === 1 ? divClass + ' offset-4' : divClass;
                            divClass     = attributes.buttons.length === 2 && index === 0 ? divClass + ' offset-2' : divClass;
                            if (index === 3) {
                                divClass = 'col-1 more_button';
                            }
                            return (
                                <div className={"custom_button " + divClass}>
                                    <div className="row no-gutters">
                                        <div className="col-12">
                                            <RichText
                                                tagName="p"
                                                className="title colored"
                                                label=""
                                                allowedFormats={['core/link']}
                                                value={button}
                                                onChange={(val) => {
                                                    let newButtons    = [...attributes.buttons];
                                                    newButtons[index] = val;
                                                    setAttributes({buttons: newButtons});
                                                }}
                                            />
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
                }
                {(attributes.type === 'post_type') &&
                <>
                    <ContentComponentDisplay/>
                    {(attributes.post_type === 'oeuvre') &&
                    <div className="col-4 offset-4 text-center button_oeuvre">
                        <RichText
                            tagName="p"
                            className="btn outline "
                            allowedFormats={['core/link']}
                            value={attributes.link}
                            onChange={(val) => setAttributes({link: val})}
                            placeholder={'Toutes les oeuvres du parcours'}
                        />
                    </div>
                    }
                </>
                }
                {(attributes.type === 'photo') &&
                <>
                    <BlockControls>
                        <ToolbarGroup>
                            <ImageURLInputUI
                                url={attributes.link || ''}
                                onChangeUrl={(val) => setAttributes({link: val.href})}
                            />
                        </ToolbarGroup>
                    </BlockControls>
                    <div className="col-12">
                        <div className="row no-gutters">
                            <div className="col-4 offset-1 photo_text_cont animated">
                                <RichText
                                    tagName="h3"
                                    className="photo_text"
                                    allowedFormats={[]}
                                    value={attributes.photo_text}
                                    onChange={(val) => setAttributes({photo_text: val})}
                                    placeholder='Titre'
                                />
                            </div>
                            <div className="col-6 photo_cont animated">
                                <MediaUploadCheck>
                                    <MediaUpload
                                        onSelect={selectedImg =>
                                            onSelectPhoto(selectedImg)
                                        }
                                        type="image"
                                        value={attributes.photo.imgid}
                                        accept="image/*"
                                        className=""
                                        render={({open}) => (
                                            <>
                                                <Button className={"image-button"} onClick={open}>
                                                    <div className="photo_placeholder">
                                                        {!jQuery.isEmptyObject(attributes.photo) && (
                                                            <img src={attributes.photo.url}/>
                                                        )}
                                                    </div>
                                                </Button>
                                                {!jQuery.isEmptyObject(attributes.photo) && (
                                                    <div className="mcr-media-row--delete-button">
                                                        <Button
                                                            className={"button button-large"}
                                                            onClick={() => {
                                                                removePhoto();
                                                            }}
                                                        >
                                                            X
                                                        </Button>
                                                    </div>
                                                )}
                                            </>
                                        )}
                                    />
                                </MediaUploadCheck>
                            </div>
                        </div>
                    </div>
                </>
                }
            </section>
        ]
    },
    save(props) {
        return null
    },
});
