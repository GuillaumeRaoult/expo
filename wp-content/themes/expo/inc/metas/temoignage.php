<?php
/**
 * Metas pour les chapitres
 * User: guillaume
 * Date: 25/08/20
 * Time: 10:02
 */

add_action('cmb2_admin_init', 'cmb2_expo_metaboxes_temoignage');
function cmb2_expo_metaboxes_temoignage()
{
    $cmbChapitre = new_cmb2_box(array(
        'id'           => 'temoignage_metabox',
        'title'        => __('Informations', 'cmb2'),
        'object_types' => array('temoignage'),
        'context'      => 'side',
        'priority'     => 'low',
        'show_names'   => true,
    ));

    $cmbChapitre->add_field(array(
        'name' => 'Sous-titre',
        'id'   => 'temoignage_subtitle',
        'type' => 'text',
        'attributes'  => array(
            'placeholder' => 'Autre regard',
        ),
    ));

}

/**
 * Create two taxonomies, genres and writers for the post type "book".
 *
 * @see register_post_type() for registering custom post types.
 */
function addTemoignagesTag()
{
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => __('Tags'),
        'singular_name'     => __('Tag'),
        'search_items'      => __('Chercher les tags'),
        'all_items'         => __('Tous les tags'),
        'parent_item'       => __('Tag parent'),
        'parent_item_colon' => __('Tag parent:'),
        'edit_item'         => __('Editer le tag'),
        'update_item'       => __('Mettre à jour le tag'),
        'add_new_item'      => __('Ajouter un nouveau tag'),
        'new_item_name'     => __('Nom du nouveau tag'),
        'menu_name'         => __('Tag', 'textdomain'),
    );

    $args = array(
        'public'            => false,
        'rewrite'           => false,
        'hierarchical'      => false,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'show_in_rest'      => true,
    );

    register_taxonomy('temoignage_tag', 'temoignage', $args);

}

// hook into the init action and call create_book_taxonomies when it fires
add_action('init', 'addTemoignagesTag', 0);