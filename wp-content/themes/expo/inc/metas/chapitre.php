<?php
/**
 * Metas pour les chapitres
 * User: guillaume
 * Date: 25/08/20
 * Time: 10:02
 */

add_action('cmb2_admin_init', 'cmb2_expo_metaboxes_chapitre');
function cmb2_expo_metaboxes_chapitre()
{
    $cmbChapitre = new_cmb2_box(array(
        'id'           => 'chapitre_metabox',
        'title'        => __('Informations', 'cmb2'),
        'object_types' => array('chapitre'),
        'context'      => 'side',
        'priority'     => 'low',
        'show_names'   => true,
    ));

    $cmbChapitre->add_field(array(
        'name'       => 'Sous-titre',
        'id'         => 'chapitre_subtitle',
        'type'       => 'text',
        'attributes' => array(
            'placeholder' => 'L\'histoire racontée',
        ),
    ));

    $cmbChapitre->add_field(array(
        'name' => 'Auteur',
        'id'   => 'chapitre_author',
        'type' => 'text',
    ));

    $cmbChapitre->add_field(array(
        'name'        => 'Date',
        'id'          => 'chapitre_date',
        'type'        => 'text_date_timestamp',
        'date_format' => 'd/m/Y',
    ));
}