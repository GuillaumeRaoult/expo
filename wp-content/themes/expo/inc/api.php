<?php
/**
 * Created by PhpStorm.
 * User: guillaume
 * Date: 26/08/20
 * Time: 18:28
 */

add_action('rest_api_init', 'createApiFeaturedTagField');
function createApiFeaturedTagField()
{
    $postTypes = array(
        'chapitre',
        'oeuvre',
        'temoignage'
    );
    foreach ($postTypes as $postType) {
        register_rest_field($postType, 'featuredTag', array(
            'get_callback' => 'generateFeaturedTag',
            'schema'       => null,
        ));
    }
}

function generateFeaturedTag($object)
{
    //On temoignage, send back mobile image
    $thumb = get_the_post_thumbnail($object['id']);
    if ($object['type'] === 'temoignage') {
        if (get_post_meta($object['id'], 'mobile_image_thumbnail', true)) {
            $mobileUrl    = get_post_meta($object['id'], 'mobile_image_thumbnail', true);
            $attachmentId = attachment_url_to_postid($mobileUrl);
            $thumb        = wp_get_attachment_image($attachmentId, 'large');
        }
    }

    return $thumb;
}

add_filter('rest_page_query', 'addMetaFieldPageQuery', 10, 2);
function addMetaFieldPageQuery($args, $request)
{
    if ($meta_key = $request->get_param('meta_key')) {
        $args['meta_key']   = $meta_key;
        $args['meta_value'] = $request->get_param('meta_value');
    }

    return $args;
}
