(function ($) {
    $(document).ready(function () {

        $('#options_url_group_repeat input[id^="options_url_group_"]').blur(function () {
            if ($(this).hasClass('borne_url_input')) {
                var currentVal = $(this).val();
                if (isInternalUrl(currentVal)) {
                    var lastChar   = currentVal.slice(-1);
                    if (lastChar != '/') {
                        $(this).val(currentVal + '/');
                    }
                }
            }
            addLinksButtons();
            validateFields();
        });

        addLinksButtons();
        validateFields();

        function addLinksButtons() {
            $('.button-link-original').remove();
            $('.button-link-borne').remove();
            $('#options_url_group_repeat input[class^="borne_url_input"]').each(function () {
                var val     = $(this).val();
                if (val) {
                    var parents = $(this).parents('*[id^="cmb-group-options_url_group-"]');
                    var suffix  = parents.find('.borne_suffix_input').val();
                    if (val.indexOf("*") > -1) {
                        val = val.slice(0, -2);
                        if (val.indexOf("/oeuvre/") > -1 || val.indexOf("/chapitre/") > -1) {
                            val = '';
                        }
                    }
                    var disabled = !suffix ? 'disabled' : '';
                    if (val) {
                        var buttonPrimary   = '<a target="_blank" class="button button-secondary button-link-original" href="' + val + '" >Page</a>';
                        var buttonSecondary = '';
                        if (isInternalUrl(val)) {
                            buttonSecondary = '<a ' + disabled + ' target="_blank" class="button button-secondary button-link-borne" href="' + val + suffix + '/">Page borne</a>';
                        }
                        $(this).after(buttonPrimary + buttonSecondary);
                    }
                }
            });
        }

        function validateFields() {
            var vals = [];
            $('#options_url_group_repeat input[class^="borne_url_input"]').each(function () {
                if ($(this).val() != '') {
                    vals.push($(this).val());
                }
            });

            $.ajax({
                url: bornejs.ajaxurl,
                type: "POST",
                data: {
                    'action': 'validate_urls',
                    'urls': vals
                }
            }).done(function(response) {
                const urls = JSON.parse(response);
                $('#options_url_group_repeat input[class^="borne_url_input"]').removeClass('invalid');
                if (urls.length > 0) {
                    $.each(urls, function( index, url ) {
                        $('#options_url_group_repeat input[class^="borne_url_input"]').each(function () {
                            if ($(this).val() == url) {
                                $(this).addClass('invalid');
                            }
                        });
                    });
                }
            });
        }

        function isInternalUrl(url) {
            var urlObject = new URL(url);
            return urlObject.hostname == window.location.hostname;
        }
    });
})(jQuery);
