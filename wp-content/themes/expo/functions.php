<?php
/**
 * Expo functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Expo
 */

/**
 * CPTs
 */
require get_template_directory() . '/inc/cpt/chapitre.php';
require get_template_directory() . '/inc/cpt/oeuvre.php';
require get_template_directory() . '/inc/cpt/glossaire.php';
require get_template_directory() . '/inc/cpt/temoignage.php';

/**
 * Metas
 */
require get_template_directory() . '/inc/metas/mobile.php';
require get_template_directory() . '/inc/metas/page.php';
require get_template_directory() . '/inc/metas/oeuvre.php';
require get_template_directory() . '/inc/metas/chapitre.php';
require get_template_directory() . '/inc/metas/temoignage.php';
require get_template_directory() . '/inc/metas/post.php';
require get_template_directory() . '/inc/metas/options.php';

/**
 * Admin
 */
require get_template_directory() . '/inc/admin.php';

/**
 * API
 */
require get_template_directory() . '/inc/api.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Helpers functions
 */
require get_template_directory() . '/inc/helpers.php';

/**
 * Bootstrap Nav Walker
 */
require get_template_directory() . '/inc/class-bootstrap-collapse-navwalker.php';

if (!defined('_S_VERSION')) {
    // Replace the version number of the theme on each release.
    define('_S_VERSION', '1.0.0');
}

if (!function_exists('expoSetup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function expoSetup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Expo, use a find and replace
         * to change 'expo' to the name of your theme in all the template files.
         */
        load_theme_textdomain('expo', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-principal' => esc_html__('Principal', 'expo'),
            'footer'         => esc_html__('Footer', 'expo'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
            'style',
            'script',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('expo_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height'      => 250,
            'width'       => 250,
            'flex-width'  => true,
            'flex-height' => true,
        ));

        add_theme_support('editor-styles');
        add_editor_style('assets/css/editor.css');
        add_theme_support('editor-color-palette');
        add_theme_support('disable-custom-colors');

        add_theme_support('editor-color-palette', array(
            array(
                'name'  => __('Couleur de l\'expo'),
                'slug'  => 'expo-color',
                'color' => '#' . get_header_textcolor()
            )
        ));

        remove_theme_support('core-block-patterns');

        //Custom image size for overlays
        add_image_size('header', 1400);
    }
endif;
add_action('after_setup_theme', 'expoSetup');

/**
 * Enqueue scripts and styles.
 */
function expoScripts()
{
    wp_deregister_script('jquery');
    wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', false, '1.12.4', true);
    wp_enqueue_script('jquery');

    wp_enqueue_style('dashicons');

    wp_enqueue_script('bootstrap-js', '//stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js', array('jquery'), '4.5.2', true);
    wp_enqueue_script('expo-global', get_theme_file_uri('/assets/js/site.min.js'), array('bootstrap-js'), filemtime(get_stylesheet_directory() . '/assets/js/site.min.js'), true);

    wp_enqueue_style('bootstrap-css', '//stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css', array(), '4.5.2');
    wp_enqueue_style('expo-font', 'https://fonts.googleapis.com/css2?family=Lato:wght@400;500;800&family=Lora:wght@400;500;700&display=swap', array(), '1.0.0');

    if (!wp_script_is('swiper-js')) {
        wp_enqueue_script('swiper-js', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.5/swiper-bundle.min.js', array('jquery'), '6.4.5', true);
    }
    if (!wp_style_is('swiper-css')) {
        wp_enqueue_style('swiper-css', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.5/swiper-bundle.min.css', array(), '6.4.5');
    }

    if (!wp_script_is('lightbox-js')) {
        wp_enqueue_script('lightbox-js', plugins_url('assets/dist/lightbox.js', __FILE__), array('jquery'), '1.5.0', true);
    }
    if (!wp_style_is('lightbox-css')) {
        wp_enqueue_style('lightbox-css', plugins_url('assets/dist/lightbox.css', __FILE__), array(), '1.5.0');
    }

    wp_enqueue_style('video-css', '//cdnjs.cloudflare.com/ajax/libs/video.js/7.8.4/video-js.min.css', array(), '7.8.4');
    wp_enqueue_script('video-js', '//cdnjs.cloudflare.com/ajax/libs/video.js/7.8.4/video.min.js', array('jquery'), '7.8.4', true);

    wp_enqueue_style('plyr-css', 'https://cdn.plyr.io/3.6.3/plyr.css', array(), '3.6.3');
    wp_enqueue_script('plyr-js', 'https://cdn.plyr.io/3.6.3/plyr.polyfilled.js', array('jquery'), '3.6.3', true);

    wp_enqueue_script('macy-js', '//cdn.jsdelivr.net/npm/macy@2', array(), '2.5.1', true);

    wp_enqueue_style('expo-min-style', get_theme_file_uri('/assets/css/style.min.css'), array(), filemtime(get_stylesheet_directory() . '/assets/css/style.min.css'));

    wp_localize_script('expo-global', 'expojs', array(
        'ajaxurl' => admin_url('admin-ajax.php')
    ));
}

add_action('wp_enqueue_scripts', 'expoScripts');

function expoAdminScripts()
{
    wp_enqueue_style('expo-admin-style', get_theme_file_uri('/assets/css/admin.css'), array(), filemtime(get_stylesheet_directory() . '/assets/css/admin.css'));
    wp_enqueue_style('expo-font', 'https://fonts.googleapis.com/css2?family=Lato:wght@400;500;800&family=Lora:wght@400;500&display=swap', array(), '1.0.0');
    wp_enqueue_style('bootstrap-grid', '//cdn.jsdelivr.net/npm/bootstrap-4-grid@3.4.0/css/grid.min.css');
}

add_action('admin_enqueue_scripts', 'expoAdminScripts');

function expoBlockEditorScript()
{
    wp_enqueue_script('expo-block-script', get_theme_file_uri('/assets/js/remove-block-styles.js'), array(
        'wp-blocks',
        'wp-dom'
    ), filemtime(get_stylesheet_directory() . '/assets/js/remove-block-styles.js'), true);
}

add_action('enqueue_block_editor_assets', 'expoBlockEditorScript');


function addCustomImageSizes($sizes)
{
    return array_merge($sizes, array(
        'header' => __('Header'),
    ));
}

add_filter('image_size_names_choose', 'addCustomImageSizes');


/**
 * Redirect /
 */
function redirectRoot()
{
    $isLogin = in_array($GLOBALS['pagenow'], array(
        'wp-login.php',
        'wp-register.php',
        'wp-cron.php'
    ));

    if (!is_admin() && !$isLogin && !WP_DEBUG) {
        $path = get_blog_details()->path;

        if ($path == '/') {
            if (is_admin()) {
                wp_safe_redirect('/deodat-de-severac/wp-admin/');
            } else {
                wp_safe_redirect('/deodat-de-severac/');
            }
            exit;
        }

        $isPublic = get_blog_status(get_current_blog_id(), 'public');
        if (!$isPublic && !is_user_logged_in()) {
            wp_die('Vous n\'êtes pas autorisé à accéder à ce site.');
        }

    }
}

add_action('init', 'redirectRoot');

/**
 * Favicon
 */
function setFavicon($url, $size, $blog_id)
{
    return get_stylesheet_directory_uri() . '/favicon.ico';

}

add_filter('get_site_icon_url', 'setFavicon', 10, 3);

/**
 * Disable the emoji's
 */
function disableEmojis()
{
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    add_filter('tiny_mce_plugins', 'disableEmojisTinymce');
    add_filter('wp_resource_hints', 'disableEmojisRemoveDnsPrefetch', 10, 2);
}

add_action('init', 'disableEmojis');

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 *
 * @return array Difference betwen the two arrays
 */
function disableEmojisTinymce($plugins)
{
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    } else {
        return array();
    }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array  $urls          URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 *
 * @return array Difference betwen the two arrays.
 */
function disableEmojisRemoveDnsPrefetch($urls, $relation_type)
{
    if ('dns-prefetch' == $relation_type) {
        /** This filter is documented in wp-includes/formatting.php */
        $emoji_svg_url = apply_filters('emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/');

        $urls = array_diff($urls, array($emoji_svg_url));
    }

    return $urls;
}

function expoCustomizeCss()
{
    $homeTopBbackgroundImageUrlDesktop = '';
    $homeTopBackgroundImageUrlMobile   = '';
    if (get_theme_mod('expo_home_background_image')) {
        $homeTopBackgroundImageUrl         = get_theme_mod('expo_home_background_image');
        $homeTopBackgroundImageUrl         = attachment_url_to_postid($homeTopBackgroundImageUrl);
        $homeTopBbackgroundImageUrlDesktop = wp_get_attachment_image_url($homeTopBackgroundImageUrl, 'header');
        $homeTopBackgroundImageUrlMobile   = wp_get_attachment_image_url($homeTopBackgroundImageUrl, 'large');
    }

    $prefix    = '';
    $important = '';
    if (is_admin()) {
        $prefix = '.wp-block';
        //        $important = ' !important';
    }

    $opacity  = 0.3;
    $delay    = 2;
    $duration = 2;
    if (isset($_GET['opacity'])) {
        $opacity = $_GET['opacity'];
    }
    if (isset($_GET['delay'])) {
        $delay = $_GET['delay'];
    }
    if (isset($_GET['duration'])) {
        $duration = $_GET['duration'];
    }
    $opacity  = ' rgba(0,0,0,' . $opacity . ')';
    $duration = $duration . 's';
    $delay    = $delay . 's';
    ?>
    <style type="text/css">
        :root {
            --main-expo-color: <?php echo '#'.get_header_textcolor(); ?>;
            --overlay-duration: <?php echo $duration ?>;
            --overlay-delay: <?php echo $delay ?>;
            --overlay-opacity: <?php echo $opacity ?>;
        }

        <?php echo $prefix ?>
        a {
            color: <?php echo '#'.get_header_textcolor(); ?>;
        }

        a:hover {
            color: #1a1715;
        }

        <?php echo $prefix ?>
        a[data-type="definition"] {
            background-color: <?php echo '#'.get_header_textcolor(); ?> !important;
        }

        <?php echo $prefix ?>
        *.colored {
            color: <?php echo '#'.get_header_textcolor().$important; ?>;
        }

        <?php echo $prefix ?>
        *.svg_colored {
            fill: <?php echo '#'.get_header_textcolor(); ?>;
        }

        <?php echo $prefix ?>
        .bg_colored {
            background-color: <?php echo '#'.get_header_textcolor(); ?>;
        }

        <?php echo $prefix ?>
        .border_colored {
            border-color: <?php echo '#'.get_header_textcolor(); ?>;
        }

        <?php echo $prefix ?>
        .hover_colored:hover {
            color: <?php echo '#'.get_header_textcolor(); ?>;
        }

        <?php echo $prefix ?>
        .hover_bg_colored:hover {
            background-color: <?php echo '#'.get_header_textcolor(); ?> !important;
        }

        @media (min-width: 769px) {
            body.home .overlay:before,
            body.error404 .overlay:before {
                background-image: url('<?php echo $homeTopBbackgroundImageUrlDesktop; ?>');
            }
        }

        @media (max-width: 768px) {
            body.home .overlay:before,
            body.error404 .overlay:before {
                background-image: url('<?php echo $homeTopBackgroundImageUrlMobile; ?>');
            }
        }
    </style>
    <?php
}

add_action('wp_head', 'expoCustomizeCss');
add_action('admin_head', 'expoCustomizeCss');

/**
 * Parse content for définition and replace with modal link
 */
add_filter('the_content', 'createModalDefinition', 20);
function createModalDefinition($content)
{
    if (!is_admin()) {

        $dom = new DOMDocument;
        libxml_use_internal_errors(true);
        $dom->loadHTML('<?xml encoding="utf-8" ?>' . $content);
        $links       = $dom->getElementsByTagName('a');
        $definitions = [];
        foreach ($links as $link) {
            $dataType = $link->getAttribute('data-type');
            if ($dataType == 'definition') {
                $definitions[] = $link;
            }
        }

        if ($definitions) {
            foreach ($definitions as $definition) {
                $content = str_replace($definition->getAttribute('href'), 'javascript:void(0)', $content);
            }

            $modal = '
                <div class="modal" id="modal_definition" tabindex="-1" role="dialog">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title colored"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="row">
                          <div class="col-10 offset-1 col-md-8 offset-md-2">
                              <div class="modal-body">
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
            ';

            global $post;
            foreach ($definitions as $definition) {
                $id   = $definition->getAttribute('data-id');
                $post = get_post($id);
                if ($post) {
                    setup_postdata($post);
                    $content .= '<div class="definition_placeholder" data-id="' . $id . '" data-title="' . get_the_title() . '">' . apply_filters('the_content', get_the_content()) . '</div>';
                }
            }
            wp_reset_postdata();

            $content .= $modal;
        }
    }

    return $content;
}

function wrapBlock($block_content, $block)
{
    if (is_admin()) {
        return $block_content;
    }

    if (!is_singular('chapitre') && $block['blockName'] == 'expo/extract') {
        return $block_content;
    }

    $blocks = [
        'expo/carrousel',
        'expo/extract',
        'expo/iiif',
        'core/gallery',
        'core/html',
        'core/embed'
    ];
    if (!in_array($block['blockName'], $blocks)) {
        return $block_content;
    }

    if ($block['blockName'] == 'core/html' || $block['blockName'] == 'core/embed') {

        $ratio = '1by1';
        if (preg_match_all('/(youtube|dailymotion|vimeo|tiktok)/', $block_content)) {
            $ratio = '16by9';
        }

        return str_replace([
            '<iframe',
            '</iframe>'
        ], [
            '<div class="embed-responsive embed-responsive-' . $ratio . '">' . '<iframe class="embed-responsive-item"',
            '</iframe></div>'
        ], $block_content);
    }

    $output = '
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-12">
    ';

    switch ($block['blockName']) {
        case 'expo/carrousel':
            $output .= '<div class="swiper_nomargin_cont">' . $block_content . '</div>';
            break;
        case 'expo/extract':
            $output .= '<div class="row">';
            $output .= '<div class="col-12 col-md-8 offset-md-2 no-mobile-padding">' . $block_content . '</div>';
            $output .= '</div>';
            break;
        case 'expo/iiif':
            $output .= '<div class="row">';
            $output .= '<div class="col-12 col-md-8 offset-md-2 no-mobile-padding">' . $block_content . '</div>';
            $output .= '</div>';
            break;
        case 'core/gallery':
            $borne = get_query_var('borne', '');
            $class = 'col-12';
            if (!$borne) {
                $class .= ' col-md-8 offset-md-2';
            }
            $output .= '<div class="row">';
            $output .= '<div class="' . $class . ' no-mobile-padding">' . $block_content . '</div>';
            $output .= '</div>';
            break;
    }

    $output .= '
        </div>
    </div>
    <div class="row">
        <div class="col-12 offset-0 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">';

    return $output;
}

add_filter('render_block', 'wrapBlock', 10, 3);

/**
 * Add "chapter #" over title
 *
 * @param $block_content
 * @param $block
 *
 * @return string
 */
function renderChapitreTitleBlock($block_content, $block)
{
    if (is_admin()) {
        return $block_content;
    }

    if ($block['blockName'] != 'core/heading' || isset($block['attrs']['level'])) {
        return $block_content;
    }

    $chapitres = get_query_var('header-chapitre', false);
    if ($chapitres) {
        foreach ($chapitres as $k => $chapitre) {
            if ($chapitre == trim(strip_tags($block['innerHTML']))) {
                $number        = '<span class="chapter_number">Chapitre ' . ($k + 1) . '</span>';
                $block_content = str_replace($chapitre, $number . $chapitre, $block_content);
                $block_content = '
                    </div>
                </div>
                <div class="row no-gutters chapter_title_cont">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 col-md-8 offset-md-2 no-mobile-padding">
                                ' . $block_content . '                    
                            </div>                
                        </div>            
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 offset-0 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">';
            }
        }
    }

    return $block_content;
}

add_filter('render_block', 'renderChapitreTitleBlock', 10, 3);

/**
 * Create a custom category for our blocks
 *
 * @param $categories
 * @param $post
 *
 * @return array
 */
function createBlockCategory($categories, $post)
{
    $category_slugs = wp_list_pluck($categories, 'slug');
    if (!in_array('expo', $category_slugs, true)) {
        $categories = array_merge($categories, array(
            array(
                'slug'  => 'expo',
                'title' => 'Expositions'
            )
        ));
    }

    return $categories;
}

add_filter('block_categories_all', 'createBlockCategory', 10, 2);

/**
 * Create a callback URL for the video convert API
 */
function convertVideoCallback()
{
    if (isset($_GET['video_convert_callback'])) {
        $job        = file_get_contents('php://input');
        $jobAsArray = json_decode($job, true);
        if (!empty($jobAsArray['id'])) {
            if ($jobAsArray['status']['code'] == 'completed') {
                $fileUrl    = $jobAsArray["output"][0]["uri"];
                $fileName   = $jobAsArray["output"][0]["filename"];
                $noExtFile  = pathinfo($fileUrl, PATHINFO_FILENAME);
                $newName    = $noExtFile . '_resized';
                $newName    = str_replace($noExtFile, $newName, $fileName);
                $upload_dir = wp_upload_dir();
                $options    = array(
                    CURLOPT_FILE    => fopen($upload_dir['basedir'] . '/' . $newName, 'w'),
                    CURLOPT_TIMEOUT => 28800,
                    CURLOPT_URL     => $fileUrl
                );

                $ch = curl_init();
                curl_setopt_array($ch, $options);
                curl_exec($ch);
                curl_close($ch);
                delete_transient('convert_job_id');
            }
        }
    }
}

add_filter('init', 'convertVideoCallback', 10, 0);

/**
 * Add a wrapper around credits links
 *
 * @param $values
 *
 * @return mixed
 */
function wrapCreditsOptions($values)
{
    foreach ($values as $k => $content) {
        preg_replace('/(<a(.*)a>)/', '<div class="btn colored">${1}</div>', $content);
    }

    return $values;
}

add_filter('option_footer_options', 'wrapCreditsOptions');

/**
 * Reorder posts query by start date
 *
 * @param WP_Query $query
 *
 */
function reorderAgendaPosts($query)
{
    if (!is_admin() && $query->is_home() && $query->is_main_query()) {
        $query->set('ignore_sticky_posts', 1);
        $query->set('meta_key', 'post_start');
        $query->set('orderby', 'meta_value_num');
        $query->set('meta_type', 'NUMERIC');
        $query->set('order', 'ASC');
        $query->set('meta_query', [
            'relation' => 'OR',
            array(
                'key'     => 'post_end',
                'value'   => strtotime("now"),
                'compare' => '>=',
                'type'    => 'NUMERIC'
            ),
            array(
                'relation' => 'AND',
                array(
                    'key'     => 'post_end',
                    'compare' => 'NOT EXISTS',
                    'type'    => 'NUMERIC'
                ),
                array(
                    'key'     => 'post_start',
                    'compare' => '>=',
                    'value'   => strtotime("now"),
                    'type'    => 'NUMERIC'
                )
            ),
        ]);

    }
}

add_action('pre_get_posts', 'reorderAgendaPosts');

add_filter('auto_update_plugin', '__return_false');
add_filter('auto_update_theme', '__return_false');