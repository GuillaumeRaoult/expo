<?php /* Template Name: Agenda borne */ ?>

<?php
set_query_var('header', 'no-header');
get_header();
?>
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-sm-4">
                <div class="sidebar" style="background-image:url(<?php the_post_thumbnail_url() ?>)">
					<?php
					$title    = get_post_meta(get_the_ID(), 'agenda_borne_title', true);
					$subtitle = get_post_meta(get_the_ID(), 'agenda_borne_subtitle', true);

                    if ($title) {
                        ?>
                        <h1><?php echo $title ?></h1>
                        <?php
                    }
					if ($subtitle) {
						?>
                        <p class="banner bg_colored"><?php echo $subtitle ?></p>
						<?php
					}
					?>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="agenda_cont">
					<?php
					$posts = get_posts(array(
						'post_type'           => 'post',
						'posts_per_page'      => '-1',
						'suppress_filters'    => true,
						'ignore_sticky_posts' => 1,
						'meta_key'            => 'post_start',
						'orderby'             => 'meta_value_num',
						'meta_type'           => 'NUMERIC',
						'order'               => 'ASC',
						'meta_query'          => array(
							'relation' => 'OR',
							array(
								'key'     => 'post_end',
								'value'   => strtotime("now"),
								'compare' => '>=',
								'type'    => 'NUMERIC'
							),
							array(
								'relation' => 'AND',
								array(
									'key'     => 'post_start',
									'compare' => '>=',
									'value'   => strtotime("now"),
									'type'    => 'NUMERIC'
								),
								array(
									'key'     => 'post_end',
									'compare' => 'NOT EXISTS',
									'type'    => 'NUMERIC'
								)
							),
						)
					));
					if ($posts) {
						?>
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
								<?php
								global $post;
								foreach ($posts as $post) {
									setup_postdata($post);
									?>
                                    <div class="swiper-slide"
                                         style="background-image:url(<?php the_post_thumbnail_url() ?>)">
                                        <div class="actu_content">
                                            <div class="row">
                                                <div class="col-3">
                                                    <div class="meta_cont bg_colored">
                                                        <div class="meta">
															<?php
															$start = get_post_meta(get_the_ID(), 'post_start', true);
															$end   = get_post_meta(get_the_ID(), 'post_end', true);
															if ($start || $end) {
																?>
                                                                <p class="start">
																	<?php
																	if ($end) {
																		?>
                                                                        DU
																		<?php
																	}
																	?>
                                                                    <span><?php echo date('d/m', $start) ?></span>
                                                                </p>
																<?php
																if ($end) {
																	?>
                                                                    <p class="end">
                                                                        AU <span><?php echo date('d/m', $end) ?></span>
                                                                    </p>
																	<?php
																}
															}
															?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-9">
                                                    <div class="title_cont">
														<?php
														$categories = get_the_category();
														if ($categories) {
															$cat      = $categories[0];
															$category = $cat->name;
															?>
                                                            <p class="category"><?php echo $cat->name ?></p>
															<?php
														}
														?>
                                                        <h2><?php the_title() ?></h2>
														<?php
														$place = get_post_meta(get_the_ID(), 'post_place', true);
														if ($place) {
															?>
                                                            <p class="place"><span
                                                                        class="dashicons dashicons-location"></span><?php echo $place ?>
                                                            </p>
															<?php
														}
														?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<?php
								}
								?>
                            </div>
                        </div>
						<?php
						wp_reset_postdata();
					} else {
						?>
                        <div class="no_posts">
                            <h2>Aucun évènement à venir</h2>
                        </div>
						<?php
					}
					?>
                </div>
                <div class="page_content">
					<?php the_content() ?>
                </div>
            </div>
        </div>
        <div class="row">

        </div>
    </div>
<?php
set_query_var('footer', 'no-footer');
get_footer();
