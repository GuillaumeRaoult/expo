<?php

$desktopThumbnail = get_the_post_thumbnail_url(get_the_ID(), 'header');
$mobileThumbnail  = get_the_post_thumbnail_url(get_the_ID(), 'large');
$title            = get_the_title();
$subtitle         = get_query_var('subtitle');
$pageForPosts     = get_option('page_for_posts');

//Blog index
if (is_home()) {
    $desktopThumbnail = get_the_post_thumbnail_url($pageForPosts, 'header');
    $mobileThumbnail  = get_the_post_thumbnail_url($pageForPosts, 'large');
    $title            = get_the_title($pageForPosts);
}

//Default
if (!$desktopThumbnail || !$mobileThumbnail) {
    $homeBackgroundImageUrl = get_theme_mod('expo_home_background_image');
    $homeBackgroundImageUrl = attachment_url_to_postid($homeBackgroundImageUrl);
    $desktopThumbnail       = wp_get_attachment_image_url($homeBackgroundImageUrl, 'header');
    $mobileThumbnail        = wp_get_attachment_image_url($homeBackgroundImageUrl, 'large');
}

//Replace mobile thumbnail if specified in meta
if (get_post_meta(get_the_ID(), 'mobile_image_thumbnail', true)) {
    $mobileUrl       = get_post_meta(get_the_ID(), 'mobile_image_thumbnail', true);
    $attachmentId    = attachment_url_to_postid($mobileUrl);
    $mobileThumbnail = wp_get_attachment_image_url($attachmentId, 'large');
}

if (is_404()) {
    $title = 'Page non trouvée';
}

$borne    = get_query_var('borne', '');
if ($borne) {
    $hideHeader = get_post_meta(get_the_ID(), 'hide_borne_header', true);
    if ($hideHeader == 'on') {
        return;
    }
}
?>

<style type="text/css">
    @media (min-width: 769px) {
        .overlay:before {
            background-image: url(<?php echo $desktopThumbnail ?>);
        }
    }

    @media (max-width: 768px) {
        .overlay:before {
            background-image: url(<?php echo $mobileThumbnail ?>);
        }
    }
</style>
<div class="container-fluid overlay_cont">
    <div class="row overlay">
        <div class="col-10 offset-1 col-sm-8 offset-sm-2 my-auto titles">
            <?php
            if ($subtitle) {
                ?>
                <p class="subtitle"><?php echo $subtitle ?></p>
                <?php
            }
            ?>
            <h1><?php echo $title ?></h1>
        </div>
    </div>
</div>