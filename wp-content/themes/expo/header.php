<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Expo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<!--    <a class="skip-link screen-reader-text" href="#primary">-->
<?php //esc_html_e('Skip to content', 'expo'); ?><!--</a>-->

<?php
$isHeader = get_query_var('header');
$borne    = get_query_var('borne', '');
if ($borne) {
    $hideHeader = get_post_meta(get_the_ID(), 'hide_borne_header', true);
    if ($hideHeader == 'on') {
        return;
    }
}

if (!$isHeader || $isHeader != 'no-header') {
    ?>
    <nav class="nav nav_menu">
        <?php
        wp_nav_menu(array(
            'theme_location' => 'menu-principal',
            'depth'          => 3,
            'menu_class'     => 'nav-list',
            'container'      => false,
            'walker'         => new Bootstrap_Collapse_NavWalker(),
            'fallback_cb'    => false
        ));
        ?>
    </nav>
    <?php
    if (get_query_var('header-chapitre')) {
        ?>
        <nav class="nav nav_chapitre">
            <ul class="nav-list">
                <li id="menu-item-0"
                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-14 nav-item">
                    <ul class="sub-menu">
                        <li>
                            <a href="javascript:void(0)" class="link-item colored" data-index="0">
                                Sommaire
                            </a>
                        </li>
                    </ul>
                </li>
                <?php
                foreach (get_query_var('header-chapitre') as $k => $chapitre) {
                    $index = $k + 1;
                    ?>
                    <li id="menu-item-<?php echo $index ?>"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-14 nav-item">
                        <span>Chapitre <?php echo $index ?></span>
                        <ul class="sub-menu">
                            <li>
                                <a href="javascript:void(0)" class="link-item colored"
                                   data-index="<?php echo $index ?>">
                                    <?php echo $chapitre ?>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </nav>
        <?php
    }

    ?>
    <header id="masthead" class="site-header <?php echo get_query_var('header-color') ?>">
        <div class="container-fluid">
            <?php
            if (is_front_page()) {
                ?>
                <div class="row" id="rosalis_header">
                    <div class="col-12">
                        <div class="float-left rosalis">
                            <a href="https://rosalis.bibliotheque.toulouse.fr/" target="_blank">
                                <img alt="Rosalis, Bibliothèque numérique patrimoniale de Toulouse"
                                     src="<?php echo get_template_directory_uri() ?>/assets/images/Logo Rosalis_Blanc.svg"/>
                                <span>Bibliothèque numérique patrimoniale de Toulouse</span>
                            </a>
                        </div>
                        <div class="float-right title">
                            <a href="https://www.bibliotheque.toulouse.fr/" target="_blank">
                                <img alt="Bibliothèque de Toulouse"
                                     src="<?php echo get_template_directory_uri() ?>/assets/images/Logo_Bibliotheque_Blanc.png"/>
                            </a>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="row align-items-center" id="menu_header">
                <div class="col-4 col-sm-4">
                    <div class="home_link">
                        <?php
                        if (!$borne) {
                        ?>
                        <a href="<?php echo esc_url(home_url('/')); ?>"
                           rel="home">
                            <?php
                            } else {
                            ?>
                            <div>
                                <?php
                                }
                                echo file_get_contents(get_template_directory() . '/assets/images/Rosace_Rosalis.svg', false, null);
                                ?>
                                <span>
                            <?php echo substr(get_bloginfo('name'), 0, 1); ?>
                            </span>
                                <?php
                                if (!$borne) {
                                ?>
                        </a>
                        <?php
                        } else {
                        ?>
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <?php
                if (!get_query_var('borne', '')) {
                    ?>
                    <div class="menu_cont">
                        <a href="javascript:void(0)" class="nav-toggle" aria-controls="primary-menu"
                           aria-expanded="false">
                            <p class="nav_icon">
                                <span></span>
                            </p>
                            <p class="nav_text">MENU</p>
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="col-12 d-none col-lg-4 d-lg-block">
                <?php
                if (get_query_var('header-title')) {
                    ?>
                    <div class="header-title">
                        <?php echo get_query_var('header-title') ?>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="col-8 col-lg-4">
                <?php
                if (get_query_var('header-chapitre')) {
                    ?>
                    <div class="chapitre_cont">
                        <a href="javascript:void(0)" class="chapitre-nav-toggle" aria-controls="chapitre-menu"
                           aria-expanded="false">
                            <p class="nav_text">
                                <span>Sommaire</span>
                                <?php
                                foreach (get_query_var('header-chapitre') as $k => $chapitre) {
                                    ?>
                                    <span><?php echo $chapitre ?></span>
                                    <?php
                                }
                                ?>
                            </p>
                            <p class="caret">
                                <span class="dashicons dashicons-arrow-down-alt2"></span>
                            </p>
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        </div>
    </header>
    <?php
}
?>

