<?php
/**
 * Created by PhpStorm.
 * User: guillaume
 * Date: 18/08/20
 * Time: 16:14
 */

/**
 * Return pages IDs using a specific template
 *
 * @param $template
 *
 * @return int[]|WP_Post[]
 */
function getPageByTemplate($template)
{
    $args = [
        'post_type'  => 'page',
        'fields'     => 'ids',
        'nopaging'   => true,
        'meta_key'   => '_wp_page_template',
        'meta_value' => $template
    ];

    return get_posts($args);
}

function shareButtons()
{
    if (!get_query_var('borne', '')) {
        ?>
        <a onClick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>', 'pop', 'width=600, height=400, scrollbars=no');"
           href="javascript:void(0);" title="Facebook"
           class="border_colored hover_bg_colored">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15pt"
                 height="15pt"
                 viewBox="0 0 15 15" version="1.1">
                <g id="surface1">
                    <path style="stroke:none;fill-rule:nonzero;fill-opacity:1;" class="svg_colored"
                          d="M 3.953125 7.988281 L 5.597656 7.988281 L 5.597656 14.757812 C 5.597656 14.890625 5.707031 15 5.839844 15 L 8.628906 15 C 8.761719 15 8.871094 14.890625 8.871094 14.757812 L 8.871094 8.019531 L 10.757812 8.019531 C 10.882812 8.019531 10.984375 7.929688 11 7.804688 L 11.285156 5.316406 C 11.292969 5.246094 11.273438 5.175781 11.226562 5.125 C 11.179688 5.074219 11.117188 5.046875 11.046875 5.046875 L 8.871094 5.046875 L 8.871094 3.484375 C 8.871094 3.011719 9.121094 2.773438 9.625 2.773438 C 9.695312 2.773438 11.046875 2.773438 11.046875 2.773438 C 11.179688 2.773438 11.289062 2.664062 11.289062 2.53125 L 11.289062 0.242188 C 11.289062 0.109375 11.179688 0 11.046875 0 L 9.085938 0 C 9.070312 0 9.039062 0 8.996094 0 C 8.65625 0 7.472656 0.0664062 6.535156 0.925781 C 5.5 1.878906 5.644531 3.019531 5.679688 3.21875 L 5.679688 5.042969 L 3.953125 5.042969 C 3.820312 5.042969 3.710938 5.152344 3.710938 5.285156 L 3.710938 7.746094 C 3.710938 7.878906 3.820312 7.988281 3.953125 7.988281 Z M 3.953125 7.988281 "/>
                </g>
            </svg>
        </a>
        <a onclick="window.open('https://twitter.com/intent/tweet?text=<?php the_permalink() ?>', 'pop', 'width=600, height=400, scrollbars=no');"
           href="javascript:void(0);" title="Twitter"
           class="border_colored hover_bg_colored">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15pt"
                 height="12pt"
                 viewBox="0 0 15 12" version="1.1">
                <g id="surface1">
                    <path style="stroke:none;fill-rule:nonzero;fill-opacity:1;" class="svg_colored"
                          d="M 4.734375 11.960938 C 10.359375 11.960938 13.433594 7.378906 13.433594 3.40625 C 13.433594 3.273438 13.429688 3.144531 13.425781 3.015625 C 14.023438 2.589844 14.539062 2.0625 14.949219 1.460938 C 14.402344 1.699219 13.8125 1.859375 13.195312 1.933594 C 13.824219 1.558594 14.308594 0.972656 14.539062 0.269531 C 13.949219 0.613281 13.292969 0.863281 12.597656 1 C 12.039062 0.414062 11.246094 0.0507812 10.367188 0.0507812 C 8.679688 0.0507812 7.308594 1.394531 7.308594 3.054688 C 7.308594 3.292969 7.335938 3.519531 7.386719 3.742188 C 4.847656 3.617188 2.59375 2.417969 1.085938 0.601562 C 0.824219 1.042969 0.671875 1.558594 0.671875 2.109375 C 0.671875 3.152344 1.210938 4.074219 2.035156 4.613281 C 1.53125 4.597656 1.0625 4.464844 0.648438 4.238281 C 0.648438 4.25 0.648438 4.261719 0.648438 4.277344 C 0.648438 5.734375 1.703125 6.949219 3.101562 7.222656 C 2.84375 7.292969 2.574219 7.328125 2.292969 7.328125 C 2.097656 7.328125 1.90625 7.3125 1.71875 7.277344 C 2.109375 8.46875 3.238281 9.339844 4.574219 9.363281 C 3.53125 10.171875 2.210938 10.652344 0.777344 10.652344 C 0.53125 10.652344 0.289062 10.636719 0.0507812 10.609375 C 1.402344 11.460938 3.007812 11.960938 4.734375 11.960938 "/>
                </g>
            </svg>
        </a>
        <?php
    }
}

/**
 * Attempt to get a shorter video from a video URL or create one if not available
 *
 * @param $videoUrl
 *
 * @return string
 */
function getShortVideo($videoUrl)
{
    if (WP_DEBUG) {
        return $videoUrl;
    }

    $transient_name = 'convert_job_id';

    $noExtFile               = pathinfo($videoUrl, PATHINFO_FILENAME);
    $ext                     = pathinfo($videoUrl, PATHINFO_EXTENSION);
    $shortName               = $noExtFile . '_resized';
    $baseUploadDir           = wp_get_upload_dir()['baseurl'] . '/';
    $opts['http']['timeout'] = 0.5;
    $defaultOptions          = stream_context_get_options(stream_context_get_default());
    stream_context_set_default($opts);
    $headers = get_headers($baseUploadDir . $shortName . '.' . $ext);
    stream_context_set_default($defaultOptions);
    $fileExists = stripos($headers[0], "200 OK") ? true : false;

    if ($fileExists) {
        return $baseUploadDir . $shortName . '.' . $ext;
    }

    if (get_transient($transient_name)) {
        return $videoUrl;
    }

    $callbackUrl  = get_site_url() . '?video_convert_callback=1';
    $curl         = curl_init();
    $json_request = '{
        "input": [{
            "type": "remote",
            "source": "' . $videoUrl . '"
        }],
        "conversion": [{
            "target": "mp4",
            "options": {
                "remove_audio": true,
                "start": "00:00:00",
                "end": "00:00:10",
                "width": 200,
                "resize_handling": "do_not_upscale"
            }
        }],
        "callback": "' . $callbackUrl . '",
        "notify_status": true
    }';

    curl_setopt_array($curl, array(
        CURLOPT_URL            => "https://api2.online-convert.com/jobs",
        CURLOPT_CUSTOMREQUEST  => "POST",
        CURLOPT_POSTFIELDS     => $json_request,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTPHEADER     => array(
            "cache-control: no-cache",
            "x-oc-api-key: 5d7c2cc3af1e60e3163159b7fdf8c226",
            'Content-Type: application/json',
        ),
    ));

    $content = curl_exec($curl);
    $decoded = json_decode($content, true);
    if (!empty($decoded['id'])) {
        set_transient($transient_name, $decoded['id'], 12 * HOUR_IN_SECONDS);
        if (is_user_logged_in() && get_current_user_id() == 1) {
            print_r($decoded['id']);
        }
    }

    curl_close($curl);

    return $videoUrl;
}

function getBorneNextPreviousLink($prevPost, $nextPost, $post)
{
    global $borneController;

    $currentVar   = get_query_var('borne', '');
    $originalLink = get_permalink($post);
    $originalLink = str_replace($currentVar . '/', '', $originalLink);

    $siblings = $borneController->getGroupUrls($originalLink);

    set_query_var('borne', '');
    if ($nextPost) {
        $nextPostLink = get_permalink($nextPost);
        if (!in_array($nextPostLink, $siblings)) {
            $nextPost = false;
        }
    }

    if ($prevPost) {
        $prevPostLink = get_permalink($prevPost);
        if (!in_array($prevPostLink, $siblings)) {
            $prevPost = false;
        }
    }
    set_query_var('borne', $currentVar);

    return [
        $nextPost,
        $prevPost
    ];
}

function getBorneParcoursLink($post, $oeuvreSubtitle)
{
    global $borneController;

    $parcours       = getPageByTemplate('template-parcours.php');
    if ($parcours) {
        $currentVar   = get_query_var('borne', '');
        set_query_var('borne', '');
        $permalink = get_permalink($parcours[0]);
        $currentPostLink = get_permalink($post);
        set_query_var('borne', $currentVar);
        $siblings = $borneController->getGroupUrls($permalink);
        if (in_array($currentPostLink, $siblings)) {
            return '<a href="' . get_permalink($parcours[0]) . '"><span class="dashicons dashicons-arrow-left-alt"></span> ' . $oeuvreSubtitle . '</a>';
        }

    }
    return $oeuvreSubtitle;
}