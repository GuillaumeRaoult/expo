<?php
/**
 * Options du site
 */
add_action('cmb2_admin_init', 'cmb2_expo_metaboxes_options');
function cmb2_expo_metaboxes_options()
{
    $cmb_options = new_cmb2_box(array(
        'id'           => 'expo_option_metabox',
        'title'        => esc_html__('Options de l\'expo', 'expo'),
        'object_types' => array('options-page'),
        'option_key'   => 'partenaires_options',
        'icon_url'     => 'dashicons-info',
        'position'     => 90,
        'display_cb'   => 'expo_options_display_with_tabs',
        'tab_group'    => 'partenaires_options',
        'tab_title'    => 'Partenaires',
    ));

    $group_field_id = $cmb_options->add_field(array(
        'id'          => 'options_partenaires_group',
        'type'        => 'group',
        'description' => __('Les partenaires du site', 'cmb2'),
        'options'     => array(
            'group_title'   => __('Partenaire {#}', 'cmb2'),
            'add_button'    => __('Ajouter un autre partenaire', 'cmb2'),
            'remove_button' => __('Retirer le partenaire', 'cmb2'),
            'sortable'      => true,
        ),
    ));

    $cmb_options->add_group_field($group_field_id, array(
        'name' => 'URL',
        'id'   => 'partenaire_url',
        'type' => 'text_url',
    ));

    $cmb_options->add_group_field($group_field_id, array(
        'name'         => 'Image',
        'desc'         => '',
        'id'           => 'partenaire_image',
        'type'         => 'file',
        'options'      => array(
            'url' => false,
        ),
        'query_args'   => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png',
            ),
        ),
        'preview_size' => array(
            200,
            200
        ),
    ));

    $cmb_options = new_cmb2_box(array(
        'id'           => 'expo_option_footer_metabox',
        'title'        => esc_html__('Crédits', 'expo'),
        'object_types' => array('options-page'),
        'option_key'   => 'footer_options',
        'icon_url'     => 'dashicons-info',
        'position'     => 100,
        'display_cb'   => 'expo_options_display_with_tabs',
        'parent_slug'  => 'partenaires_options',
        'tab_group'    => 'partenaires_options',
        'tab_title'    => 'Crédits'
    ));

    $cmb_options->add_field(array(
        'name'    => 'Champ gauche',
        'desc'    => '',
        'id'      => 'footer_left_side_content',
        'type'    => 'wysiwyg',
        'options' => array(
            'wpautop'       => true,
            'media_buttons' => false,
            'tinymce'       => array(
                'height'        => 200,
                'toolbar1'      => 'formatselect, link, unlink',
                'block_formats' => 'Paragraph=p; Header 3=h3'
            )
        ),
    ));

    $cmb_options->add_field(array(
        'name'    => 'Champ droite',
        'desc'    => '',
        'id'      => 'footer_right_side_content',
        'type'    => 'wysiwyg',
        'options' => array(
            'wpautop'       => true,
            'media_buttons' => false,
            'tinymce'       => array(
                'height'        => 200,
                'toolbar1'      => 'formatselect, link, unlink',
                'block_formats' => 'Paragraph=p; Header 3=h3'
            )
        ),
    ));

    $cmb_options = new_cmb2_box(array(
        'id'           => 'expo_option_general_metabox',
        'title'        => esc_html__('Divers', 'expo'),
        'object_types' => array('options-page'),
        'option_key'   => 'general_options',
        'icon_url'     => 'dashicons-info',
        'position'     => 100,
        'display_cb'   => 'expo_options_display_with_tabs',
        'parent_slug'  => 'partenaires_options',
        'tab_group'    => 'partenaires_options',
        'tab_title'    => 'Divers'
    ));

    $cmb_options->add_field( array(
        'name'    => 'Titre principal des oeuvres',
        'desc'    => 'Titre affiché dans la page de chaque oeuvre',
        'default' => 'Parcours musical',
        'id'      => 'oeuvres_subtitle',
        'type'    => 'text',
    ) );
}

function expo_options_display_with_tabs($cmb_options)
{
    $tabs = expo_options_page_tabs($cmb_options);
    ?>
    <div class="wrap cmb2-options-page option-<?php echo $cmb_options->option_key; ?>">
        <?php if (get_admin_page_title()) : ?>
            <h2><?php echo wp_kses_post(get_admin_page_title()); ?></h2>
        <?php endif; ?>
        <h2 class="nav-tab-wrapper">
            <?php foreach ($tabs as $option_key => $tab_title) : ?>
                <a class="nav-tab<?php if (isset($_GET['page']) && $option_key === $_GET['page']) : ?> nav-tab-active<?php endif; ?>"
                   href="<?php menu_page_url($option_key); ?>"><?php echo wp_kses_post($tab_title); ?></a>
            <?php endforeach; ?>
        </h2>
        <form class="cmb-form" action="<?php echo esc_url(admin_url('admin-post.php')); ?>" method="POST"
              id="<?php echo $cmb_options->cmb->cmb_id; ?>" enctype="multipart/form-data"
              encoding="multipart/form-data">
            <input type="hidden" name="action" value="<?php echo esc_attr($cmb_options->option_key); ?>">
            <?php $cmb_options->options_page_metabox(); ?>
            <?php submit_button(esc_attr($cmb_options->cmb->prop('save_button')), 'primary', 'submit-cmb'); ?>
        </form>
    </div>
    <?php
}

function expo_options_page_tabs($cmb_options)
{
    $tab_group = $cmb_options->cmb->prop('tab_group');
    $tabs      = array();

    foreach (CMB2_Boxes::get_all() as $cmb_id => $cmb) {
        if ($tab_group === $cmb->prop('tab_group')) {
            $tabs[$cmb->options_page_keys()[0]] = $cmb->prop('tab_title') ? $cmb->prop('tab_title') : $cmb->prop('title');
        }
    }

    return $tabs;
}

/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 *
 * @param  string $key     Options array key
 * @param  mixed  $default Optional default value
 *
 * @return mixed           Option value
 */
function partenairesGetOption($key = '', $default = false)
{
    if (function_exists('cmb2_get_option')) {
        return cmb2_get_option('partenaires_options', $key, $default);
    }

    $opts = get_option('partenaires_options', $default);
    $val  = $default;
    if ('all' == $key) {
        $val = $opts;
    } elseif (is_array($opts) && array_key_exists($key, $opts) && false !== $opts[$key]) {
        $val = $opts[$key];
    }

    return $val;
}

/**
 * Wrapper function around cmb2_get_option
 *
 * @since  0.1.0
 *
 * @param  string $key     Options array key
 * @param  mixed  $default Optional default value
 *
 * @return mixed           Option value
 */
function footerGetOption($key = '', $default = false)
{
    if (function_exists('cmb2_get_option')) {
        return cmb2_get_option('footer_options', $key, $default);
    }

    $opts = get_option('footer_options', $default);
    $val  = $default;
    if ('all' == $key) {
        $val = $opts;
    } elseif (is_array($opts) && array_key_exists($key, $opts) && false !== $opts[$key]) {
        $val = $opts[$key];
    }

    return $val;
}