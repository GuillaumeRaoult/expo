<?php
/**
 * Template part for displaying meta in post/agenda page
 *
 * @package Expo
 */

?>
    <div class="meta_cont">
        <?php
        $categories = get_the_category();
        if ($categories) {
            $cat      = $categories[0];
            $category = $cat->name;
            ?>
            <p class="category bg_colored"><?php echo $cat->name ?></p>
            <?php
        }

        $place = get_post_meta(get_the_ID(), 'post_place', true);
        $start = get_post_meta(get_the_ID(), 'post_start', true);
        $end   = get_post_meta(get_the_ID(), 'post_end', true);

        if ($start || $end) {
            ?>
            <p class="date">
                <span class="dashicons dashicons-calendar-alt colored"></span>
                <?php
                echo date('d/m/Y', $start);
                if ($end) {
                    if ($start) {
                        echo ' - ';
                    }
                    echo date('d/m/Y', $end);
                }
                ?>
            </p>
            <?php
        }

        if ($place) {
            ?>
            <p class="place"><span
                        class="dashicons dashicons-location colored"></span><?php echo $place ?></p>
            <?php
        }
        ?>
    </div>
<?php
