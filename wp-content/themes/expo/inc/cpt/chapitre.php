<?php
/**
 * Chapitre custom post type
 *
 * @package Expo
 */

$labels = array(
    'name'               => _x('Chapitres', 'expo'),
    'singular_name'      => _x('Chapitre', 'expo'),
    'menu_name'          => _x('Chapitres', 'expo'),
    'name_admin_bar'     => _x('Chapitre', 'expo'),
    'add_new'            => _x('Ajouter un chapitre', 'expo'),
    'add_new_item'       => __('Ajouter un nouveau chapitre', 'expo'),
    'new_item'           => __('Nouveau chapitre', 'expo'),
    'edit_item'          => __('Modifier le chapitre', 'expo'),
    'view_item'          => __('Voir le chapitre', 'expo'),
    'all_items'          => __('Tous les chapitres', 'expo'),
    'search_items'       => __('Chercher', 'expo'),
    'parent_item_colon'  => __('Chapitre parent:', 'expo'),
    'not_found'          => __('Aucun chapitre trouvé.', 'expo'),
    'not_found_in_trash' => __('Aucun chapitre dans la corbeille.', 'expo'),
);

$args = array(
    'labels'              => $labels,
    'public'              => true,
    'show_ui'             => true,
    'hierarchical'        => false,
    'exclude_from_search' => false,
    'has_archive'         => false,
    'query_var'           => true,
    'can_export'          => true,
    'show_in_rest'        => true,
    'rewrite'             => true,
    'capability_type'     => 'post',
    'label'               => 'expo Chapitres',
    'menu_icon'           => 'dashicons-edit-page',
    'supports'            => array(
        'author',
        'title',
        'thumbnail',
        'editor'
    )
);

register_post_type('chapitre', $args);