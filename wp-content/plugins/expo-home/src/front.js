(function ($) {

    const expandChaptersButton = function() {
        const button = document.querySelector('.more_chapters');
        if (!!button) {
            button.addEventListener('click', event => {
                document.querySelector('.expand_chapters_cont').classList.toggle("expanded");
                button.parentNode.remove();
            });
        }
    };

    const createSliderOeuvres = function () {
        const swiperOeuvres = new Swiper('.oeuvres .swiper-container', {
            loop                    : false,
            spaceBetween            : 20,
            slidesPerView           : 1,
            watchOverflow           : true,
            centerInsufficientSlides: true,
            breakpoints             : {
                768: {
                    slidesPerView: 3
                },
                1024: {
                    slidesPerView: 5
                }
            },
            navigation              : {
                nextEl: '.swiper-next-oeuvre',
                prevEl: '.swiper-prev-oeuvre',
            }
        });
    };

    const createSliderTemoignages = function () {
        const swiperTemoignages = new Swiper('.temoignages .swiper-container', {
            loop                    : true,
            spaceBetween            : 10,
            slidesPerView           : 1,
            watchOverflow           : true,
            centerInsufficientSlides: true,
            effect                  : 'fade',
            pagination              : {
                el            : '.swiper-pagination',
                type          : 'fraction',
                renderFraction: function (currentClass, totalClass) {
                    return '<span class="' + currentClass + '"></span>' +
                        ' sur ' +
                        '<span class="' + totalClass + '"></span>';
                }
            },
            navigation              : {
                nextEl: '.swiper-next-temoignage',
                prevEl: '.swiper-prev-temoignage',
            }
        });
    };

    document.addEventListener("DOMContentLoaded", function (event) {
        createSliderOeuvres();
        createSliderTemoignages();
        expandChaptersButton();
    });

    document.addEventListener('oeuvresSlider', function (e) {
        createSliderOeuvres();
    }, false);

    document.addEventListener('temoignagesSlider', function (e) {
        createSliderTemoignages();
    }, false);

    document.addEventListener('expandChapters', function (e) {
        expandChaptersButton();
    }, false);

    const triggerAnimations = function () {
        if ($('section.chapitres').length) {
            if (($(window).scrollTop() + $(window).height()) > $('section.chapitres .chapitre').eq(0).offset().top + 100) {
                $('section.chapitres .chapitre').each(function (index, element) {
                    const parent = $(element);
                    setTimeout(function () {
                        parent.find('.chapitre_illus, .chapitre_title').addClass('animated');
                    }, index * 300);
                });
            }
        }

        if ($('section.buttons').length) {
            $('section.buttons').each(function (index, element) {
                if (($(window).scrollTop() + $(window).height()) > $(element).offset().top + 250) {
                    $(element).find('.custom_button a').addClass('animated');
                }
            });
        }

        if ($('section.photo').length) {
            $('section.photo').each(function (index, element) {
                if (($(window).scrollTop() + $(window).height()) > $(element).offset().top + 250) {
                    $(element).find('.photo_text_cont, .photo_cont').addClass('animated');
                }
            });
        }
    };

    $(document).ready(function () {
        $(window).scroll(function () {
            triggerAnimations();
        });
        triggerAnimations();
    });
})(jQuery);

