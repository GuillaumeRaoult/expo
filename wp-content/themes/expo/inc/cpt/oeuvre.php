<?php
/**
 * Chapitre custom post type
 *
 * @package Expo
 */

$labels = array(
    'name'               => _x('Oeuvres', 'expo'),
    'singular_name'      => _x('Oeuvre', 'expo'),
    'menu_name'          => _x('Oeuvres', 'expo'),
    'name_admin_bar'     => _x('Oeuvre', 'expo'),
    'add_new'            => _x('Ajouter une oeuvre', 'expo'),
    'add_new_item'       => __('Ajouter une nouvelle oeuvre', 'expo'),
    'new_item'           => __('Nouvelle oeuvre', 'expo'),
    'edit_item'          => __('Modifier l\'oeuvre', 'expo'),
    'view_item'          => __('Voir l\'oeuvre', 'expo'),
    'all_items'          => __('Toutes les oeuvres', 'expo'),
    'search_items'       => __('Chercher', 'expo'),
    'parent_item_colon'  => __('Oeuvre parente :', 'expo'),
    'not_found'          => __('Aucune oeuvre trouvée.', 'expo'),
    'not_found_in_trash' => __('Aucune oeuvre dans la corbeille.', 'expo'),
);

$args = array(
    'labels'              => $labels,
    'public'              => true,
    'show_ui'             => true,
    'hierarchical'        => false,
    'exclude_from_search' => false,
    'has_archive'         => false,
    'query_var'           => true,
    'can_export'          => true,
    'show_in_rest'        => true,
    'rewrite'             => true,
    'capability_type'     => 'post',
    'label'               => 'expo Oeuvres',
    'menu_icon'           => 'dashicons-format-aside',
    'supports'            => array(
        'author',
        'title',
        'excerpt',
        'thumbnail',
        'editor'
    )
);

register_post_type('oeuvre', $args);