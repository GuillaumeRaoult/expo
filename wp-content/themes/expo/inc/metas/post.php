<?php

add_action('cmb2_admin_init', 'cmb2_expo_metaboxes_post');
function cmb2_expo_metaboxes_post()
{
    $cmbPost = new_cmb2_box(array(
        'id'           => 'post_metabox',
        'title'        => __('Métadonnées', 'cmb2'),
        'object_types' => array('post'),
        'context'      => 'normal',
        'priority'     => 'low',
        'show_names'   => true,
    ));

    $cmbPost->add_field(array(
        'name'        => 'Date de début',
        'id'          => 'post_start',
        'type'        => 'text_date_timestamp',
        'date_format' => 'd/m/Y',
        'column'      => true,
    ));

    $cmbPost->add_field(array(
        'name'        => 'Date de fin',
        'id'          => 'post_end',
        'type'        => 'text_date_timestamp',
        'date_format' => 'd/m/Y',
        'column'      => true,
    ));

    $cmbPost->add_field(array(
        'name'   => 'Lieu',
        'id'     => 'post_place',
        'type'   => 'text',
        'column' => true,
    ));

}