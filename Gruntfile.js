module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
        less  : {
            production: {
                options: {
                    strictMath: true
                },
                files  : [
                    {
                        expand: true,
                        cwd   : 'wp-content/themes/expo/assets/less',
                        src   : [
                            '*.less',
                        ],
                        dest  : 'wp-content/themes/expo/assets/css',
                        ext   : '.css'
                    }
                ]
            }
        },
        cssmin: {
            style: {
                src : 'wp-content/themes/expo/assets/css/style.css',
                dest: 'wp-content/themes/expo/assets/css/style.min.css'
            }
        },
        uglify: {
            expo: {
                files: {
                    'wp-content/themes/expo/assets/js/expo.min.js': ['wp-content/themes/expo/assets/js/expo.js'],
                }
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            dist   : {
                src : [
                    'wp-content/themes/expo/assets/js/*.min.js',
                    '!wp-content/themes/expo/assets/js/site.min.js',
                ],
                dest: 'wp-content/themes/expo/assets/js/site.min.js'
            }
        },
        clean : {
            css: [
                'wp-content/themes/expo/assets/css/*.css',
                '!wp-content/themes/expo/assets/css/style.min.css',
                '!wp-content/themes/expo/assets/css/editor.css',
                '!wp-content/themes/expo/assets/css/admin.css',
            ],
            js : [
                'wp-content/themes/expo/assets/js/*.min.js',
                '!wp-content/themes/expo/assets/js/site.min.js',
            ]
        },
        watch : {
            scripts: {
                files: [
                    'wp-content/themes/expo/assets/js/expo.js',
                ],
                tasks: ['uglify:expo', 'concat', 'clean']
            },
            styles : {
                files: [
                    'wp-content/themes/expo/assets/less/*.less',
                    'wp-content/themes/expo/assets/less/imports/*.less',
                    'wp-content/themes/expo/assets/css/style.css',
                ],
                tasks: ['less', 'cssmin', 'clean']
            }
        }
    });

    grunt.loadNpmTasks('grunt-css');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask('default', ['less', 'cssmin', 'uglify:expo', 'concat', 'clean']);

};