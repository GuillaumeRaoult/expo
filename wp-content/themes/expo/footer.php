<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Expo
 */

$footer       = get_query_var('footer');
$borne        = get_query_var('borne', '');
$leftContent  = cmb2_get_option('footer_options', 'footer_left_side_content');
$rightContent = cmb2_get_option('footer_options', 'footer_right_side_content');
if ($footer != 'no-footer' && !$borne && ($leftContent || $rightContent)) {
    ?>
    <div class="footer_cont <?php echo get_query_var('footer') ?>">
        <div class="container-fluid">
            <section class="row align-items-center section credits">
                <div class="col-sm-6  col-md-5 offset-md-1 no-mobile-padding">
                    <div class="bloc participation bg_colored">
                        <?php
                        if ($leftContent) {
                            echo wpautop($leftContent);
                        }
                        ?>
                    </div>
                </div>
                <div class="col-sm-6 col-md-5">
                    <div class="bloc rosalis">
                        <?php
                        if ($rightContent) {
                            echo wpautop($rightContent);
                        }

                        ?>
                        <a href="https://rosalis.bibliotheque.toulouse.fr/" target="_blank" class="rosalis_link">
                            <?php
                            echo file_get_contents(get_template_directory() . '/assets/images/Rosace_Rosalis.svg', false, null);
                            ?>
                        </a>
                    </div>
                </div>
            </section>
            <?php
            $partenaires = partenairesGetOption('options_partenaires_group');
            if ($partenaires) {
                ?>
                <section class="row section partenaires">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-8 offset-sm-2">
                                <div class="row">
                                    <div class="col-12 section_title">
                                        <h2>Les partenaires de l'exposition</h2>
                                        <span class="dashicons dashicons-arrow-down-alt2 colored trigger_expand_partenaires"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 partenaires_list">
                        <div class="row">
                            <div class="prev d-lg-block d-none">
                                <div class="swiper-nav swiper-prev swiper-prev-video">
                                    <span class="dashicons dashicons-arrow-left-alt"></span>
                                </div>
                            </div>
                            <div class="swipecont">
                                <div class="swiper-container" id="partenaires-swiper-container">
                                    <div class="swiper-wrapper">
                                        <?php
                                        foreach ($partenaires as $partenaire) {
                                            ?>
                                            <div class="col-12 col-md-3 swiper-slide">
                                                <div class="partenaire_cont">
                                                    <div class="partenaire">
                                                        <?php
                                                        $hasUrl = isset($partenaire['partenaire_url']) && $partenaire['partenaire_url'];
                                                        if ($hasUrl) {
                                                        ?>
                                                        <a href="<?php echo $partenaire['partenaire_url'] ?>"
                                                           target="_blank">
                                                            <?php
                                                            }

                                                            echo wp_get_attachment_image($partenaire['partenaire_image_id'], 'medium', false, ['class' => 'img-fluid']);

                                                            if ($hasUrl) {
                                                            ?>
                                                        </a>
                                                    <?php
                                                    }
                                                    ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="next d-lg-block d-none">
                                <div class="swiper-nav swiper-next swiper-next-video">
                                    <span class="dashicons dashicons-arrow-right-alt"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php
            }
            ?>
            <section class="row">
                <div class="col-12">
                    <footer id="colophon" class="relative site-footer row">
                        <div class="col-12 col-sm-9">
                            <nav class="nav-footer">
                                <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'footer',
                                    'depth'          => 0,
                                    'menu_class'     => 'nav-list',
                                    'container'      => false,
                                    'walker'         => new Bootstrap_Collapse_NavWalker(),
                                    'fallback_cb'    => false
                                ));
                                ?>
                            </nav>
                        </div>
                        <div class="col-12 col-sm-3">
                            <div class="logo_mairie">
                                <a href="https://www.toulouse.fr/" target="_blank">
                                    <img alt="Mairie de Toulouse"
                                         src="<?php echo get_template_directory_uri() ?>/assets/images/Logo-Mairie-footer.png"/>
                                </a>
                            </div>
                        </div>
                    </footer>
                </div>
            </section>
            <?php

            ?>
        </div>
    </div>
    <?php
}

wp_footer(); ?>

</body>
</html>
